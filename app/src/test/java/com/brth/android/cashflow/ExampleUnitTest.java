package com.brth.android.cashflow;

import com.brth.android.cashflow.persistance.db_table_services.TagService;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() {
        assertEquals(4, 2 + 2);
        assertEquals( 0 , log2(4));
    }


    public static int log2(int N)
    {
        // calculate log2 N indirectly
        // using log() method
        int result = (int)(Math.log(N) / Math.log(3));
        return result;
    }

    @Test
    public void tagBinary () {
        List<Long> tagIds = new ArrayList<>();
        Long a = Long.valueOf(1);
        Long b = Long.valueOf(20);
        Long c = Long.valueOf(53);// the max pieces
        tagIds.add(b);
        tagIds.add(a);
        tagIds.add(c);
        long idBin = TagService.generateBinaryFromList(tagIds);
        System.out.println(idBin);
        tagIds = TagService.listOfIdsFromBinary(idBin);
        for (Long id : tagIds
        ) {
            System.out.println(id);
        }
        assertEquals(a, tagIds.get(0));
        assertEquals(b, tagIds.get(1));
        assertEquals(c, tagIds.get(2));

    }

}