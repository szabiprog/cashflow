package com.brth.android.cashflow.view.controller.models;

import android.os.Bundle;

import java.util.List;

public abstract class Views implements ViewModels {

    private Bundle savedInstanceState;

    public Bundle getSavedInstanceState() {
        return savedInstanceState;
    }

    public void setSavedInstanceState(Bundle savedInstanceState) {
        this.savedInstanceState = savedInstanceState;
    }

    public static Views selectViewModel(List<Views> history) {
        if (history == null || history.isEmpty()) return null;
        Views actualView = history.get(history.size() - 1);
        return actualView;
    }
}
