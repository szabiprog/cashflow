package com.brth.android.cashflow.view.activities.managements;

import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.brth.android.cashflow.R;
import com.brth.android.cashflow.data.model.db_data_tables.ProjectsTable;
import com.brth.android.cashflow.data.model.db_datas_entities.ProjectEntity;
import com.brth.android.cashflow.persistance.DataBaseOperations;
import com.brth.android.cashflow.persistance.db_table_services.ProjectService;

public class ProjectManagementActivity extends AppCompatActivity {


    private boolean update = false;
    private TextView textViewParent;
    private EditText textViewName;
    private EditText textViewDescription;
    private ProjectEntity parentProjectEntity=null;
    private ProjectEntity projectEntity;
    private DataBaseOperations dataBaseOperations;
    private Button deleteBt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project_management);

        textViewParent = findViewById(R.id.parent_value);
        textViewParent.setText("");
        textViewName = findViewById(R.id.name_value);
        textViewDescription = findViewById(R.id.description_value);

        Intent intent = getIntent();
        String nameOfProject = intent.getStringExtra("name");
        deleteBt = findViewById(R.id.bt_delete_project);

        update = (nameOfProject != null);
        if(!update){
            projectEntity =  ProjectService.searchDeactivatedProject(this);
            if (projectEntity != null )  nameOfProject = projectEntity.getName();
            update = (nameOfProject != null);
        }

        dataBaseOperations =  DataBaseOperations.getInstance(this);

        if (update) {
            projectEntity = ProjectService.searchByName(this,nameOfProject);
            textViewName.setText(projectEntity.getName());
            parentProjectEntity = (ProjectEntity)
                    dataBaseOperations.selectById
                            (new ProjectsTable(this), projectEntity.getOwnerId());
            deleteBt.setVisibility(View.VISIBLE);
        } else {
            projectEntity=new ProjectEntity();
            deleteBt.setVisibility(View.GONE);
            String ownerIdStr = intent.getStringExtra("ownerId");
            Long ownerId =Long.valueOf(ownerIdStr);
            if(ownerId>0){

                parentProjectEntity = (ProjectEntity)
                        dataBaseOperations.selectById(new ProjectsTable(this),ownerId);

            }else{
                parentProjectEntity = null;
            }

        }

        if (parentProjectEntity == null) {
            parentProjectEntity = new ProjectEntity();
            parentProjectEntity.setId(0);
            textViewParent.setText(R.string.root);
        } else textViewParent.setText(parentProjectEntity.getName());
    }

    synchronized public void onClick(View view) {
        switch (view.getId()) {
            case R.id.bt_save_project:
                findViewById(R.id.bt_save_project).setClickable(false);
                String nameValue = null;
                try {
                    nameValue = textViewName.getText().toString();
               }catch (Exception e){
                   nameValue = null;
               }
                if (nameValue!=null && nameValue.length() > 0) {
                    projectEntity.setName(nameValue);
                    if (saveEntity()) this.onBackPressed();
                    else  findViewById(R.id.bt_save_project).setClickable(true);
                    this.onBackPressed();
                } else {
                    sendToast(R.string.give_me_a_name);
                    findViewById(R.id.bt_save_project).setClickable(true);
                }
                break;
            case  R.id.bt_delete_project:
                if(usedProjectId(projectEntity))  sendToast(R.string.cancellable);
                    else ProjectService.delete(projectEntity,this);
                this.onBackPressed();
                break;
        }
    }

    private boolean usedProjectId(ProjectEntity projectEntity) {
        return ProjectService.usedProjectId(projectEntity.getId());

    }

    private boolean saveEntity() {
        if (ProjectService.unUsedName(this,projectEntity,update)) {
            fillUpEntity();
            if (update)
                ProjectService.updateProjectEntity(this,projectEntity);
            else {
                if (dataBaseOperations.insert(projectEntity, new ProjectsTable(this)) != 0)
                  return true;
            }
        } else sendToast(R.string.used_name);
        return false;
    }

    private void fillUpEntity() {
        String description = textViewDescription.getText().toString();
        projectEntity.setOwnerId(parentProjectEntity.getId());
        projectEntity.setDescription(description);
        projectEntity.setStatus(0);
    }



    private void sendToast(int str) {
        Toast toast = Toast.makeText(this, str, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 0, 0);
        toast.show();
    }



}
