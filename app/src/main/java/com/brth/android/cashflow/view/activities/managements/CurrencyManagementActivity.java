package com.brth.android.cashflow.view.activities.managements;


import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.brth.android.cashflow.R;
import com.brth.android.cashflow.data.enums.CurrencyRatioType;
import com.brth.android.cashflow.data.model.db_data_tables.CurrenciesTable;
import com.brth.android.cashflow.data.model.db_datas_entities.CurrencyEntity;
import com.brth.android.cashflow.persistance.db_table_services.CurrencyService;

public class CurrencyManagementActivity extends AppCompatActivity {
    private EditText name;
    private TextView left;
    private TextView right;
    private EditText ratio;
    private TextView operator;
    private CurrencyRatioType ratioType = CurrencyRatioType.BIGGER;
    private long defaultId;
    private CurrenciesTable currenciesTable = new CurrenciesTable(this);
    private CurrencyEntity currencyEntity = null;
    private CurrencyEntity defaultCurrency;
    private boolean update = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_currency_management);
        CurrencyEntity defCurrency = CurrencyService.getDefaultCurrency(this);
        if (defCurrency != null) defaultId = defCurrency.getId();
        else defaultId = 0;
        name = findViewById(R.id.name_value);
        left = findViewById(R.id.left_currensy);
        right = findViewById(R.id.right_currensy);
        operator = findViewById(R.id.operator);
        ratio = findViewById(R.id.ratio_value);
        Intent intent = getIntent();
        String nameOfCurrency = intent.getStringExtra("name");
        boolean onUpdateDefault = false;
        update = (nameOfCurrency != null);
        if (update) {
            currencyEntity = CurrencyService.getByName(nameOfCurrency, this);
            name.setText(nameOfCurrency);
            ratio.setText(currencyEntity.getRatioString());
            right.setText("1 " + name.getText());
            onUpdateDefault = defaultId == currencyEntity.getId();
        } else currencyEntity = new CurrencyEntity();


        if (defaultId > 0 && !onUpdateDefault) {
            defaultCurrency = CurrencyService.selectById(defaultId, this);
            left.setText(" 1 " + defaultCurrency.getName());
            name.addTextChangedListener(new TextWatcher() {
                @Override
                public void afterTextChanged(Editable s) {
                }

                @Override
                public void beforeTextChanged(CharSequence s, int start,
                                              int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start,
                                          int before, int count) {
                    right.setText("1 " + name.getText());
                }
            });
        } else {
            TextView sign = findViewById(R.id.sign);
            sign.setVisibility(View.INVISIBLE);
            TextView ratioText = findViewById(R.id.ratio);
            ratioText.setText(R.string.DefaultCurrency);
            ratio.setVisibility(View.INVISIBLE);
            operator.setVisibility(View.INVISIBLE);
            left.setVisibility(View.INVISIBLE);
            right.setVisibility(View.INVISIBLE);
        }
    }

    synchronized public void onClick(View view) {

        switch (view.getId()) {
            case R.id.save_currency:
                findViewById(R.id.save_currency).setClickable(false);
                String nameValue;
                try {
                    nameValue =
                            this.name.getText().toString();
                } catch (Exception e) {
                    nameValue = null;
                }
                if (nameValue != null && nameValue.length() > 0) {
                    if (saveEntity(nameValue)) this.onBackPressed();
                    else         findViewById(R.id.save_currency).setClickable(true);
                } else {
                    sendToast(R.string.give_me_a_name);
                    findViewById(R.id.save_currency).setClickable(true);
                }
                break;
            case R.id.operator:
                ratioType = CurrencyRatioType.valueOfDefaultType(
                        CurrencyRatioType.valueOfCurrencyRatioType(ratioType) * -1);
                if (ratioType == CurrencyRatioType.BIGGER) operator.setText(" * ");
                else operator.setText(" / ");
                break;
        }
    }

     synchronized private boolean saveEntity(String nameValue) {
        if (CurrencyService.unUsedName(currencyEntity, update, nameValue, this)) {
            fillUpEntity(nameValue);
            if (update) CurrencyService.update(currencyEntity, this);
            else CurrencyService.insert(currencyEntity, this);
            return true;
        } else sendToast(R.string.used_name);
        return false;
    }

    private void sendToast(int str) {
        Toast toast = Toast.makeText(this, str, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 0, 0);
        toast.show();
    }

    private void fillUpEntity(String nameValue) {
        String ratioString =
                ((EditText) findViewById(R.id.ratio_value)).getText().toString();
        if (ratioString.length() < 1) ratioString = "1";
        currencyEntity.setName(nameValue);
        currencyEntity.setRatioString(ratioString);
        currencyEntity.setRatioType(CurrencyRatioType.valueOfCurrencyRatioType(ratioType));
        if (!update) {
            if (defaultId < 1) {
                currencyEntity.setDefaultOne
                        (currencyEntity.DEFAULT);
                currencyEntity.setRatioString("1");
                currencyEntity.setRatioType(
                        CurrencyRatioType.valueOfCurrencyRatioType(CurrencyRatioType.BIGGER));
            } else {
                currencyEntity.setDefaultOne
                        (currencyEntity.NOT_DEFAULT);
            }
        }
    }
}
