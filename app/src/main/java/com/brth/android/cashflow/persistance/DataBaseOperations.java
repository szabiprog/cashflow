package com.brth.android.cashflow.persistance;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.brth.android.cashflow.data.model.db_data_tables.DbTable;
import com.brth.android.cashflow.data.model.db_datas_entities.DbObjectEntity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class DataBaseOperations implements Serializable {

    private static DataBaseOperations dataBaseOperations;
    private static ApkSqlHelper sqLiteOpenHelper;
    private static SQLiteDatabase database;
    private static List<DbTable> dbTables ;

    private DataBaseOperations( ) {
    }

    public static synchronized DataBaseOperations getInstance(Context context) {
        if (dataBaseOperations == null) {
            dataBaseOperations = new DataBaseOperations();
            sqLiteOpenHelper = new ApkSqlHelper(context);
            database = sqLiteOpenHelper.getWritableDatabase();
            dbTables = sqLiteOpenHelper.getDbTables();
        }
        return dataBaseOperations;
    }

    public long insert(DbObjectEntity dbObjectEntity, DbTable dbTable) {
       database = sqLiteOpenHelper.getWritableDatabase();
       return dbTable.insert(dbObjectEntity, database);
    }

    public boolean update(DbObjectEntity dbObjectEntity, DbTable dbTable) {
        database = sqLiteOpenHelper.getWritableDatabase();
        return  dbTable.update(dbObjectEntity,database);
    }

    public int delete(DbObjectEntity dbObjectEntity, DbTable dbTable) {
        database = sqLiteOpenHelper.getWritableDatabase();
        return dbTable.delete(dbObjectEntity, database);
    }

    public void deleteAll(DbTable dbTable){
        database = sqLiteOpenHelper.getWritableDatabase();
        dbTable.deleteAll(database);
    }

    public List<DbObjectEntity> selectAllDbObjectEntities(DbTable dbTable) {
        database = sqLiteOpenHelper.getWritableDatabase();
        return dbTable.selectAllDbObjectEntities( database);
    }

    public   List<DbObjectEntity> selectDbObjectEntities(DbTable dbTable, String inColums, String values)
    {
        database = sqLiteOpenHelper.getWritableDatabase();
        return dbTable.selectDbObjectEntities( database,  inColums,  values);
    }

    public static List<DbTable> getDbTables() {
        return dbTables;
    }

    public void close(){
        try {
            finalize();
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }

    @Override
    protected void finalize() throws Throwable {
        database.close();
        super.finalize();
    }

    @Override
    public String toString() {
        return " DataBaseOperations{" +
                "sqLiteOpenHelper=" + sqLiteOpenHelper +
                ", database=" + database +
                '}';
    }

    public DbObjectEntity selectById(DbTable dbTable ,long id) {
        database = sqLiteOpenHelper.getWritableDatabase();
        return  dbTable.selectById(id,database);
    }

    public void insertAll(DbTable table, ArrayList<DbObjectEntity> dbObjectEntities) {
        for (int i = 0; i < dbObjectEntities.size(); i++) {
            DbObjectEntity dbObjectEntity =dbObjectEntities.get(i);
           table.insert(dbObjectEntity,database);
        }
    }
}



