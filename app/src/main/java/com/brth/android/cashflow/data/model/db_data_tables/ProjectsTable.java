package com.brth.android.cashflow.data.model.db_data_tables;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.brth.android.cashflow.R;
import com.brth.android.cashflow.data.model.db_datas_entities.DbObjectEntity;
import com.brth.android.cashflow.data.model.db_datas_entities.ProjectEntity;

import java.util.ArrayList;
import java.util.List;

public class ProjectsTable extends DbTable {


    public final static String NAME_TABLE = "projects";
    public final static String ID_COLUMN = "id";
    public final static String NAME_COLUMN = "name";
    public final static String OWNER_ID_COLUMN = "owner_id";
    public final static String DESCRIPTION_COLUMN = "description";
    public final static String INN_COLUMN = "inn";
    public final static String OUT_COLUMN = "out";
    public final static String STATUS_COLUMN = "status";
    public final static String[] ALL_COLUMN =
            {ID_COLUMN, NAME_COLUMN, OWNER_ID_COLUMN, DESCRIPTION_COLUMN,INN_COLUMN,OUT_COLUMN,STATUS_COLUMN};

    public ProjectsTable(Context context) {
        super(context);
    }


    @Override
    public long insert(DbObjectEntity dbObjectEntity, SQLiteDatabase database) {
        ProjectEntity p = (ProjectEntity) dbObjectEntity;
        long index = database.insert(NAME_TABLE, null, dbObjectEntityToContentValues(p));
        p.setId(index);
        return index;
    }

    @Override
    public boolean update(DbObjectEntity dbObjectEntity, SQLiteDatabase database) {
        long result = database.update(NAME_TABLE, dbObjectEntityToContentValues(dbObjectEntity),
                ID_COLUMN + "=" + dbObjectEntity.getId(), null);
        return result==1;
    }

    @Override
    public int delete(DbObjectEntity dbObjectEntity, SQLiteDatabase database) {
        // database.delete(NAME_TABLE,ID_COLUMN + "=" + dbObjectEntity.getId(), null);
        return 0;
    }

    @Override
    public List<DbObjectEntity> selectAllDbObjectEntities( SQLiteDatabase database) {

        Cursor cursor = database.query(NAME_TABLE, ALL_COLUMN, null,
                null, null, null, ID_COLUMN + " asc");
        List<DbObjectEntity> dbObjectEntityList = new ArrayList<>();
        while (cursor.moveToNext()) {
            DbObjectEntity dbObjectEntity = cursorToDbObjectEntity(cursor);
            dbObjectEntityList.add(dbObjectEntity);

        }
        cursor.close();
        return dbObjectEntityList;
    }

    @Override
    public DbObjectEntity cursorToDbObjectEntity(Cursor cursor) {
        ProjectEntity projectEntity = new ProjectEntity();
        projectEntity.setId(cursor.getLong(0));
        projectEntity.setName(cursor.getString(1));
        projectEntity.setOwnerId(cursor.getLong(2));
        projectEntity.setDescription(cursor.getString(3));
        projectEntity.setInn(cursor.getLong(4));
        projectEntity.setOut(cursor.getLong(5));
        projectEntity.setStatus(cursor.getLong(6));
        return projectEntity;
    }

    @Override
    public ContentValues dbObjectEntityToContentValues(DbObjectEntity dbObjectEntity) {
        ProjectEntity p = (ProjectEntity) dbObjectEntity;

        ContentValues values = new ContentValues();
        values.put(NAME_COLUMN, p.getName());
        values.put(OWNER_ID_COLUMN, p.getOwnerId());
        values.put(DESCRIPTION_COLUMN, p.getDescription());
        values.put(INN_COLUMN, p.getInn());
        values.put(OUT_COLUMN, p.getOut());
        values.put(STATUS_COLUMN, p.getStatus());
        return values;
    }

    @Override
    public List<DbObjectEntity> selectDbObjectEntities
            ( SQLiteDatabase database, String inColums, String values) {
        Cursor cursor;
        cursor = database.query(NAME_TABLE, ALL_COLUMN, inColums + "=?", new String[]{values}
                , null, null, ID_COLUMN + " asc");
        List<DbObjectEntity> dbObjectEntityList = new ArrayList<>();
        while (cursor.moveToNext()) {
            DbObjectEntity dbObjectEntity = cursorToDbObjectEntity(cursor);
            dbObjectEntityList.add(dbObjectEntity);
        }
        cursor.close();
        return dbObjectEntityList;
    }

    @Override
    public DbObjectEntity selectById(Long id , SQLiteDatabase database) {
        Cursor cursor;
        cursor = database.query(NAME_TABLE, ALL_COLUMN, ID_COLUMN + "=?", new String[]{id+""}
                , null, null, ID_COLUMN + " asc");
        List<DbObjectEntity> dbObjectEntityList = new ArrayList<>();

        while (cursor.moveToNext()) {
            DbObjectEntity dbObjectEntity = cursorToDbObjectEntity(cursor);
                dbObjectEntityList.add(dbObjectEntity);
              }
        cursor.close();
        if(!dbObjectEntityList.isEmpty()) return dbObjectEntityList.get(0);
        else return null;
    }

    @Override
    public String createTable() {

        return "CREATE TABLE IF NOT EXISTS " + NAME_TABLE + " (" +
                 ID_COLUMN + " integer primary key autoincrement, " + NAME_COLUMN + " text not null, " +
                 OWNER_ID_COLUMN + " integer default 0, "  + DESCRIPTION_COLUMN + " text not null, "+
                 INN_COLUMN + " integer default 0 , " + OUT_COLUMN + " integer default 0 ,"+
                 STATUS_COLUMN + " integer default 0 );";
    }

    @Override
    public String getTableName() {
        return NAME_TABLE;
    }
    @Override
    public void deleteAll( SQLiteDatabase database)
    {
        database.execSQL("delete from "+ NAME_TABLE);
        database.execSQL(String.format("DELETE FROM SQLITE_SEQUENCE WHERE NAME = '%s'", NAME_TABLE));
    }

    @Override
    public String toString() {
        return getContext().getString(R.string.projects);

    }
}


