package com.brth.android.cashflow.view.activities.managements;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.CompoundButton;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;

import com.brth.android.cashflow.R;
import com.brth.android.cashflow.data.model.db_datas_entities.CarServiceEntity;
import com.brth.android.cashflow.data.model.db_datas_entities.MoneyAccountEntity;
import com.brth.android.cashflow.data.model.db_datas_entities.PaymentEventEntity;
import com.brth.android.cashflow.data.model.db_datas_entities.ProjectEntity;
import com.brth.android.cashflow.persistance.db_table_services.CarServiceService;
import com.brth.android.cashflow.persistance.db_table_services.CurrencyService;
import com.brth.android.cashflow.persistance.db_table_services.MoneyAccountService;
import com.brth.android.cashflow.persistance.db_table_services.PaymentEventService;
import com.brth.android.cashflow.persistance.db_table_services.ProjectService;
import com.brth.android.cashflow.view.activities.HaveOnlyNameActivity;
import com.brth.android.cashflow.view.activities.MoneyAccountsActivity;
import com.brth.android.cashflow.view.activities.ProjectsActivity;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class PaymentEventManagementActivity extends AbstractPaymentEventManagement {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        onMyCreate();

        swMoneyMoveDirection.setBackground(ContextCompat.getDrawable
                (PaymentEventManagementActivity.this, R.drawable.button_green));


        datePicker();
        tvDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(PaymentEventManagementActivity.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd. HH:mm:ss");
        currentDateTime = sdf.format(new Date());

        incomingIntent = getIntent();
        buttonSave.setText(getResources().getString(R.string.save));

        String dateOfEntity = incomingIntent.getStringExtra("date");
        update = false;
        update = (dateOfEntity != null);
        if (update) {
            paymentEventEntity = (PaymentEventEntity)
                    PaymentEventService.allPaymentsByADate(dateOfEntity, this).get(0);
            if (oldPaymentEventEntity == null) {
                oldPaymentEventEntity =
                        PaymentEventService.clonePaymentEntityInverseDirection(paymentEventEntity);
            }
            setUpByEntity(false);
        } else {
            if (savedInstanceState == null) {
                paymentEventEntity = PaymentEventService.createPaymentEntity(currentDateTime);
                swMoneyMoveDirection.setChecked(true);
                swMoneyMoveDirection.setText(getMoney);
            } else {
                paymentEventEntity =
                        (PaymentEventEntity)savedInstanceState.getSerializable("payment");
            }
            carServiceEntity = new CarServiceEntity();
            carServiceEntity.setOdometerInterval(0);
            dateOfNextService = "";
        }
        selectViewOfBtType();
        tvDate.setText(paymentEventEntity.getDate());
        swMoneyMoveDirection.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView,
                                         boolean isChecked) {
                if (isChecked) {
                    swMoneyMoveDirection.setText(getMoney);
                    swMoneyMoveDirection.setBackground(ContextCompat.getDrawable
                            (PaymentEventManagementActivity.this, R.drawable.button_green));
                } else {
                    swMoneyMoveDirection.setText(spendMoney);
                    swMoneyMoveDirection.setBackground(ContextCompat.getDrawable
                            (PaymentEventManagementActivity.this, R.drawable.button_red));
                }
                long moneyMoveDirection = paymentEventEntity.getDirectionType() * -1;
                paymentEventEntity.setDirectionType(moneyMoveDirection);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        setupScreen();
    }

    synchronized public void onClick(View view) {
        Intent intent;
        switch (view.getId()) {
            case R.id.switch_service_fuel_normal:
                long paymentType = PaymentEventService.switchType(paymentEventEntity.getType());
                paymentEventEntity.setType(paymentType);
                selectViewOfBtType();
                break;
            case R.id.bt_select_project:
                intent = new Intent(this, ProjectsActivity.class);
                intent.putExtra("select", "select");
                if (update) intent.putExtra("old", oldPaymentEventEntity);
                startActivityForResult(intent, REQUEST_CODE_PROJECT);
                break;
            case R.id.bt_select_tag:
                intent = new Intent(this, HaveOnlyNameActivity.class);
                intent.putExtra("select", "select");
                intent.putExtra("selected", "tags");
                if (update) intent.putExtra("old", oldPaymentEventEntity);
                intent.putExtra("tagIdsBin", paymentEventEntity.getTagIdsBin());
                startActivityForResult(intent, REQUEST_CODE_TAG);
                break;
            case R.id.bt_select_account:
                intent = new Intent(this, MoneyAccountsActivity.class);
                intent.putExtra("select", "select");
                if (update) intent.putExtra("old", oldPaymentEventEntity);
                startActivityForResult(intent, REQUEST_CODE_MONEY_ACC);
                break;
            case R.id.bt_save:
                buttonSave.setClickable(false);
                if (update) alertDialog();
                else {
                    String moneyValue;
                    try {
                        moneyValue = textMoney.getText().toString();
                    } catch (Exception e) {
                        moneyValue = null;
                    }
                    if (paymentEventEntity.getMoneyAccountId() > 0 &&
                            paymentEventEntity.getProjectId() > 0 &&
                            moneyValue != null && moneyValue.length() > 0 && Integer.valueOf(moneyValue) > 0) {
                        if (saveEntity() != 0) this.onBackPressed();
                    } else{
                        sendToast(R.string.select_project_and_acc_and_price);
                        buttonSave.setClickable(true);
                    }
                }
                break;
            default:
                break;
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_CODE_PROJECT:
                if (resultCode == RESULT_OK) {
                    String nameOfProject = data.getStringExtra("name");
                    btProjectSelect.setText(nameOfProject);
                    long projectAccId = ProjectService.getIdByName(this, nameOfProject);
                    paymentEventEntity.setProjectId(projectAccId);
                }
                break;
            case REQUEST_CODE_MONEY_ACC:
                if (resultCode == RESULT_OK) {
                    String nameOfMoneyAccount = data.getStringExtra("name");
                    oldPaymentEventEntity = (PaymentEventEntity) data.getExtras().get("old");
                    btMoneyAccountSelect.setText(nameOfMoneyAccount);
                    setupFromAccByName(nameOfMoneyAccount);
                }
                break;
            case REQUEST_CODE_TAG:
                if (resultCode == RESULT_OK) {
                    String name = data.getStringExtra("name");
                    oldPaymentEventEntity = (PaymentEventEntity) data.getExtras().get("old");
                    //btTagSelect.setText(name);
                    Long tagIdsBin = data.getLongExtra("tagIdsBin",0);

                    paymentEventEntity.setTagIdsBin(tagIdsBin);
                }
                break;
        }
    }

    private void alertDialog() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setMessage(R.string.alert_update);
        dialog.setTitle(R.string.update);
        dialog.setPositiveButton(R.string.yes,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,
                                        int which) {
                        String moneyValue = textMoney.getText().toString();
                        if (paymentEventEntity.getMoneyAccountId() > 0 &&
                                paymentEventEntity.getProjectId() > 0 &&
                                moneyValue.length() > 0 && Integer.valueOf(moneyValue) > 0) {
                            if (saveEntity() != 0)
                                PaymentEventManagementActivity.this.onBackPressed();
                        } else {
                            sendToast(R.string.select_project_and_acc_and_price);
                            buttonSave.setClickable(true);
                        }
                    }
                });
        dialog.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                    buttonSave.setClickable(true);
            }
        });
        AlertDialog alertDialog = dialog.create();
        alertDialog.show();
    }

    private void setUpByEntity(boolean update) {

        selectViewOfBtType();
        String quantity = paymentEventEntity.getQuantity();
        textQuantity.setText(quantity);
        textOdometer.setText(String.valueOf(paymentEventEntity.getOdometer()));

        CarServiceEntity carServiceEntity =
                CarServiceService.selectById(paymentEventEntity.getServiceId(), this);
        if (carServiceEntity != null) {
            textOdometerInterval.setText(String.valueOf(carServiceEntity.getOdometerInterval()));
            dateOfNextService = carServiceEntity.getDateOfNextService();
            tvDateInterval.setText(dateOfNextService);
        } else {
            textOdometerInterval.setText(String.valueOf(0));
            dateOfNextService = "";
            tvDateInterval.setText("");
        }
        currentDateTime = paymentEventEntity.getDate();
        tvDate.setText(currentDateTime);
        if (paymentEventEntity.getProjectId() > 0) {
            ProjectEntity projectEntity = ProjectService.selectById(paymentEventEntity.getProjectId(), this);
            String nameOfProject = projectEntity.getName();
            btProjectSelect.setText(nameOfProject);
        }
        long moneyAccountId = paymentEventEntity.getMoneyAccountId();
        if (moneyAccountId > 0) {
            MoneyAccountEntity moneyAccountEntity = MoneyAccountService.selectById(moneyAccountId, this);
            String nameOfMoneyAccount = moneyAccountEntity.getName();
            btMoneyAccountSelect.setText(nameOfMoneyAccount);
            long currencyId = moneyAccountEntity.getCurrencyId();
            String nameOfCurrency = CurrencyService.selectById(currencyId, this).getName();
            tvCurrencyName.setText(nameOfCurrency);
        }

        textDescription.setText(paymentEventEntity.getDescription());
        textMoney.setText(String.format("%d", paymentEventEntity.getAmount()));
        setUpMoneyMoveDirection();
    }

    private long setUpMoneyMoveDirection() {
        if (paymentEventEntity.getDirectionType() > 0) {
            swMoneyMoveDirection.setText(getMoney);
            swMoneyMoveDirection.setBackground(ContextCompat.getDrawable
                    (PaymentEventManagementActivity.this, R.drawable.button_green));
            swMoneyMoveDirection.setChecked(true);
            return 1;
        } else {
            swMoneyMoveDirection.setChecked(false);
            swMoneyMoveDirection.setText(spendMoney);
            swMoneyMoveDirection.setBackground(ContextCompat.getDrawable
                    (PaymentEventManagementActivity.this, R.drawable.button_red));
            swMoneyMoveDirection.setChecked(false);
            return -1;
        }
    }

    private void setupScreen() {
        if (paymentEventEntity.getId() > 0) setUpByEntity(true);
        else setUpByEntity(false);
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        getDatasFromForm();
        outState.putSerializable("payment", paymentEventEntity);
    }

}
