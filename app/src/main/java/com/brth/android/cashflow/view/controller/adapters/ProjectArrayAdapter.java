package com.brth.android.cashflow.view.controller.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.brth.android.cashflow.R;
import com.brth.android.cashflow.data.model.db_datas_entities.DbObjectEntity;
import com.brth.android.cashflow.data.model.db_datas_entities.ProjectEntity;

import java.text.NumberFormat;
import java.util.List;


public class ProjectArrayAdapter extends ArrayAdapter<DbObjectEntity> {

    private final Activity context;
    private final List<DbObjectEntity> items;

    public ProjectArrayAdapter(Activity context, List<DbObjectEntity> items) {
        super(context, R.layout.row_project, items);
        this.context = context;
        this.items = items;
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {

        LayoutInflater inflater = context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.row_project, null, true);

        TextView TVName = rowView.findViewById(R.id.name);
        TextView TVDescription = rowView.findViewById(R.id.description);
        TextView TVItemId = rowView.findViewById(R.id.tv_id);
        TextView TVSaldo = rowView.findViewById(R.id.tv_saldo);
        LinearLayout LLToList = rowView.findViewById(R.id.ll_for_list);
        ProjectEntity projectEntity = (ProjectEntity) items.get(position);
        TVName.setText(projectEntity.getName());
        TVDescription.setText(projectEntity.getDescription());
        TVItemId.setText(projectEntity.getId()+"");
        Long balance = projectEntity.getInn() - projectEntity.getOut();
       // TVSaldo.setText(String.valueOf(PaymentEventService.balance(projectEntity.getId(),context)));
        TVSaldo.setText(NumberFormat.getInstance().format(balance));

        LLToList.setTag(projectEntity.getId());
        return rowView;
    }



}