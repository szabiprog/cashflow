package com.brth.android.cashflow.data.enums;

public enum Periodic {
    FIX_DAILY,
    WEEKLY,
    TWO_WEEKLY,
    MONTHLY,
    TWO_MONTHLY,
    QUARTERLY,
    HALF_YEARLY,
    YEARLY;

    static int valueOfPeriodic(Periodic periodic) {
        if (periodic.equals(FIX_DAILY)) return 0;
        if (periodic.equals(WEEKLY)) return 7;
        if (periodic.equals(TWO_WEEKLY)) return 14;
        if (periodic.equals(MONTHLY)) return 30;
        if (periodic.equals(TWO_MONTHLY)) return 60;
        if (periodic.equals(QUARTERLY)) return 90;
        if (periodic.equals(HALF_YEARLY)) return 180;
        if (periodic.equals(YEARLY)) return 360;
        return 0;
    }
}
