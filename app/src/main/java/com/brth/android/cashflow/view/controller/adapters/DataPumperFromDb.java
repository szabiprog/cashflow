package com.brth.android.cashflow.view.controller.adapters;

import android.content.Context;

import com.brth.android.cashflow.data.model.db_data_tables.DbTable;
import com.brth.android.cashflow.persistance.DataBaseOperations;

import java.util.List;

public class DataPumperFromDb {


    static private DataBaseOperations dataBaseOperations;


    private DataPumperFromDb() {
    }

    static private void getDataBaseOperations(Context context) {
        dataBaseOperations = DataBaseOperations.getInstance(context);

    }

    static public List<DbTable> dataTablesPumper(Context context) {
        getDataBaseOperations(context);
        return DataBaseOperations.getDbTables();
    }
}
