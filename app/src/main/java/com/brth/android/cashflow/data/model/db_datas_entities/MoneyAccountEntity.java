package com.brth.android.cashflow.data.model.db_datas_entities;

public class MoneyAccountEntity extends DbObjectEntity {


    private long defaultOne;
    private String description;
    private long projectId;
    private long balance;
    private long currencyId;

    public MoneyAccountEntity() {
    }



    public long getDefaultOne() {
        return defaultOne;
    }

    public void setDefaultOne(long defaultOne) {
        this.defaultOne = defaultOne;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getProjectId() {
        return projectId;
    }

    public void setProjectId(long projectId) {
        this.projectId = projectId;
    }

    public long getBalance() {
        return balance;
    }

    public void setBalance(long balance) {
        this.balance = balance;
    }

    public long getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(long currencyId) {
        this.currencyId = currencyId;
    }
}
