package com.brth.android.cashflow.data.model.db_data_tables;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.brth.android.cashflow.R;
import com.brth.android.cashflow.data.model.db_datas_entities.CarServiceEntity;
import com.brth.android.cashflow.data.model.db_datas_entities.DbObjectEntity;

import java.util.ArrayList;
import java.util.List;

public class CarServicesTable extends DbTable {

    public final static String NAME_TABLE = "services";
    public final static String ID_COLUMN = "id";
    public final static String DESCRIPTION_COLUMN = "description";
    public final static String PAYMENT_ID_COLUMN = "payment_id";
    public final static String DATE_COLUMN = "date";
    public final static String ODOMETER_COLUMN = "odometer";
    public final static String VALID_COLUMN = "valid";

    public final static String[] ALL_COLUMN =
            {ID_COLUMN, DESCRIPTION_COLUMN,PAYMENT_ID_COLUMN,DATE_COLUMN,ODOMETER_COLUMN ,VALID_COLUMN};

    public CarServicesTable(Context context) {
        super(context);
    }


    @Override
    public long insert(DbObjectEntity dbObjectEntity, SQLiteDatabase database) {
        CarServiceEntity p = (CarServiceEntity) dbObjectEntity;

        long index = database.insert(NAME_TABLE, null, dbObjectEntityToContentValues(p));
        p.setId(index);
        return index;
    }

    @Override
    public boolean update(DbObjectEntity dbObjectEntity, SQLiteDatabase database) {
        long result = database.update(NAME_TABLE, dbObjectEntityToContentValues(dbObjectEntity),
                ID_COLUMN + "=" + dbObjectEntity.getId(), null);
        return result==1;

    }

    @Override
    public int delete(DbObjectEntity dbObjectEntity, SQLiteDatabase database) {

        return database.delete(NAME_TABLE,ID_COLUMN + "=" + dbObjectEntity.getId(), null);
    }

    @Override
    public List<DbObjectEntity> selectAllDbObjectEntities(SQLiteDatabase database) {

        Cursor cursor = database.query(NAME_TABLE, ALL_COLUMN, null,
                null, null, null, ID_COLUMN + " asc");
        List<DbObjectEntity> dbObjectEntityList = new ArrayList<>();
        while (cursor.moveToNext()) {
            DbObjectEntity dbObjectEntity = cursorToDbObjectEntity(cursor);
            dbObjectEntityList.add(dbObjectEntity);

        }
        cursor.close();
        return dbObjectEntityList;
    }

    @Override
    public DbObjectEntity cursorToDbObjectEntity(Cursor cursor) {
        CarServiceEntity entity = new CarServiceEntity();
        entity.setId(cursor.getLong(0));
        entity.setName(cursor.getString(1));
        entity.setPaymentId(cursor.getLong(2));
        entity.setDateOfNextService(cursor.getString(3));
        entity.setOdometerInterval(cursor.getLong(4));
        entity.setValid(cursor.getLong(5));
        return entity;
    }

    @Override
    public ContentValues dbObjectEntityToContentValues(DbObjectEntity dbObjectEntity) {
        CarServiceEntity p = (CarServiceEntity) dbObjectEntity;

        ContentValues values = new ContentValues();
        values.put(DESCRIPTION_COLUMN, p.getName());
        values.put(PAYMENT_ID_COLUMN,p.getPaymentId());
        values.put(DATE_COLUMN,p.getDateOfNextService());
        values.put(ODOMETER_COLUMN,p.getOdometerInterval());
        values.put(VALID_COLUMN,p.getValid());
        return values;
    }

    @Override
    public List<DbObjectEntity> selectDbObjectEntities
            ( SQLiteDatabase database, String inColums, String values) {
        Cursor cursor;
        cursor = database.query(NAME_TABLE, ALL_COLUMN, inColums + "=?", new String[]{values}
                , null, null, ID_COLUMN + " asc");
        List<DbObjectEntity> dbObjectEntityList = new ArrayList<>();
        while (cursor.moveToNext()) {
            DbObjectEntity dbObjectEntity = cursorToDbObjectEntity(cursor);
            dbObjectEntityList.add(dbObjectEntity);
        }
        cursor.close();
        return dbObjectEntityList;
    }

    @Override
    public DbObjectEntity selectById(Long id , SQLiteDatabase database) {
        Cursor cursor;
        cursor = database.query(NAME_TABLE, ALL_COLUMN, ID_COLUMN + "=?", new String[]{id+""}
                , null, null, ID_COLUMN + " asc");
        List<DbObjectEntity> dbObjectEntityList = new ArrayList<>();

        while (cursor.moveToNext()) {
            DbObjectEntity dbObjectEntity = cursorToDbObjectEntity(cursor);
            dbObjectEntityList.add(dbObjectEntity);
        }
        cursor.close();
        if(!dbObjectEntityList.isEmpty()) return dbObjectEntityList.get(0);
        else return null;
    }

    @Override
    public String createTable() {

        return "CREATE TABLE IF NOT EXISTS " + NAME_TABLE + " (" +
                ID_COLUMN + " integer primary key autoincrement, " + DESCRIPTION_COLUMN + " text not null,"
                + PAYMENT_ID_COLUMN + " integer default 0, " + DATE_COLUMN + " text not null, "
                + ODOMETER_COLUMN + " integer default 0, " + VALID_COLUMN + " integer default 0 );";
    }

    @Override
    public String getTableName() {
        return NAME_TABLE;
    }
    @Override
    public void deleteAll( SQLiteDatabase database)
    {
        database.execSQL("delete from "+ NAME_TABLE);
        database.execSQL(String.format("DELETE FROM SQLITE_SEQUENCE WHERE NAME = '%s'", NAME_TABLE));
    }

    @Override
    public String toString() {
        return getContext().getString(R.string.carServices);

    }
}

