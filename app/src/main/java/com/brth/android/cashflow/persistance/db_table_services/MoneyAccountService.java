package com.brth.android.cashflow.persistance.db_table_services;

import android.content.Context;

import com.brth.android.cashflow.data.enums.CurrencyRatioType;
import com.brth.android.cashflow.data.model.db_data_tables.ProjectsTable;
import com.brth.android.cashflow.data.model.db_datas_entities.CurrencyEntity;
import com.brth.android.cashflow.data.model.db_datas_entities.DbObjectEntity;
import com.brth.android.cashflow.data.model.db_datas_entities.MoneyAccountEntity;
import com.brth.android.cashflow.data.model.db_datas_entities.PaymentEventEntity;

import java.util.List;

public class MoneyAccountService extends DbService {
    private MoneyAccountService() {

    }


    public static List<DbObjectEntity> allMoneyAcc(Context context) {
        return selectAllDbObjectEntitiesByClass(MoneyAccountEntity.class, context);
    }

    public static boolean registeringToMoneyAccount(PaymentEventEntity p, Context context) {
        initializing(context);
        MoneyAccountEntity moneyAccountEntity;
        try {
            moneyAccountEntity = (MoneyAccountEntity) dataBaseOperations.selectById
                    (moneyAccountsTable, p.getMoneyAccountId());
        } catch (Exception e) {
            return false;
        }
        if (moneyAccountEntity != null) {
            long actualBalance = moneyAccountEntity.getBalance() +
                    (p.getDirectionType() * p.getAmount());
            actualBalance = MoneyAccountService.balanceOfOneMoneyAcc(moneyAccountEntity.getName(),context);
           // System.out.println(actualBalance);
            moneyAccountEntity.setBalance(actualBalance);
            return dataBaseOperations.update(moneyAccountEntity, moneyAccountsTable);
        } else return false;

    }

    public static Long balanceOfOneMoneyAcc(String nameOfAcc, Context context) {
        initializing(context);
        try {
            long idOfAcc = getIdByColumn(nameOfAcc, moneyAccountsTable, moneyAccountsTable.NAME_COLUMN);
            List<DbObjectEntity> payments = dataBaseOperations.selectDbObjectEntities
                    (paymentEventsTable, paymentEventsTable.ACCOUNT_ID_COLUMN,
                            String.valueOf(idOfAcc));
            long inn = PaymentEventService.allMovingBySameCurrency(context, payments, 1);
            long out = PaymentEventService.allMovingBySameCurrency(context, payments, -1);
            return inn - out;

        } catch (Exception e) {
            return null;
        }
    }

    public static MoneyAccountEntity selectByName(String nameOfMoneyAccount, Context context) {
        initializing(context);
        MoneyAccountEntity m;
        try {
            m = (MoneyAccountEntity) dataBaseOperations.selectById(moneyAccountsTable,
                    getIdByColumn(nameOfMoneyAccount, moneyAccountsTable, moneyAccountsTable.NAME_COLUMN));
        } catch (Exception e) {
            m = null;
        }
        return m;
    }

    public static MoneyAccountEntity selectById(long id, Context context) {
        initializing(context);
        MoneyAccountEntity entity =
                (MoneyAccountEntity) dataBaseOperations.selectById(moneyAccountsTable, id);
        return entity;
    }

    public static boolean usedMoneyAccId(long id) {
        List<DbObjectEntity> dbObjectEntities = dataBaseOperations.selectDbObjectEntities
                (paymentEventsTable, paymentEventsTable.ACCOUNT_ID_COLUMN, String.valueOf(id));
        return dbObjectEntities.size() > 0;
    }

    public static void insert(MoneyAccountEntity moneyAccountEntity, Context context) {
        initializing(context);
        dataBaseOperations.insert(moneyAccountEntity, moneyAccountsTable);

    }

    public static void update(MoneyAccountEntity moneyAccountEntity, Context context) {
        initializing(context);
        dataBaseOperations.update(moneyAccountEntity, moneyAccountsTable);
    }

    public static boolean unUsedName(MoneyAccountEntity moneyAccountEntity,  boolean update, Context context) {
        initializing(context);
        String nameValue = moneyAccountEntity.getName();
        List<DbObjectEntity> moneyAccounts =
                        dataBaseOperations.selectDbObjectEntities
                                (moneyAccountsTable, ProjectsTable.NAME_COLUMN, nameValue);;
        if (update && !moneyAccounts.isEmpty()) {
            for (int i = 0; i < moneyAccounts.size(); i++) {
                MoneyAccountEntity c;
                c = (MoneyAccountEntity) moneyAccounts.get(0);
                if (c.getId() != moneyAccountEntity.getId()) return false;
            }
            return true;
        }
        return moneyAccounts.isEmpty();
    }

    public static long balanceOfMoneyAccounts(Context context) {
        List<DbObjectEntity> moneyAcconts = MoneyAccountService.allMoneyAcc(context);
        return balanceFromAllMoneyAcc(context, moneyAcconts);
    }

    private static long balanceFromAllMoneyAcc(Context context, List<DbObjectEntity> moneyAccounts) {
        initializing(context);
        long amount = 0;
        for (int i = 0; i < moneyAccounts.size(); i++) {
            MoneyAccountEntity p = (MoneyAccountEntity) moneyAccounts.get(i);
            long cId = p.getCurrencyId();
            CurrencyEntity c = (CurrencyEntity) CurrencyService.selectById
                    (cId, context);
            if (c.getRatioType() == CurrencyRatioType.valueOfCurrencyRatioType(CurrencyRatioType.BIGGER)) {
                amount += p.getBalance() * Double.valueOf(c.getRatioString());
            } else {
                amount += p.getBalance() / Double.valueOf(c.getRatioString());
            }
        }
        return amount;
    }

}
