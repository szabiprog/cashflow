package com.brth.android.cashflow.data.interfaces;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.brth.android.cashflow.data.model.db_datas_entities.DbObjectEntity;

import java.util.List;

public interface DataBaseFunctions {


    long insert(DbObjectEntity dbObjectEntity, SQLiteDatabase database);

    boolean update(DbObjectEntity dbObjectEntity, SQLiteDatabase database);


    int delete(DbObjectEntity dbObjectEntity, SQLiteDatabase database);

    void deleteAll(SQLiteDatabase database);

    List<DbObjectEntity> selectAllDbObjectEntities( SQLiteDatabase database);

    DbObjectEntity cursorToDbObjectEntity(Cursor cursor);

    ContentValues dbObjectEntityToContentValues(DbObjectEntity dbObjectEntity);

    List<DbObjectEntity> selectDbObjectEntities( SQLiteDatabase database, String inColums, String values);

    DbObjectEntity selectById(Long id, SQLiteDatabase database);

    //boolean overWriteId(Long oldID,Long newId, SQLiteDatabase database);
}
