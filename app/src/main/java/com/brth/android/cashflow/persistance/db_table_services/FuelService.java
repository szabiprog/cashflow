package com.brth.android.cashflow.persistance.db_table_services;

import android.content.Context;

import com.brth.android.cashflow.data.model.db_data_tables.FuelsTable;
import com.brth.android.cashflow.data.model.db_datas_entities.DbObjectEntity;
import com.brth.android.cashflow.data.model.db_datas_entities.FuelEntity;

import java.util.List;

public class FuelService extends DbService {
    private FuelService() {
    }

    public static FuelEntity selectById(long id, Context context) {
        initializing(context);
        return (FuelEntity) dataBaseOperations.selectById(fuelsTable, id);
    }

    public static boolean unUsedName(FuelEntity fuelEntity, boolean update, String nameValue, Context context) {
        initializing(context);
        List<DbObjectEntity> dbObjectEntities =
                dataBaseOperations.selectDbObjectEntities
                        (fuelsTable, FuelsTable.NAME_COLUMN, nameValue);
        if (update && !dbObjectEntities.isEmpty()) {
            for (int i = 0; i < dbObjectEntities.size(); i++) {
                FuelEntity c;
                c = (FuelEntity) dbObjectEntities.get(i);
                if (c.getId() != fuelEntity.getId()) return false;
            }
            return true;
        }
        return dbObjectEntities.isEmpty();
    }

    public static boolean update(FuelEntity fuelEntity,  Context context) {
        initializing(context);
        return dataBaseOperations.update(fuelEntity, fuelsTable);
    }

    public static long insert(FuelEntity fuelEntity,  Context context) {
        initializing(context);
        return dataBaseOperations.insert(fuelEntity, fuelsTable);
    }
    public static FuelEntity getByName( Context context, String name) {
        initializing(context);
        List<DbObjectEntity> dbObjectEntities =
                dataBaseOperations.selectDbObjectEntities
                        (fuelsTable, FuelsTable.NAME_COLUMN, name);
        if (dbObjectEntities.size() > 0) return (FuelEntity) dbObjectEntities.get(0);
        return null;
    }

    public static List<DbObjectEntity> getAllFuelEntities(Context context) {
        return DbService.selectAllDbObjectEntitiesByClass( FuelEntity.class,context);
    }
}
