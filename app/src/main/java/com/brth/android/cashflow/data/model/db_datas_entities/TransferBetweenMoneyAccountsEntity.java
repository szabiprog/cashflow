package com.brth.android.cashflow.data.model.db_datas_entities;

public class TransferBetweenMoneyAccountsEntity extends DbObjectEntity {

    public static final long STATUS_DONE = 2;
    public static final long STATUS_DELETED = 0;
    public static final long STATUS_ACTIVE = 1;

    private long fromAccId;
    private long toAccId;
    private long fromPaymentEventId;
    private long toPaymentEventId;
    private long fromAmount;
    private long toAmount;

    private String description;


    private String date;
    private long status;

    public TransferBetweenMoneyAccountsEntity() {
    }

    public long getFromAccId() {
        return fromAccId;
    }

    public void setFromAccId(long fromAccId) {
        this.fromAccId = fromAccId;
    }

    public long getToAccId() {
        return toAccId;
    }

    public void setToAccId(long toAccId) {
        this.toAccId = toAccId;
    }

    public long getFromPaymentEventId() {
        return fromPaymentEventId;
    }

    public void setFromPaymentEventId(long fromPaymentEventId) {
        this.fromPaymentEventId = fromPaymentEventId;
    }

    public long getToPaymentEventId() {
        return toPaymentEventId;
    }

    public void setToPaymentEventId(long toPaymentEventId) {
        this.toPaymentEventId = toPaymentEventId;
    }

    public long getFromAmount() {
        return fromAmount;
    }

    public void setFromAmount(long fromAmount) {
        this.fromAmount = fromAmount;
    }

    public long getToAmount() {
        return toAmount;
    }

    public void setToAmount(long toAmount) {
        this.toAmount = toAmount;
    }



    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public long getStatus() {
        return status;
    }

    public void setStatus(long status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
