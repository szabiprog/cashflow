package com.brth.android.cashflow.data.model.db_datas_entities;

import com.brth.android.cashflow.persistance.db_table_services.TagService;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class PaymentEventEntity extends DbObjectEntity {

    public static final long INVALID = 0;
    public static final long VALID = 1;
    public static final long REGISTERED = 2;
    public static final long TRANSFER = -1;
    public static final long NORMAL_TYPE = 0;
    public static final long FUEL_TYPE = 1;
    public static final long SERVICE_TYPE = 2;

    private long projectId;
    private long moneyAccountId;
    private long currencyId;
    private long tagIdsBin; // binary max 31 pieces tag
    private Set<Long> tagIds;

    private long amount;
    private String description;
    private long directionType;
    private String date;
    private long valid;

    private long type ;
    private long odometer;
    private String quantity;
    private long serviceId;
    private long fuelId;

    public PaymentEventEntity() {
       this.tagIds = new HashSet<>();
    }

    public long getProjectId() {
        return projectId;
    }

    public void setProjectId(long projectId) {
        this.projectId = projectId;
    }

    public long getAmount() {
        return amount;
    }

    public void setAmount(long amount) {
        this.amount = amount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getDirectionType() {
        return directionType;
    }

    public void setDirectionType(long directionType) {
        this.directionType = directionType;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }


    public long getValid() {
        return valid;
    }

    public void setValid(long valid) {
        this.valid = valid;
    }

    public long getMoneyAccountId() {
        return moneyAccountId;
    }

    public void setMoneyAccountId(long moneyAccountId) {
        this.moneyAccountId = moneyAccountId;
    }

    public long getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(long currencyId) {
        this.currencyId = currencyId;
    }

    public long getTagIdsBin() {
        return this.tagIdsBin;
    }

    public void setTagIdsBin(long tagIdsBin) {
        this.tagIdsBin = tagIdsBin;
        this.tagIds.clear();
        this.tagIds.addAll(TagService.listOfIdsFromBinary(tagIdsBin));
    }

    public long getType() {
        return type;
    }

    public void setType(long type) {
        this.type = type;
    }

    public long getOdometer() {
        return odometer;
    }

    public void setOdometer(long odometer) {
        this.odometer = odometer;
    }

    public String getQuantity() {
        if(quantity!=null) return quantity;
        else return "";
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public long getServiceId() {
        return serviceId;
    }

    public void setServiceId(long serviceId) {
        this.serviceId = serviceId;
    }

    public long getFuelId() {
        return fuelId;
    }

    public void setFuelId(long fuelId) {
        this.fuelId = fuelId;
    }

    public Set<Long> getTagIds() {
        return this.tagIds;
    }

    public boolean adATagId(Long tagId) {
        try {
            this.tagIds.add( tagId );
            List<Long> tags = new ArrayList<>();
            tags.addAll(tagIds);
            this.tagIdsBin = TagService.generateBinaryFromList( tags);
            return true;
        }catch (Exception e){
            return false;
        }
    }

    public boolean removeATagId (Long tagId){
        try {
            this.tagIds.remove( tagId );
            this.tagIdsBin = TagService.generateBinaryFromList((List<Long>) tagIds);
            return true;
        }catch (Exception e){
            return false;
        }
    }

    @Override
    public String toString() {
        return "PaymentEventEntity{" +
                "projectId=" + projectId +
                ", moneyAccountId=" + moneyAccountId +
                ", currencyId=" + currencyId +
                ", amount=" + amount +
                ", description='" + description + '\'' +
                ", directionType=" + directionType +
                ", date='" + date + '\'' +
                ", valid=" + valid +
                ", type=" + type +
                ", odometer=" + odometer +
                ", quantity='" + quantity + '\'' +
                ", serviceId=" + serviceId +
                ", fuelId=" + fuelId +
                ", Id=" + getId() +
                '}';
    }
}
