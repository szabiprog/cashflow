package com.brth.android.cashflow.view.controller.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.brth.android.cashflow.R;
import com.brth.android.cashflow.data.model.db_datas_entities.DbObjectEntity;
import com.brth.android.cashflow.data.model.db_datas_entities.ProjectEntity;

import java.util.List;


public class ProjectArrayAdapterToSelect extends ArrayAdapter<DbObjectEntity> {

    private final Activity context;
    private final List<DbObjectEntity> items;

    public ProjectArrayAdapterToSelect(Activity context, List<DbObjectEntity> items) {
        super(context, R.layout.row_project, items);
        this.context = context;
        this.items = items;
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {

        LayoutInflater inflater = context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.row_project, null, true);

        TextView name = rowView.findViewById(R.id.name);
        rowView.findViewById(R.id.ll_for_list).setVisibility(View.GONE);
        TextView description = rowView.findViewById(R.id.description);
        TextView itemId = rowView.findViewById(R.id.tv_id);
        TextView balanceTv = rowView.findViewById(R.id.tv_saldo);
        ProjectEntity projectEntity = (ProjectEntity) items.get(position);
        name.setText(projectEntity.getName());
        description.setText(projectEntity.getDescription());
        itemId.setText(projectEntity.getId()+"");
        balanceTv.setVisibility(View.GONE);
        return rowView;
    }




}