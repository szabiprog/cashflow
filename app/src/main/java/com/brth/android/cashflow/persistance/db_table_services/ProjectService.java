package com.brth.android.cashflow.persistance.db_table_services;

import android.content.Context;

import com.brth.android.cashflow.data.enums.Statements;
import com.brth.android.cashflow.data.model.db_data_tables.PaymentEventsTable;
import com.brth.android.cashflow.data.model.db_data_tables.ProjectsTable;
import com.brth.android.cashflow.data.model.db_datas_entities.DbObjectEntity;
import com.brth.android.cashflow.data.model.db_datas_entities.PaymentEventEntity;
import com.brth.android.cashflow.data.model.db_datas_entities.ProjectEntity;

import java.util.ArrayList;
import java.util.List;

public class ProjectService extends DbService {

    private ProjectService() {
    }

    public static ProjectEntity selectById(long id, Context context) {
        initializing(context);
        return (ProjectEntity) dataBaseOperations.selectById(projectsTable, id);
    }

    public static List<DbObjectEntity> allProjects(Context context) {
        initializing(context);
        return dataBaseOperations.selectAllDbObjectEntities(projectsTable);
    }

    public static List<Long> projectTree(long ownerId, Context context) {
        initializing(context);
        List<Long> list = new ArrayList<>();
        list.add(ownerId);
        List<DbObjectEntity> childrenList = dataBaseOperations.selectDbObjectEntities
                (projectsTable, ProjectsTable.OWNER_ID_COLUMN, String.valueOf(ownerId));
        for (int i = 0; i < childrenList.size(); i++) {
            list.addAll(projectTree(childrenList.get(i).getId(), context));
        }
        return list;
    }

    public static List<Long> projectSaldo(long ownerId, Context context) {
        initializing(context);
        List<Long> list = new ArrayList<>();
        list.add(ownerId);
        List<DbObjectEntity> childrenList = dataBaseOperations.selectDbObjectEntities
                (projectsTable, ProjectsTable.OWNER_ID_COLUMN, String.valueOf(ownerId));
        for (int i = 0; i < childrenList.size(); i++) {
            list.addAll(projectTree(childrenList.get(i).getId(), context));
        }
        return list;
    }


    public static List<DbObjectEntity> selectAllByOwnerId(String ownerId, Context context) {
        initializing(context);
        return dataBaseOperations.selectDbObjectEntities
                (projectsTable, projectsTable.OWNER_ID_COLUMN, ownerId);
    }

    public static boolean delete(DbObjectEntity projectEntity, Context context) {
        initializing(context);
        ((ProjectEntity)projectEntity).setOwnerId(-1);
        ((ProjectEntity)projectEntity).setStatus(
                Statements.valueOfStatementsType(Statements.HIDDEN));
       return dataBaseOperations.update(projectEntity,projectsTable);
    }



    public static boolean usedProjectId(long id) {
        List<DbObjectEntity> dbObjectEntities = dataBaseOperations.selectDbObjectEntities
                (paymentEventsTable, PaymentEventsTable.OWNER_ID_COLUMN, String.valueOf(id));
        dbObjectEntities.addAll(dataBaseOperations.selectDbObjectEntities
                (projectsTable, ProjectsTable.OWNER_ID_COLUMN, String.valueOf(id)));
        //System.out.println(dbObjectEntities.size()+" "+id);
        return dbObjectEntities.size() > 0;
    }
    public static ProjectEntity searchDeactivatedProject(Context context) {
        List<DbObjectEntity> projects = allDeactivatedProject(context);
        if (projects.size() > 0) return (ProjectEntity) projects.get(0);
        return null;
    }
    public static List<DbObjectEntity> allDeactivatedProject(Context context){
        List<DbObjectEntity> projects =      dataBaseOperations.selectDbObjectEntities
                (new ProjectsTable(context), ProjectsTable.STATUS_COLUMN, "-1");
        return  projects;
    }

    public static  ProjectEntity searchByName(Context context,String nameOfProject) {
        List<DbObjectEntity> projects =
                dataBaseOperations.selectDbObjectEntities
                        (new ProjectsTable(context), ProjectsTable.NAME_COLUMN, nameOfProject);
        if (projects.size() > 0) return (ProjectEntity) projects.get(0);
        return null;
    }
    public static long getIdByName(Context context, String nameOfProject) {
        return getIdByColumn(nameOfProject, projectsTable, ProjectsTable.NAME_COLUMN);
    }

    public static boolean resetAllProjectInnOut(Context context) {
        initializing(context);
        List<DbObjectEntity> projects = allProjects(context);
        for (int i = 0; i < projects.size(); i++) {
            ProjectEntity projectEntity = (ProjectEntity) projects.get(i);
            ProjectService.resetProjectInnOut(context, projectEntity);
        }
        return true;
    }

    private static void resetProjectInnOut(Context context, ProjectEntity projectEntity) {
        projectEntity.setInn(0);
        projectEntity.setOut(0);
        dataBaseOperations.update(projectEntity, projectsTable);
    }

    public static boolean paymentRegisterToProjectTree(Context context, PaymentEventEntity paymentEventEntity) {
        long ownerId = paymentEventEntity.getProjectId();
        long amount = paymentEventEntity.getAmount();
        long direction = paymentEventEntity.getDirectionType();
        List<DbObjectEntity> list = new ArrayList<>();
        list.add(paymentEventEntity);
        while (ownerId > 0) {
            ProjectEntity projectEntity = ProjectService.selectById(ownerId, context);
            if (direction == -1) {
                long out = PaymentEventService.moving(context, list, -1);
                out = projectEntity.getOut() + out;
                projectEntity.setOut(out);
            } else {
                long inn = PaymentEventService.moving(context, list, 1);
                inn = projectEntity.getInn() + inn;
                projectEntity.setInn(inn);
            }
            dataBaseOperations.update(projectEntity, projectsTable);
            ownerId = projectEntity.getOwnerId();
        }
        return true;
    }

    public static boolean paymentUpdateRegisterToProjectTree
            (Context context, PaymentEventEntity oldPaymentEventEntity, PaymentEventEntity paymentEventEntity) {
            oldPaymentEventEntity=PaymentEventService.clonePaymentEntityInverseDirection(oldPaymentEventEntity);
            long ownerId = oldPaymentEventEntity.getProjectId();
            long amount = oldPaymentEventEntity.getAmount();
            long direction = oldPaymentEventEntity.getDirectionType();
            List<DbObjectEntity> list = new ArrayList<>();
            list.add(oldPaymentEventEntity);
            while (ownerId > 0) {
                ProjectEntity projectEntity = ProjectService.selectById(ownerId, context);
                if (direction == -1) {
                    long out = PaymentEventService.moving(context, list, -1);
                    out =  projectEntity.getOut()  - out ;
                    projectEntity.setOut(out);
                } else {
                    long inn = PaymentEventService.moving(context, list, 1);
                    inn = projectEntity.getInn() - inn;
                    projectEntity.setInn(inn);
                }
                dataBaseOperations.update(projectEntity, projectsTable);
                ownerId = projectEntity.getOwnerId();
            }
            return  paymentRegisterToProjectTree(context,paymentEventEntity);
    }

    public static void updateProjectEntity(Context context, ProjectEntity projectEntity) {
        dataBaseOperations.update(projectEntity, new ProjectsTable(context));
    }
    public static void rebuild(Context context){
        List<DbObjectEntity> paymentEventEntities = PaymentEventService.allValidPayments(context);
        ProjectService.resetAllProjectInnOut(context);
        for (int i = 0; i < paymentEventEntities.size(); i++) {
            PaymentEventEntity paymentEventEntity = (PaymentEventEntity) paymentEventEntities.get(i);
            ProjectService.paymentRegisterToProjectTree(context, paymentEventEntity);
        }
    }
    public static boolean unUsedName(Context context,ProjectEntity projectEntity,Boolean update) {
        initializing(context);
        String nameValue = projectEntity.getName();
        List<DbObjectEntity> projects =
                dataBaseOperations.selectDbObjectEntities
                        (new ProjectsTable(context), ProjectsTable.NAME_COLUMN, nameValue);
        if (update && !projects.isEmpty()) {
            for (int i = 0; i < projects.size(); i++) {
                ProjectEntity c;
                c = (ProjectEntity) projects.get(0);

                if (c.getId() != projectEntity.getId()) return false;
            }
            return true;
        }
        return projects.isEmpty();
    }

}