package com.brth.android.cashflow.view.activities.managements;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.brth.android.cashflow.R;
import com.brth.android.cashflow.data.model.db_datas_entities.CurrencyEntity;
import com.brth.android.cashflow.data.model.db_datas_entities.MoneyAccountEntity;
import com.brth.android.cashflow.data.model.db_datas_entities.TransferBetweenMoneyAccountsEntity;
import com.brth.android.cashflow.persistance.db_table_services.CurrencyService;
import com.brth.android.cashflow.persistance.db_table_services.MoneyAccountService;
import com.brth.android.cashflow.persistance.db_table_services.TransferService;
import com.brth.android.cashflow.view.activities.MoneyAccountsActivity;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class TransferManagementActivity extends AppCompatActivity {


    private final int REQUEST_CODE_FROM_ACC = 111;
    private final int REQUEST_CODE_TO_ACC = 222;
    private Button btSelectAccFrom;
    private Button btSelectAccTo;
    private String nameOfFromAcc = null;
    private String nameOfToAcc = null;
    private Long amountFrom = null;

    private Long amountTo = null;

    private EditText amountFromEt;
    private TextView amountToTv;
    private TextView fromBalanceTv;
    private TextView toBalanceTv;
    private TransferBetweenMoneyAccountsEntity transferBetweenMoneyAccountsEntity;
    private TextView tvDate;
    private String currentDateTime = null;
    private Calendar myCalendar;
    private DatePickerDialog.OnDateSetListener date;
    private boolean update = false;
    private EditText descriptionTv;
    private MoneyAccountEntity fromAcc;
    private MoneyAccountEntity toAcc;
    private CurrencyEntity fromCurrency;
    private CurrencyEntity toCurrency;
    private TextView fromCurrencyTv;
    private TextView toCurrencyTv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transfer_management);
        btSelectAccFrom = findViewById(R.id.bt_select_from_account);
        btSelectAccTo = findViewById(R.id.bt_select_to_account);
        amountFromEt = findViewById(R.id.et_from_amount);
        amountToTv = findViewById(R.id.tv_to_amount);
        descriptionTv = findViewById(R.id.text_description);
        toCurrencyTv = findViewById(R.id.tv_to_currency_name);
        fromCurrencyTv = findViewById(R.id.tv_from_currency_name);
        fromBalanceTv = findViewById(R.id.tv_from_balance);
        toBalanceTv = findViewById(R.id.tv_to_balance);
        tvDate = findViewById(R.id.tv_currentDate);
        datePicker();
        tvDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(TransferManagementActivity.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
        amountFromEt.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
                try {
                    amountFrom = Long.valueOf(s.toString());
                    if (s.length() > 0 && fromCurrency != null & toCurrency != null) {
                        exchange();
                    } else {
                        amountTo = null;
                        amountToTv.setText("");
                    }
                } catch (Exception e) {
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {

            }
        });
        Intent intent = getIntent();
        String idOfTransfer = intent.getStringExtra("id");
        update = (idOfTransfer != null);

        if (update) {
            try {
                fillUppFields(Long.valueOf(idOfTransfer));
            } catch (Exception e) {
                update = false;
            }
            System.out.println(update);
        } else {
            initializing();
        }

    }

    private void initializing() {
        transferBetweenMoneyAccountsEntity = new TransferBetweenMoneyAccountsEntity();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd. HH:mm:ss");
        currentDateTime = sdf.format(new Date());
        transferBetweenMoneyAccountsEntity.setDate(currentDateTime);
        tvDate.setText(currentDateTime);
        toAcc = null;
        fromAcc = null;
        amountFrom = null;
        amountTo = null;
    }

    private void fillUppFields(long id) {

        transferBetweenMoneyAccountsEntity = TransferService.selectById(id, this);
        fromAcc = MoneyAccountService.selectById(transferBetweenMoneyAccountsEntity.getFromAccId(), this);
        toAcc = MoneyAccountService.selectById(transferBetweenMoneyAccountsEntity.getToAccId(), this);

        fromCurrency = CurrencyService.selectById(fromAcc.getCurrencyId(), this);
        toCurrency = CurrencyService.selectById(toAcc.getCurrencyId(), this);

        nameOfFromAcc = fromAcc.getName();
        nameOfToAcc = toAcc.getName();
        btSelectAccTo.setText(nameOfToAcc);
        btSelectAccFrom.setText(nameOfFromAcc);
        currentDateTime = transferBetweenMoneyAccountsEntity.getDate();
        tvDate.setText(currentDateTime);
        amountFromEt.setText(String.format("%d", transferBetweenMoneyAccountsEntity.getFromAmount()));
        amountToTv.setText(String.format("%d", transferBetweenMoneyAccountsEntity.getToAmount()));
        descriptionTv.setText(transferBetweenMoneyAccountsEntity.getDescription());
    }


    synchronized public void onClick(View view) {
        Intent intent;
        switch (view.getId()) {
            case R.id.bt_select_from_account:
                intent = new Intent(this, MoneyAccountsActivity.class);
                intent.putExtra("select", "select");
                startActivityForResult(intent, REQUEST_CODE_FROM_ACC);
                break;
            case R.id.bt_select_to_account:

                intent = new Intent(this, MoneyAccountsActivity.class);
                intent.putExtra("select", "select");
                startActivityForResult(intent, REQUEST_CODE_TO_ACC);
                break;
            case R.id.bt_save_transfer:
                findViewById(R.id.bt_save_transfer).setClickable(false);
                try {
                    amountFrom = Long.valueOf(amountFromEt.getText().toString());
                } catch (Exception e) {
                    amountFrom = null;
                }
                transferBetweenMoneyAccountsEntity.setDescription(descriptionTv.getText().toString());
                if (toAcc != null && fromAcc != null
                        && amountFrom != null && amountFrom != 0
                        && amountTo != null) {
                    if (saveEntity()) this.onBackPressed();
                    else{
                        sendToast(R.string.backwards_error);
                        findViewById(R.id.bt_save_transfer).setClickable(true);
                    }
                } else {
                    sendToast(R.string.select_money_account_and_grant_value);
                    findViewById(R.id.bt_save_transfer).setClickable(true);
                }
                break;
            default:

                break;

        }
    }

    private boolean saveEntity() {
        transferBetweenMoneyAccountsEntity.setFromAmount(amountFrom.longValue());
        transferBetweenMoneyAccountsEntity.setToAmount(amountTo.longValue());
        if (update) {
            return TransferService.updateTransfer(transferBetweenMoneyAccountsEntity, this);
        } else return TransferService.makeTransfer(transferBetweenMoneyAccountsEntity, this);
    }


    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_CODE_FROM_ACC:
                if (resultCode == RESULT_OK) {
                    String name = data.getStringExtra("name");
                    nameOfFromAcc = name;
                    if (!nameOfFromAcc.equals(nameOfToAcc)) {
                        btSelectAccFrom.setText(name);
                        fromAcc = MoneyAccountService.selectByName(nameOfFromAcc, this);
                        transferBetweenMoneyAccountsEntity.setFromAccId(fromAcc.getId());
                        fromCurrency = CurrencyService.selectById(fromAcc.getCurrencyId(), this);
                        fromCurrencyTv.setText(fromCurrency.getName());
                        ((TextView) findViewById(R.id.tv_from_balance_currency)).setText(fromCurrency.getName());
                        fromBalanceTv.setText(String.valueOf(fromAcc.getBalance()));
                    } else sendToast(R.string.select_an_other_acc);
                    if (amountFrom != null && toAcc != null) {
                        exchange();
                    }
                }
                break;
            case REQUEST_CODE_TO_ACC:
                if (resultCode == RESULT_OK) {
                    String name = data.getStringExtra("name");
                    nameOfToAcc = name;

                    if (!nameOfToAcc.equals(nameOfFromAcc)) {
                        btSelectAccTo.setText(nameOfToAcc);
                        toAcc = MoneyAccountService.selectByName(nameOfToAcc, this);
                        transferBetweenMoneyAccountsEntity.setToAccId(toAcc.getId());
                        toCurrency = CurrencyService.selectById(toAcc.getCurrencyId(), this);
                        toCurrencyTv.setText(toCurrency.getName());
                        ((TextView) findViewById(R.id.tv_to_balance_currency)).setText(toCurrency.getName());
                        toBalanceTv.setText(String.valueOf(toAcc.getBalance()));
                    } else sendToast(R.string.select_an_other_acc);
                    if (amountFrom != null && fromAcc != null) {
                        exchange();
                    }
                }
                break;
        }

    }

    private void exchange() {
        amountTo =
                CurrencyService.exchange(fromCurrency, toCurrency, amountFrom, this);
        amountToTv.setText(amountTo.toString());
    }


    private void sendToast(int str) {
        Toast toast = Toast.makeText(this, str, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 0, 0);
        toast.show();
    }

    private void updateLabel() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd. HH:mm:ss");
        tvDate.setText(sdf.format(myCalendar.getTime()));
        currentDateTime = (String) tvDate.getText();
        transferBetweenMoneyAccountsEntity.setDate(currentDateTime);
    }

    private void datePicker() {
        myCalendar = Calendar.getInstance();
        date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }
        };

    }
}
