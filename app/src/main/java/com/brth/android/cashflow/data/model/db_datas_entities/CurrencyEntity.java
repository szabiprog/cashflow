package com.brth.android.cashflow.data.model.db_datas_entities;

public class CurrencyEntity extends DbObjectEntity {

    public static final long DEFAULT = 1;
    public static final long NOT_DEFAULT = 0;
    private long defaultOne;    // correlated currency
    private String ratioString;
    private long ratioType; // minus value dividing, positive value multiplication


    public CurrencyEntity() {
    }

    public long getDefaultOne() {
        return defaultOne;
    }

    public void setDefaultOne(long defaultOne) {
        this.defaultOne = defaultOne;
    }

    public String getRatioString() {
        return ratioString;
    }

    public void setRatioString(String ratioString) {
        this.ratioString = ratioString;
    }

    public long getRatioType() {
        return ratioType;
    }

    public void setRatioType(long ratioType) {
        this.ratioType = ratioType;
    }
}
