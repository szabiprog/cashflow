package com.brth.android.cashflow.persistance.db_table_services;

import android.content.Context;

import com.brth.android.cashflow.data.model.db_data_tables.CarServicesTable;
import com.brth.android.cashflow.data.model.db_data_tables.CurrenciesTable;
import com.brth.android.cashflow.data.model.db_data_tables.DbTable;
import com.brth.android.cashflow.data.model.db_data_tables.FuelsTable;
import com.brth.android.cashflow.data.model.db_data_tables.MoneyAccountsTable;
import com.brth.android.cashflow.data.model.db_data_tables.PaymentEventsTable;
import com.brth.android.cashflow.data.model.db_data_tables.ProjectsTable;
import com.brth.android.cashflow.data.model.db_data_tables.TagTable;
import com.brth.android.cashflow.data.model.db_data_tables.TransferBMATable;
import com.brth.android.cashflow.data.model.db_datas_entities.CurrencyEntity;
import com.brth.android.cashflow.data.model.db_datas_entities.DbObjectEntity;
import com.brth.android.cashflow.data.model.db_datas_entities.FuelEntity;
import com.brth.android.cashflow.data.model.db_datas_entities.MoneyAccountEntity;
import com.brth.android.cashflow.data.model.db_datas_entities.PaymentEventEntity;
import com.brth.android.cashflow.data.model.db_datas_entities.ProjectEntity;
import com.brth.android.cashflow.data.model.db_datas_entities.TagEntity;
import com.brth.android.cashflow.data.model.db_datas_entities.TransferBetweenMoneyAccountsEntity;
import com.brth.android.cashflow.persistance.DataBaseOperations;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class DbService {
    protected static DataBaseOperations dataBaseOperations;
    protected static PaymentEventsTable paymentEventsTable;
    protected static ProjectsTable projectsTable;
    protected static MoneyAccountsTable moneyAccountsTable;
    protected static CurrenciesTable currenciesTable;
    protected static TransferBMATable transferBMATable;
    protected static CarServicesTable carServicesTable;
    protected static FuelsTable fuelsTable;
    protected static TagTable tagTable;

    protected static void initializing(Context context) {
        DbService.dataBaseOperations = DataBaseOperations.getInstance(context);
        if (DbService.projectsTable == null) DbService.projectsTable = new ProjectsTable(context);
        if (DbService.paymentEventsTable == null)
            DbService.paymentEventsTable = new PaymentEventsTable(context);
        if (DbService.currenciesTable == null)
            DbService.currenciesTable = new CurrenciesTable(context);
        if (DbService.transferBMATable == null)
            DbService.transferBMATable = new TransferBMATable(context);
        if (DbService.moneyAccountsTable == null)
            DbService.moneyAccountsTable = new MoneyAccountsTable(context);
        if (DbService.carServicesTable == null)
            DbService.carServicesTable = new CarServicesTable(context);
        if (DbService.fuelsTable == null)
            DbService.fuelsTable = new FuelsTable(context);
        if (DbService.tagTable == null)
            DbService.tagTable = new TagTable(context);
    }

    public static  List<DbObjectEntity> selectAllDbObjectEntitiesByClass(Class nameOfClass, Context context) {
        if (nameOfClass==null)  throw new NullPointerException();
        initializing(context);
        if (FuelEntity.class.equals(nameOfClass))
            return dataBaseOperations.selectAllDbObjectEntities(fuelsTable);

        if (TagEntity.class.equals(nameOfClass))
            return dataBaseOperations.selectAllDbObjectEntities(tagTable);

        if (CurrencyEntity.class.equals(nameOfClass))
            return dataBaseOperations.selectAllDbObjectEntities(currenciesTable);

        if (MoneyAccountEntity.class.equals(nameOfClass))
            return dataBaseOperations.selectAllDbObjectEntities(moneyAccountsTable);

        if (TransferBetweenMoneyAccountsEntity.class.equals(nameOfClass))
            return dataBaseOperations.selectAllDbObjectEntities(transferBMATable);

        throw new IllegalStateException("Unexpected value: " + nameOfClass.getClass());
    }

    public static long getIdByColumn(String nameValue, DbTable dbTable, String column) {
        long id = 0;
        List<DbObjectEntity> dbObjectEntities =
                dataBaseOperations.selectDbObjectEntities
                        (dbTable, column, nameValue);
        if (!dbObjectEntities.isEmpty()) id = dbObjectEntities.get(0).getId();
        return id;
    }

    public static String getAllEntitiesFromDatabase(Context context) {
        initializing(context);
        StringBuilder json = new StringBuilder();

       List<DbObjectEntity> listOfAll ;
        List<DbTable> dbTables =dataBaseOperations.getDbTables();

        Gson gson = new Gson();

        for (int i = 0; i < dbTables.size() ; i++) {
            DbTable table =dbTables.get(i);
            json.append("/!/"+table.getTableName()+"/!/");
            listOfAll =  dataBaseOperations.selectAllDbObjectEntities(table);
            json.append( gson.toJson(listOfAll));
        }
        return  json.toString();
    }

    public static String reBuildDatabaseFromGson(Context context, String json) {
        initializing(context);

        String[] jsonArray = json.split("/!/");

        for (int i = 1; i < jsonArray.length ; i++) {
            String nameOfTable = jsonArray[i];
            i++;
           rebuildTable(nameOfTable,jsonArray[i]);

        }
        return  "";
    }

    private static void rebuildTable(String nameOfTable, String json) {
        Type type = null;
        DbTable table = null;
        Gson gson = new Gson();
        ArrayList<DbObjectEntity> dbObjectEntities;
        switch (nameOfTable){
            case ProjectsTable.NAME_TABLE:
                dataBaseOperations.deleteAll(projectsTable);
                type = new TypeToken< ArrayList< ProjectEntity >>(){}.getType();
                 table = projectsTable;
                break;
            case CurrenciesTable.NAME_TABLE:
                dataBaseOperations.deleteAll(currenciesTable);
                type = new TypeToken< ArrayList< CurrencyEntity >>(){}.getType();
                table = currenciesTable;
                break;
            case MoneyAccountsTable.NAME_TABLE:
                dataBaseOperations.deleteAll(moneyAccountsTable);
                type = new TypeToken< ArrayList< MoneyAccountEntity >>(){}.getType();
                table = moneyAccountsTable;
                break;
            case PaymentEventsTable.NAME_TABLE:
                dataBaseOperations.deleteAll(paymentEventsTable);
                type = new TypeToken< ArrayList< PaymentEventEntity >>(){}.getType();
                table = paymentEventsTable;
                break;
            case TransferBMATable.NAME_TABLE:
                dataBaseOperations.deleteAll(transferBMATable);
                type = new TypeToken< ArrayList< TransferBetweenMoneyAccountsEntity >>(){}.getType();
                table = transferBMATable;
                break;
            case FuelsTable.NAME_TABLE:
                dataBaseOperations.deleteAll(fuelsTable);
                type = new TypeToken< ArrayList<FuelEntity>>(){}.getType();
                table = fuelsTable;
                break;
            case TagTable.NAME_TABLE:
                dataBaseOperations.deleteAll(tagTable);
                type = new TypeToken< ArrayList<TagEntity>>(){}.getType();
                table = tagTable;
                break;
        }

        if( type != null){
            dbObjectEntities = gson.fromJson(json,type);
            dataBaseOperations.insertAll(table,dbObjectEntities);
        }
    }
}
