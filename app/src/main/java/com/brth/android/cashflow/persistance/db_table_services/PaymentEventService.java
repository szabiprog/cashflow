package com.brth.android.cashflow.persistance.db_table_services;

import android.content.Context;
import android.util.Log;

import com.brth.android.cashflow.R;
import com.brth.android.cashflow.data.enums.CurrencyRatioType;
import com.brth.android.cashflow.data.model.db_data_tables.PaymentEventsTable;
import com.brth.android.cashflow.data.model.db_datas_entities.CurrencyEntity;
import com.brth.android.cashflow.data.model.db_datas_entities.DbObjectEntity;
import com.brth.android.cashflow.data.model.db_datas_entities.PaymentEventEntity;
import com.brth.android.cashflow.data.model.db_datas_entities.TagEntity;
import com.brth.android.cashflow.view.controller.data.AverageFuelConsumption;
import com.brth.android.cashflow.view.controller.data.ElementFromEntity;
import com.brth.android.cashflow.view.controller.data.SelectedPeriodToView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class PaymentEventService extends DbService {

public static final String TAG = PaymentEventService.class.getSimpleName();
    private PaymentEventService() {
    }

    public static void revalidateTransferEvents(Context context) {
        List<DbObjectEntity> allPayments = allPayments(context);
        for (int i = 0; i < allPayments.size(); i++) {
            PaymentEventEntity p = (PaymentEventEntity) allPayments.get(i);
            if (p.getDescription().contains("transfer :")) {
                if (p.getValid() != -1) System.out.println(p);
                p.setValid(PaymentEventEntity.TRANSFER);
                PaymentEventService.update(p, context);
            }
        }
    }

    public static List<DbObjectEntity> allPayments(Context context) {
        initializing(context);
        return dataBaseOperations.selectAllDbObjectEntities(paymentEventsTable);
    }

    public static List<DbObjectEntity> allPaymentsByADate(String dateOfDay, Context context) {
        initializing(context);
        List<DbObjectEntity> list = dataBaseOperations.selectDbObjectEntities
                (paymentEventsTable, paymentEventsTable.DATE_COLUMN, dateOfDay + "%");
        return list;
    }


    public static List<DbObjectEntity> allPaymentsByMoneyAccId(String id, Context context) {
        initializing(context);
        List<DbObjectEntity> list = dataBaseOperations.selectDbObjectEntities
                (paymentEventsTable, paymentEventsTable.ACCOUNT_ID_COLUMN, id);
        return list;
    }


    public static List<DbObjectEntity> paymentsFromListByIdOfSelected
            (List<DbObjectEntity> payments, Long idBy, int selectBy, Context context) {
        List<DbObjectEntity> list = new ArrayList<>();
        for (int i = 0; i < payments.size(); i++) {
            PaymentEventEntity p = (PaymentEventEntity) payments.get(i);
            long idsInBinary;
            switch (selectBy) {
                case SelectedPeriodToView.LIST_BY_PROJECT:
                    idsInBinary = p.getProjectId();
                    break;
                case SelectedPeriodToView.LIST_BY_CURRENCY:
                    idsInBinary = p.getCurrencyId();
                    break;
                case SelectedPeriodToView.LIST_BY_MONEY_ACC:
                    idsInBinary = p.getMoneyAccountId();
                    break;
                default:
                    idsInBinary = -1;
                    break;
            }
            switch (selectBy) {
                case SelectedPeriodToView.LIST_BY_TAG:
                    idsInBinary = p.getTagIdsBin();
                    long binaryFromNormalId = (long) Math.pow(2, idBy);
                    if (idsInBinary > 0) {
                        if ((idsInBinary & binaryFromNormalId) == (binaryFromNormalId)) {
                            list.add(p);
                        }
                    } else {
                        if (idsInBinary == 0 && binaryFromNormalId == 1) {
                            list.add(p);
                        }
                    }
                    break;
                default:
                    if (idBy == idsInBinary) list.add(p);
                    break;
            }
        }
        return list;
    }


    public static long save(PaymentEventEntity paymentEventEntity, Context context) {
        initializing(context);
        long id = 0;
        try {
            id = dataBaseOperations.insert(paymentEventEntity, paymentEventsTable);
            if (paymentEventEntity.getValid() != 0) {
                if (MoneyAccountService.registeringToMoneyAccount(paymentEventEntity, context)) {
                    if (paymentEventEntity.getValid() > 0) {
                        ProjectService.paymentRegisterToProjectTree(context, paymentEventEntity);
                        paymentEventEntity.setValid(PaymentEventEntity.REGISTERED);
                        dataBaseOperations.update(paymentEventEntity, paymentEventsTable);
                    }
                }
            }
            return id;
        } catch (Exception e) {
            return 0;
        }
    }

    public static PaymentEventEntity clonePaymentEntityInverseDirection
            (PaymentEventEntity paymentEventEntity) {
        PaymentEventEntity oldPaymentEventEntity = new PaymentEventEntity();
        oldPaymentEventEntity.setDate(paymentEventEntity.getDate());
        oldPaymentEventEntity.setMoneyAccountId(paymentEventEntity.getMoneyAccountId());
        oldPaymentEventEntity.setValid(paymentEventEntity.getValid());
        oldPaymentEventEntity.setCurrencyId(paymentEventEntity.getCurrencyId());
        oldPaymentEventEntity.setProjectId(paymentEventEntity.getProjectId());
        oldPaymentEventEntity.setAmount(paymentEventEntity.getAmount());
        oldPaymentEventEntity.setDirectionType(paymentEventEntity.getDirectionType() * -1);
        return oldPaymentEventEntity;

    }

    public static boolean update(PaymentEventEntity paymentEventEntity, Context context) {
        initializing(context);
        return dataBaseOperations.update(paymentEventEntity, paymentEventsTable);
    }

    public static boolean update(PaymentEventEntity paymentEventEntity,
                                 PaymentEventEntity oldPaymentEventEntity, Context context) {
        initializing(context);
        boolean backwardsSuccess = false;
        if (paymentEventEntity.getValid() != -1)
            paymentEventEntity.setValid(PaymentEventEntity.REGISTERED);
        backwardsSuccess = dataBaseOperations.update(paymentEventEntity, paymentEventsTable);
        if (backwardsSuccess) {
            backwardsSuccess &= ProjectService.paymentUpdateRegisterToProjectTree(context, oldPaymentEventEntity, paymentEventEntity);
            backwardsSuccess &= MoneyAccountService.registeringToMoneyAccount(oldPaymentEventEntity, context);
            backwardsSuccess &= MoneyAccountService.registeringToMoneyAccount(paymentEventEntity, context);
        }
        return backwardsSuccess;
    }

    public static PaymentEventEntity selectById(long id, Context context) {
        initializing(context);
        PaymentEventEntity entity =
                (PaymentEventEntity) dataBaseOperations.selectById(paymentEventsTable, id);
        return entity;
    }

    public static long moving(Context context, List<DbObjectEntity> paymentEvents, int direction) {
        initializing(context);
        long amount = 0;
        for (int i = 0; i < paymentEvents.size(); i++) {
            PaymentEventEntity p = (PaymentEventEntity) paymentEvents.get(i);

            long cId = p.getCurrencyId();
            CurrencyEntity c = (CurrencyEntity) CurrencyService.selectById(cId, context);
            if (p.getValid() > 0) {
                if (p.getDirectionType() == direction) {
                    if (c.getRatioType() == CurrencyRatioType.valueOfCurrencyRatioType(CurrencyRatioType.BIGGER)) {
                        amount += p.getAmount() * Double.valueOf(c.getRatioString());
                    } else {
                        amount += p.getAmount() / Double.valueOf(c.getRatioString());
                    }
                }
            }
        }
        return amount;
    }

    public static int allMovingBySameCurrency(Context context, List<DbObjectEntity> paymentEvents, int direction) {
        initializing(context);
        int amount = 0;
        for (int i = 0; i < paymentEvents.size(); i++) {
            PaymentEventEntity p = (PaymentEventEntity) paymentEvents.get(i);
            if (p.getValid() != 0) {
                if (p.getDirectionType() == direction) {
                    amount += p.getAmount();
                }
            }
        }
        return amount;
    }

    public static long weeklyMoving(Context context, Calendar c, int direction) {
        Calendar calendar = (Calendar) c.clone();
        String[] daysOfThisWeek = new String[7];
        long saldo = 0;
        List<DbObjectEntity> paymentEvents;
        DateFormat format = new SimpleDateFormat("yyyy.MM.dd.");

        calendar.set(Calendar.DAY_OF_WEEK, calendar.getFirstDayOfWeek());

        for (int i = 0; i < 7; i++) {
            daysOfThisWeek[i] = format.format(calendar.getTime()) + "%";
            paymentEvents = dataBaseOperations.
                    selectDbObjectEntities(new PaymentEventsTable(context),
                            PaymentEventsTable.DATE_COLUMN, daysOfThisWeek[i]);
            saldo += PaymentEventService.moving(context, paymentEvents, direction);
            calendar.add(Calendar.DAY_OF_WEEK, 1);
        }
        return saldo;
    }

    public static long dailyMoving(Context context, SelectedPeriodToView selectedPeriodToView) {
        Calendar calendar = (Calendar) selectedPeriodToView.getMyCalendar().clone();
        initializing(context);
        List<DbObjectEntity> paymentEvents;
        String today;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd.");
        today = sdf.format(calendar.getTime()) + "%";

        paymentEvents = dataBaseOperations.
                selectDbObjectEntities(new PaymentEventsTable(context),
                        PaymentEventsTable.DATE_COLUMN, today);

        //paymentEvents = cleanPaymentsBySelected(selectedPeriodToView, paymentEvents);
        return PaymentEventService.moving(context, paymentEvents, selectedPeriodToView.getDirection());
    }

    public static long monthlyMoving(Context context, SelectedPeriodToView selectedPeriodToView) {
        Calendar calendar = (Calendar) selectedPeriodToView.getMyCalendar().clone();
        initializing(context);
        List<DbObjectEntity> paymentEvents;
        String today;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.");
        today = sdf.format(calendar.getTime()) + "%";
        paymentEvents = dataBaseOperations.
                selectDbObjectEntities(paymentEventsTable,
                        PaymentEventsTable.DATE_COLUMN, today);
        //paymentEvents = cleanPaymentsBySelected(selectedPeriodToView, paymentEvents);

        return PaymentEventService.moving(context, paymentEvents, selectedPeriodToView.getDirection());
    }

    private static List<DbObjectEntity> cleanPaymentsBySelected
            (SelectedPeriodToView selectedPeriodToView, List<DbObjectEntity> paymentEvents) {
        if (selectedPeriodToView.getListBy() > 0) {
            for (int i = 0; i < paymentEvents.size(); i++) {
                PaymentEventEntity p = (PaymentEventEntity) paymentEvents.get(i);
                switch (selectedPeriodToView.getListBy()) {
                    case SelectedPeriodToView.LIST_BY_PROJECT:
                        if (p.getProjectId() != selectedPeriodToView.getListById())
                            paymentEvents.remove(i--);
                        break;
                    case SelectedPeriodToView.LIST_BY_MONEY_ACC:
                        if (p.getMoneyAccountId() != selectedPeriodToView.getListById())
                            paymentEvents.remove(i--);
                        break;
                    case SelectedPeriodToView.LIST_BY_CURRENCY:
                        if (p.getCurrencyId() != selectedPeriodToView.getListById())
                            paymentEvents.remove(i--);
                        break;
                }
            }
        }
        return paymentEvents;
    }

    public static long yearlyMoving(Context context, Calendar c, int direction) {
        Calendar calendar = (Calendar) c.clone();
        initializing(context);
        List<DbObjectEntity> paymentEvents;
        String today;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy.");
        today = sdf.format(calendar.getTime()) + "%";
        paymentEvents = dataBaseOperations.
                selectDbObjectEntities(new PaymentEventsTable(context),
                        PaymentEventsTable.DATE_COLUMN, today);
        return PaymentEventService.moving(context, paymentEvents, direction);
    }

    public static List<SelectedPeriodToView> makeListOfMonthsOfSelectedYear
            (Context context, SelectedPeriodToView selectedPeriodToView) {
        List<SelectedPeriodToView> list = new ArrayList<>();
        DateFormat format = new SimpleDateFormat("yyyy.MM.dd.");
        Calendar calendar = (Calendar) selectedPeriodToView.getMyCalendar().clone();


        calendar.set(Calendar.MONTH, calendar.JANUARY);
        for (int i = 0; i < 12; i++) {
            SelectedPeriodToView a = new SelectedPeriodToView();
            a.setMyCalendar(calendar);
            a.setSelectedDay(format.format(calendar.getTime()));
            a.setPeriod(selectedPeriodToView.getPeriod());
            a.setDirection(1);
            a.setInnAmount(PaymentEventService.monthlyMoving(context, a));
            a.setDirection(-1);
            a.setOutAmount(PaymentEventService.monthlyMoving(context, a));
            list.add(a);
            calendar = (Calendar) calendar.clone();
            calendar.add(Calendar.MONTH, 1);
            //System.out.println(format.format(calendar.getTime()));
        }
        return list;
    }

    public static List<DbObjectEntity> allPaymentsBySelectedPeriod(Context context, List<SelectedPeriodToView> dates) {
        SelectedPeriodToView selectedPeriodToView = dates.get(0);
        int subStrValue = 11;
        switch (selectedPeriodToView.getPeriod()) {
            case SelectedPeriodToView.PERIOD_BY_MONTH:
                subStrValue = 8;
                break;
            case SelectedPeriodToView.PERIOD_BY_YEAR:
                subStrValue = 5;
                break;
        }
        List<DbObjectEntity> paymentEvents = new ArrayList<>();
        for (int i = 0; i < dates.size(); i++) {
            selectedPeriodToView = dates.get(i);
            String dateStr = selectedPeriodToView.getSelectedDay().substring(0, subStrValue);
            paymentEvents.addAll(allPaymentsByADate(dateStr, context));

        }
        return paymentEvents;

    }

    public static long movingInSameCurrency(Context context, List<DbObjectEntity> paymentEvents, int direction) {
        initializing(context);
        int amount = 0;
        for (int i = 0; i < paymentEvents.size(); i++) {
            PaymentEventEntity p = (PaymentEventEntity) paymentEvents.get(i);
            if (p.getValid() > 0) {
                if (p.getDirectionType() == direction) amount += p.getAmount();
            }
        }
        return amount;
    }

    public static long switchType(long paymentType) {
        switch ((int) paymentType) {
            case (int) PaymentEventEntity.FUEL_TYPE:
                return PaymentEventEntity.SERVICE_TYPE;
            case (int) PaymentEventEntity.NORMAL_TYPE:
                return PaymentEventEntity.FUEL_TYPE;
            default:
                return PaymentEventEntity.NORMAL_TYPE;
        }
    }

    public static long balance(long id, Context context) {
        List<DbObjectEntity> list = PaymentEventService.paymentEventsByProjectTree(id, context);
        long inn = PaymentEventService.moving(context, list, 1);
        long out = PaymentEventService.moving(context, list, -1);
        return inn - out;
    }

    public static List<DbObjectEntity> paymentEventsByProjectTree(long projectId, Context context) {
        initializing(context);
        List<Long> projects = ProjectService.projectTree(projectId, context);
        List<DbObjectEntity> paymentEvents = new ArrayList<>();
        for (int i = 0; i < projects.size(); i++) {
            paymentEvents.addAll(allPaymentsByProjectId(String.valueOf(projects.get(i)), context));
        }
        return paymentEvents;
    }

    public static List<DbObjectEntity> allPaymentsByProjectId(String id, Context context) {
        initializing(context);
        List<DbObjectEntity> list = dataBaseOperations.selectDbObjectEntities
                (paymentEventsTable, paymentEventsTable.OWNER_ID_COLUMN, id);
        return list;
    }

    public static List<DbObjectEntity> allValidPayments(Context context) {
        initializing(context);
        return dataBaseOperations.selectDbObjectEntities(paymentEventsTable, PaymentEventsTable.VALID_COLUMN, String.valueOf(PaymentEventEntity.REGISTERED));

    }

    public static List<SelectedPeriodToView> makeDayOfSelectedDay
            (Context context, SelectedPeriodToView selectedPeriodToView) {
        Calendar calendar = (Calendar) selectedPeriodToView.getMyCalendar().clone();
        return makeListOfPaymentByDays(selectedPeriodToView, calendar, 1, context);
    }

    public static List<SelectedPeriodToView> makeWeekOfSelectedDay
            (Context context, SelectedPeriodToView selectedPeriodToView) {
        Calendar calendar = (Calendar) selectedPeriodToView.getMyCalendar().clone();
        calendar.set(Calendar.DAY_OF_WEEK, calendar.getFirstDayOfWeek());
        calendar.add(Calendar.WEEK_OF_YEAR, 0);
        return makeListOfPaymentByDays(selectedPeriodToView, calendar, 7, context);
    }

    public static List<SelectedPeriodToView> makeListOfDayBySelectedMonth(Context context, SelectedPeriodToView selectedPeriodToView) {
        List<SelectedPeriodToView> list = new ArrayList<>();
        Calendar calendar = (Calendar) (selectedPeriodToView.getMyCalendar()).clone();
        calendar.set(calendar.DAY_OF_MONTH, 1);
        return makeListOfPaymentByDays
                (selectedPeriodToView, calendar, calendar.getActualMaximum(calendar.DAY_OF_MONTH), context);
    }

    private static List<SelectedPeriodToView> makeListOfPaymentByDays
            (SelectedPeriodToView selectedPeriodToView, Calendar calendar,
             int numberOfDays, Context context) {
        List<SelectedPeriodToView> list = new ArrayList<>();
        for (int i = 0; i < numberOfDays; i++) {

            SelectedPeriodToView a = new SelectedPeriodToView();
            a.setMyCalendar(calendar);
            a.resetSelectedDay();
            a.setPeriod(selectedPeriodToView.getPeriod());
            a.setListBy(selectedPeriodToView.getListBy());
            if (a.getListBy() != SelectedPeriodToView.LIST_BY_DEFAULT)
                a.setListById(selectedPeriodToView.getListById());
            a.setDirection(1);
            a.setInnAmount(PaymentEventService.dailyMoving(context, a));
            a.setDirection(-1);
            a.setOutAmount(PaymentEventService.dailyMoving(context, a));
            list.add(a);
            calendar = (Calendar) calendar.clone();
            calendar.add(Calendar.DAY_OF_WEEK, 1);
        }
        return list;
    }

    public static List<ElementFromEntity> makeListOfElementFromEntity
            (Context context, SelectedPeriodToView selectedPeriodToView) {
        initializing(context);
        List<SelectedPeriodToView> dates = new ArrayList<>();
        if (selectedPeriodToView.getPeriod() == SelectedPeriodToView.PERIOD_BY_WEEK) {
            dates = makeWeekOfSelectedDay(context, selectedPeriodToView);
        } else {
            dates.add(selectedPeriodToView);
        }
        List<DbObjectEntity> paymentEventEntities;
        switch (selectedPeriodToView.getListBy()) {
            case SelectedPeriodToView.LIST_BY_PROJECT:
                paymentEventEntities = PaymentEventService.allPaymentsBySelectedPeriod(context, dates);
                List<DbObjectEntity> projectEntities = ProjectService.allProjects(context);
                return makeListBy(selectedPeriodToView, paymentEventEntities, projectEntities, context);

            case SelectedPeriodToView.LIST_BY_MONEY_ACC:
                paymentEventEntities = PaymentEventService.allPaymentsBySelectedPeriod(context, dates);
                List<DbObjectEntity> moneyAccEntities = MoneyAccountService.allMoneyAcc(context);
                return makeListBy(selectedPeriodToView, paymentEventEntities, moneyAccEntities, context);

            case SelectedPeriodToView.LIST_BY_CURRENCY:
                paymentEventEntities = PaymentEventService.allPaymentsBySelectedPeriod(context, dates);
                List<DbObjectEntity> currencyEntities = CurrencyService.allCurrencies(context);
                return makeListBy(selectedPeriodToView, paymentEventEntities, currencyEntities, context);

            case SelectedPeriodToView.LIST_BY_TAG:
                paymentEventEntities = PaymentEventService.allPaymentsBySelectedPeriod(context, dates);
                List<DbObjectEntity> tagEntities = TagService.getAllTagEntities(context);
                TagEntity unNamed = new TagEntity();
                unNamed.setName(context.getString(R.string.untagged));
                unNamed.setId(0);
                tagEntities.add(unNamed);
                return makeListBy(selectedPeriodToView, paymentEventEntities, tagEntities, context);

            default:
                return null;
        }
    }

    public static PaymentEventEntity createPaymentEntity(String currentDateTime) {
        PaymentEventEntity paymentEventEntity = new PaymentEventEntity();
        paymentEventEntity.setId(-1);
        paymentEventEntity.setFuelId(-1);
        paymentEventEntity.setTagIdsBin(0);
        paymentEventEntity.setQuantity("");
        paymentEventEntity.setServiceId(-1);
        paymentEventEntity.setMoneyAccountId(-1);
        paymentEventEntity.setCurrencyId(-1);
        paymentEventEntity.setType(PaymentEventEntity.NORMAL_TYPE);
        paymentEventEntity.setValid(PaymentEventEntity.VALID);
        paymentEventEntity.setDate(currentDateTime);
        paymentEventEntity.setDirectionType(1);
        return paymentEventEntity;
    }

    public static List<List<DbObjectEntity>>
    separateListsByTag(Context context, List<DbObjectEntity> paymentEventEntities) {
        initializing(context);
        List<List<DbObjectEntity>> lists = new ArrayList<>();
        Set<Long> tagIds = new HashSet<>();
        System.out.println("-------------------------------");
        for (int i = 0; i < paymentEventEntities.size(); i++) {
            PaymentEventEntity entity = (PaymentEventEntity) paymentEventEntities.get(i);
            long tagId = entity.getTagIdsBin();
            if (tagIds.add(tagId)) {
                TagEntity tag = TagService.selectById(tagId, context);
                if (tag != null) System.out.println(tag.getName());
                else System.out.println("---");
            }

        }
        return lists;
    }

    private static List<ElementFromEntity> makeListBy
            (SelectedPeriodToView selectedPeriodToView, List<DbObjectEntity> paymentEventEntities,
             List<DbObjectEntity> selectedByList, Context context) {
        List<ElementFromEntity> elementFromEntities = new ArrayList<>();
        long inn, out;
        for (int j = 0; j < selectedByList.size(); j++) {

            DbObjectEntity bySelectDbObjectEntity = selectedByList.get(j);
            long id = bySelectDbObjectEntity.getId();
            ElementFromEntity elementFromEntity = new ElementFromEntity(id, bySelectDbObjectEntity.getName());
            List<DbObjectEntity> plist =
                    PaymentEventService.paymentsFromListByIdOfSelected
                            (paymentEventEntities, id, selectedPeriodToView.getListBy(), context);
            if (plist.size() > 0) {
                elementFromEntity.setPaymentEventEntities(plist);
                if (selectedPeriodToView.getListBy() == selectedPeriodToView.LIST_BY_CURRENCY ||
                        selectedPeriodToView.getListBy() == selectedPeriodToView.LIST_BY_MONEY_ACC) {
                    inn = PaymentEventService.movingInSameCurrency(context, plist, 1);
                    out = PaymentEventService.movingInSameCurrency(context, plist, -1);
                } else {
                    inn = PaymentEventService.moving(context, plist, 1);
                    out = PaymentEventService.moving(context, plist, -1);
                }
                long balance = inn - out;
                elementFromEntity.setInn(inn);
                elementFromEntity.setOut(out);
                elementFromEntity.setBalance(balance);
                elementFromEntities.add(elementFromEntity);
            }
        }
        return elementFromEntities;
    }

    public static List<AverageFuelConsumption> listOfAverageFuelConsumption(Context context) {
        initializing(context);
        List<DbObjectEntity> allFuellingTypePayment = dataBaseOperations.selectDbObjectEntities
                (paymentEventsTable, paymentEventsTable.TYPE_COLUMN, String.valueOf(PaymentEventEntity.FUEL_TYPE));

        List<AverageFuelConsumption> averageFuelConsumptions = new ArrayList<>();

        for (int i = allFuellingTypePayment.size() - 1; i >= 0; i--) {
            PaymentEventEntity paymentEventEntity = (PaymentEventEntity) allFuellingTypePayment.get(i);
            if (goodForFuelType(paymentEventEntity)) {
                AverageFuelConsumption aNew = new AverageFuelConsumption(paymentEventEntity);
                int indexOf = averageFuelConsumptions.lastIndexOf(aNew);
                if (indexOf >= 0) {
                    AverageFuelConsumption afc = averageFuelConsumptions.get(indexOf);
                    afc.addFuelling(paymentEventEntity);
                } else {
                    averageFuelConsumptions.add(aNew);
                }
            }
        }
        return averageFuelConsumptions;
    }


    public static boolean goodForFuelType(PaymentEventEntity paymentEventEntity) {
        if(paymentEventEntity.getType() == PaymentEventEntity.FUEL_TYPE){
            try {
                if  ( Double.valueOf(paymentEventEntity.getQuantity() ) > 0 ) return true;

            }catch (Exception e){
                Log.e(TAG, "goodForFuelType: " + paymentEventEntity );
                Log.e(TAG, "goodForFuelType: ",e );
            }
            return false;
        }
            // to do : drop up to edit the paymentEventEntity
            return true;
        }

}
