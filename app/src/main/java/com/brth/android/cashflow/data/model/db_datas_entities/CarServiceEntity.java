package com.brth.android.cashflow.data.model.db_datas_entities;

public class CarServiceEntity extends DbObjectEntity {

    private  long paymentId;
    private  String dateOfNextService;
    private  long odometerInterval;
    private  long valid;

    public long getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(long paymentId) {
        this.paymentId = paymentId;
    }

    public String getDateOfNextService() {
        return dateOfNextService;
    }

    public void setDateOfNextService(String dateOfNextService) {
        this.dateOfNextService = dateOfNextService;
    }

    public long getOdometerInterval() {
        return odometerInterval;
    }

    public void setOdometerInterval(long odometerInterval) {
        this.odometerInterval = odometerInterval;
    }

    public long getValid() {
        return valid;
    }

    public void setValid(long valid) {
        this.valid = valid;
    }
}

