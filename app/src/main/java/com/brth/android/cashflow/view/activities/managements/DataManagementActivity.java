package com.brth.android.cashflow.view.activities.managements;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;

import com.brth.android.cashflow.R;
import com.brth.android.cashflow.data.model.db_data_tables.CurrenciesTable;
import com.brth.android.cashflow.data.model.db_data_tables.DbTable;
import com.brth.android.cashflow.data.model.db_data_tables.FuelsTable;
import com.brth.android.cashflow.data.model.db_data_tables.MoneyAccountsTable;
import com.brth.android.cashflow.data.model.db_data_tables.ProjectsTable;
import com.brth.android.cashflow.data.model.db_data_tables.TagTable;
import com.brth.android.cashflow.data.model.db_data_tables.TransferBMATable;
import com.brth.android.cashflow.view.activities.CurrenciesActivity;
import com.brth.android.cashflow.view.activities.HaveOnlyNameActivity;
import com.brth.android.cashflow.view.activities.MainActivity;
import com.brth.android.cashflow.view.activities.MoneyAccountsActivity;
import com.brth.android.cashflow.view.activities.ProjectsActivity;
import com.brth.android.cashflow.view.activities.TransfersActivity;
import com.brth.android.cashflow.view.controller.adapters.DataPumperFromDb;

import java.util.List;

public class DataManagementActivity extends AppCompatActivity {
    ListView listViewDbTables;

    ArrayAdapter<DbTable> arrayAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_management);

    }
    @Override
    protected void onResume() {
        super.onResume();
        setUpAdapter();
    }
    private void setUpAdapter() {
        List<DbTable> dbTables = DataPumperFromDb.dataTablesPumper(this);


        arrayAdapter=new ArrayAdapter<>
                (this, R.layout.list_white_text,R.id.list_content, dbTables);

        listViewDbTables =findViewById(R.id.lv_data_management);
        listViewDbTables.setAdapter(arrayAdapter);

        listViewDbTables.setOnItemClickListener(new MyItemClickListener());
        registerForContextMenu(listViewDbTables);
    }


    private class MyItemClickListener implements AdapterView.OnItemClickListener{

        @Override
        synchronized   public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            Intent intent;
            DbTable selectedTable = arrayAdapter.getItem(position);
            if(selectedTable!=null){
                if(selectedTable.getClass().equals(ProjectsTable.class)){
                    intent = new Intent(DataManagementActivity.this, ProjectsActivity.class);
                    startActivity(intent);
                }
                if(selectedTable.getClass().equals(CurrenciesTable.class)){
                    intent = new Intent(DataManagementActivity.this, CurrenciesActivity.class);
                    startActivity(intent);
                }
                if(selectedTable.getClass().equals(MoneyAccountsTable.class)){
                    intent = new Intent(DataManagementActivity.this, MoneyAccountsActivity.class);
                    startActivity(intent);
                }
                if(selectedTable.getClass().equals(TransferBMATable.class)){
                    intent = new Intent(DataManagementActivity.this, TransfersActivity.class);
                    startActivity(intent);
                }
                if(selectedTable.getClass().equals(FuelsTable.class)){
                    intent = new Intent(DataManagementActivity.this, HaveOnlyNameActivity.class);
                    intent.putExtra("selected", "fuels");
                    startActivity(intent);
                }
                if(selectedTable.getClass().equals(TagTable.class)){
                    intent = new Intent(DataManagementActivity.this, HaveOnlyNameActivity.class);
                    intent.putExtra("selected", "tags");
                    startActivity(intent);
                }
            }

        }
    }
    @Override
    public void onBackPressed() {
        Intent intent = new Intent
                (this, MainActivity.class);
        this.startActivity(intent);
        this.finish();
    }

}
