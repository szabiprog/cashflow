package com.brth.android.cashflow.view.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.brth.android.cashflow.R;
import com.brth.android.cashflow.data.model.db_datas_entities.DbObjectEntity;
import com.brth.android.cashflow.data.model.db_datas_entities.PaymentEventEntity;
import com.brth.android.cashflow.persistance.db_table_services.CurrencyService;
import com.brth.android.cashflow.persistance.db_table_services.MoneyAccountService;
import com.brth.android.cashflow.persistance.db_table_services.ProjectService;
import com.brth.android.cashflow.view.activities.managements.MoneyAccountManagementActivity;
import com.brth.android.cashflow.view.controller.adapters.MoneyAccountsArrayAdapter;

import java.text.NumberFormat;
import java.util.List;

public class MoneyAccountsActivity extends AppCompatActivity {

    private ArrayAdapter<DbObjectEntity> arrayAdapter;
    private ListView listViewDbTables;
    private TextView headerTv;
    private TextView orderTv;
    private Button button;
    private String toSelect;

    private PaymentEventEntity oldPaymentEventEntity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        Intent intent = getIntent();
        toSelect = intent.getStringExtra("select");
        headerTv = findViewById(R.id.tv_header);
        orderTv = findViewById(R.id.tv_order);
        button = findViewById(R.id.bt_new_item);
        findViewById(R.id.iv_back_empty_list).setVisibility(View.GONE);
        if (toSelect == null) {
            headerTv.setText(R.string.money_accounts);
            button.setText(R.string.new_money_account);
            orderTv.setVisibility(View.VISIBLE);
            StringBuffer text = new StringBuffer();
            text.append(getString(R.string.balance));
            text.append(NumberFormat.getInstance().format
                    (MoneyAccountService. balanceOfMoneyAccounts(this)));
            orderTv.setText(text);
        } else {
            try {
                oldPaymentEventEntity = (PaymentEventEntity) intent.getExtras().get("old");
            } catch (Exception e) {
                oldPaymentEventEntity = null;
            }
            headerTv.setText(R.string.select_money_account);
            button.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        setUpAdapter();
    }

    private void setUpAdapter() {
        List<DbObjectEntity> currencies = CurrencyService.allCurrencies(this);
        List<DbObjectEntity> projectEntities =
                ProjectService.selectAllByOwnerId("0", this);
        if (!projectEntities.isEmpty() && !currencies.isEmpty()) {
            List<DbObjectEntity> moneyAcconts = MoneyAccountService.allMoneyAcc(this);
            arrayAdapter = new MoneyAccountsArrayAdapter(this, moneyAcconts);

            listViewDbTables = findViewById(R.id.lv_items);
            listViewDbTables.setAdapter(arrayAdapter);
            listViewDbTables.setLongClickable(true);
            if (toSelect != null)
                listViewDbTables.setOnItemClickListener(new SelectItemClickListener());
            else {
                listViewDbTables.setOnItemClickListener(new itemClickListenerToList());
                listViewDbTables.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                    @Override
                    public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
                                                   int pos, long id) {
                        String name = ((TextView) arg1.findViewById(R.id.name)).getText().toString();
                        Intent intent = new Intent
                                (MoneyAccountsActivity.this, MoneyAccountManagementActivity.class);
                        intent.putExtra("name", name);
                        MoneyAccountsActivity.this.startActivity(intent);
                        return true;
                    }
                });
            }

        } else {
            Toast toast = Toast.makeText(this, R.string.make_project_and_currency_first, Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 0, 0);
            toast.show();
            this.onBackPressed();
        }

    }

    public void onClick(View view) {
        Intent intent;
        switch (view.getId()) {
            case R.id.bt_new_item:

                intent = new Intent(this, MoneyAccountManagementActivity.class);
                startActivity(intent);
                break;
        }
    }


    private class SelectItemClickListener implements AdapterView.OnItemClickListener {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            Intent data = new Intent();
            data.putExtra("name",
                    ((TextView) view.findViewById(R.id.name)).getText().toString());
            data.putExtra("old",oldPaymentEventEntity);
            setResult(Activity.RESULT_OK, data);
            finish();
        }
    }

    private class itemClickListenerToList implements AdapterView.OnItemClickListener {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            String accId = String.valueOf(arrayAdapter.getItem(position).getId());
            Intent intent = new Intent
                    (MoneyAccountsActivity.this, PaymentEventsActivity.class);
            intent.putExtra("accId",
                    accId);
            MoneyAccountsActivity.this.startActivity(intent);
        }
    }
}