package com.brth.android.cashflow.data.model.db_data_tables;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.brth.android.cashflow.R;
import com.brth.android.cashflow.data.model.db_datas_entities.DbObjectEntity;
import com.brth.android.cashflow.data.model.db_datas_entities.TransferBetweenMoneyAccountsEntity;

import java.util.ArrayList;
import java.util.List;

public class TransferBMATable extends DbTable {


    public final static String NAME_TABLE = "transfer_between_money_accounts";
    public final static String ID_COLUMN = "id";
    public final static String DATE_COLUMN = "date";
    public final static String FROM_ID_COLUMN = "from_id";
    public final static String TO_ID_COLUMN = "to_id";
    public final static String FROM_AMOUNT_COLUMN = "from_amount";
    public final static String TO_AMOUNT_COLUMN = "to_amount";
    public final static String STATUS_COLUMN = "status";
    public final static String FROM_PAYMENT_ID_COLUMN = "from_payment_id";
    public final static String TO_PAYMENT_ID_COLUMN = "to_payment_id";

    public final static String[] ALL_COLUMN =
            {ID_COLUMN, DATE_COLUMN, FROM_ID_COLUMN, TO_ID_COLUMN,
                    FROM_AMOUNT_COLUMN, TO_AMOUNT_COLUMN, STATUS_COLUMN,
                    FROM_PAYMENT_ID_COLUMN, TO_PAYMENT_ID_COLUMN};

    public TransferBMATable(Context context) {
        super(context);
    }


    @Override
    public long insert(DbObjectEntity dbObjectEntity, SQLiteDatabase database) {
        TransferBetweenMoneyAccountsEntity p = (TransferBetweenMoneyAccountsEntity) dbObjectEntity;

        long index = database.insert(NAME_TABLE, null, dbObjectEntityToContentValues(p));
        p.setId(index);

        return index;

    }

    @Override
    public boolean update(DbObjectEntity dbObjectEntity, SQLiteDatabase database) {
        long r = database.update(NAME_TABLE, dbObjectEntityToContentValues(dbObjectEntity),
                ID_COLUMN + "=" + dbObjectEntity.getId(), null);
        return r == 1;
    }

    @Override
    public int delete(DbObjectEntity dbObjectEntity, SQLiteDatabase database) {
        return 0;
    }

    @Override
    public List<DbObjectEntity> selectAllDbObjectEntities(SQLiteDatabase database) {
        Cursor cursor;

        try {
            cursor = database.query(NAME_TABLE, ALL_COLUMN, null,
                    null, null, null, DATE_COLUMN + " desc");
        } catch (Exception e) {
            database.execSQL("ALTER TABLE " + NAME_TABLE + " ADD COLUMN " + FROM_PAYMENT_ID_COLUMN + " INTEGER DEFAULT 0");
            database.execSQL("ALTER TABLE " + NAME_TABLE + " ADD COLUMN " + TO_PAYMENT_ID_COLUMN + " INTEGER DEFAULT 0");
            cursor = database.query(NAME_TABLE, ALL_COLUMN, null,
                    null, null, null, DATE_COLUMN + " desc");
        }

        List<DbObjectEntity> dbObjectEntityList = new ArrayList<>();
        while (cursor.moveToNext()) {
            DbObjectEntity dbObjectEntity = cursorToDbObjectEntity(cursor);

            dbObjectEntityList.add(dbObjectEntity);

        }
        return dbObjectEntityList;
    }

    @Override
    public DbObjectEntity cursorToDbObjectEntity(Cursor cursor) {
        TransferBetweenMoneyAccountsEntity moneyAccountEntity = new TransferBetweenMoneyAccountsEntity();
        moneyAccountEntity.setId(cursor.getLong(0));
        moneyAccountEntity.setDate(cursor.getString(1));
        moneyAccountEntity.setFromAccId(cursor.getLong(2));
        moneyAccountEntity.setToAccId(cursor.getLong(3));
        moneyAccountEntity.setFromAmount(cursor.getLong(4));
        moneyAccountEntity.setToAmount(cursor.getLong(5));
        moneyAccountEntity.setStatus(cursor.getLong(6));
        moneyAccountEntity.setFromPaymentEventId(cursor.getLong(7));
        moneyAccountEntity.setToPaymentEventId(cursor.getLong(8));
        return moneyAccountEntity;
    }

    @Override
    public ContentValues dbObjectEntityToContentValues(DbObjectEntity dbObjectEntity) {
        TransferBetweenMoneyAccountsEntity p = (TransferBetweenMoneyAccountsEntity) dbObjectEntity;

        ContentValues values = new ContentValues();
        values.put(DATE_COLUMN, p.getDate());
        values.put(FROM_ID_COLUMN, p.getFromAccId());
        values.put(TO_ID_COLUMN, p.getToAccId());
        values.put(FROM_AMOUNT_COLUMN, p.getFromAmount());
        values.put(TO_AMOUNT_COLUMN, p.getToAmount());
        values.put(STATUS_COLUMN, p.getStatus());
        values.put(FROM_PAYMENT_ID_COLUMN, p.getFromPaymentEventId());
        values.put(TO_PAYMENT_ID_COLUMN, p.getToPaymentEventId());
        return values;
    }

    @Override
    public List<DbObjectEntity> selectDbObjectEntities(SQLiteDatabase database, String inColums, String values) {
        Cursor cursor;

        cursor = database.query(NAME_TABLE, ALL_COLUMN, inColums + "=?", new String[]{values}
                , null, null, ID_COLUMN + " asc");
        List<DbObjectEntity> dbObjectEntityList = new ArrayList<>();
        while (cursor.moveToNext()) {
            DbObjectEntity dbObjectEntity = cursorToDbObjectEntity(cursor);
            dbObjectEntityList.add(dbObjectEntity);
        }
        return dbObjectEntityList;
    }

    @Override
    public DbObjectEntity selectById(Long id, SQLiteDatabase database) {

        List<DbObjectEntity> dbObjectEntityList = new ArrayList<>();
        if (database.isOpen()) {
            Cursor cursor;
            cursor = database.query(NAME_TABLE, ALL_COLUMN, ID_COLUMN + "=?", new String[]{id + ""}
                    , null, null, ID_COLUMN + " asc");
            while (cursor.moveToNext()) {
                DbObjectEntity dbObjectEntity = cursorToDbObjectEntity(cursor);
                dbObjectEntityList.add(dbObjectEntity);
            }
        }
        if (!dbObjectEntityList.isEmpty()) return dbObjectEntityList.get(0);
        else return null;
    }

    @Override
    public String createTable() {
        return "CREATE TABLE IF NOT EXISTS " + NAME_TABLE + " (" +
                ID_COLUMN + " integer primary key autoincrement, " + DATE_COLUMN + " text not null, "
                + FROM_ID_COLUMN + " integer default 0, " + TO_ID_COLUMN + " integer default 0,"
                + FROM_AMOUNT_COLUMN + " integer default 0, " + TO_AMOUNT_COLUMN + " integer default 0, "
                + STATUS_COLUMN + " integer default 0, "
                + FROM_PAYMENT_ID_COLUMN + " integer default 0 , "
                + TO_PAYMENT_ID_COLUMN + " integer default 0 );";
    }

    @Override
    public String getTableName() {
        return NAME_TABLE;
    }
    @Override
    public void deleteAll( SQLiteDatabase database)
    {
        database.execSQL("delete from "+ NAME_TABLE);
        database.execSQL(String.format("DELETE FROM SQLITE_SEQUENCE WHERE NAME = '%s'", NAME_TABLE));
    }
    @Override
    public String toString() {
        return getContext().getString(R.string.transfers);
    }

}