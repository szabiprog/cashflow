package com.brth.android.cashflow.view.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.brth.android.cashflow.R;
import com.brth.android.cashflow.data.model.db_datas_entities.DbObjectEntity;
import com.brth.android.cashflow.persistance.db_table_services.PaymentEventService;
import com.brth.android.cashflow.view.activities.managements.PaymentEventManagementActivity;
import com.brth.android.cashflow.view.controller.adapters.PaymentEventArrayAdatpter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class PaymentTableActivity extends AppCompatActivity {

    private ListView listViewDbTables;
    private ArrayAdapter<DbObjectEntity> arrayAdapter;
    private TextView header;
    private String toSelect;
    private String period;
    private String accId;
    private Intent arrivedIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.table_base);
        header = findViewById(R.id.tv_header);

        arrivedIntent = getIntent();
        toSelect = arrivedIntent.getStringExtra("select");
        period = arrivedIntent.getStringExtra("period");
        accId = arrivedIntent.getStringExtra("accId");
        if(period==null)period="";

        if (toSelect == null) {
            header.setText(R.string.payment_events);

        } else {
            header.setText(R.string.select_payment);

        }
    }

    @Override
    protected void onResume() {

        super.onResume();
        setUpAdapter();
    }

    private void setUpAdapter() {

        List<DbObjectEntity> paymentEvents= new ArrayList<>();
        switch (period){
            case "today":
                String today;
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd.");
                today = sdf.format(new Date())+"%";
                paymentEvents = PaymentEventService.allPaymentsByADate(today,this);
                break;
            case "week":
                String[] daysOfThisWeek = arrivedIntent.getStringArrayExtra("daysOfThisWeek");
                paymentEvents= new ArrayList<>();
                for(int i = 0;i<daysOfThisWeek.length;i++){
                    paymentEvents.addAll( PaymentEventService.allPaymentsByADate(daysOfThisWeek[i],this));
                }
                break;
            case "":
                if (accId != null) {
                    paymentEvents = PaymentEventService.allPaymentsByMoneyAccId(accId,this);
                }else{
                    paymentEvents = PaymentEventService.allPayments(this);
                }
                break;
        }


        arrayAdapter = new PaymentEventArrayAdatpter(this, paymentEvents);
        listViewDbTables = findViewById(R.id.lv_items);
        listViewDbTables.setAdapter(arrayAdapter);

        listViewDbTables.setLongClickable(true);
        if (toSelect != null) listViewDbTables.setOnItemClickListener(new PaymentTableActivity.MyItemClickListener());
        else
            listViewDbTables.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                @Override
                public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
                                               int pos, long id) {
                    String date = ((TextView) arg1.findViewById(R.id.tv_currentDate)).getText().toString();
                    Intent intent = new Intent
                            (PaymentTableActivity.this, PaymentEventManagementActivity.class);
                    intent.putExtra("date", date);
                    PaymentTableActivity.this.startActivity(intent);
                    return true;
                }
            });
    }

    public void onClick(View view) {
        Intent intent;
        switch (view.getId()) {
            case R.id.bt_new_item:
                intent = new Intent(this, PaymentEventManagementActivity.class);
                startActivity(intent);
                break;
        }
    }

    private class MyItemClickListener implements AdapterView.OnItemClickListener {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Intent data = new Intent();
            data.putExtra("name",
                    ((TextView) view.findViewById(R.id.name)).getText().toString());
            setResult(Activity.RESULT_OK, data);
            finish();
        }
    }

}
