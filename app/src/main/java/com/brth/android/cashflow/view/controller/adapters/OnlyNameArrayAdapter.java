package com.brth.android.cashflow.view.controller.adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.brth.android.cashflow.R;
import com.brth.android.cashflow.data.model.db_datas_entities.DbObjectEntity;
import com.brth.android.cashflow.data.model.db_datas_entities.TagEntity;

import java.util.List;
import java.util.Set;

public class OnlyNameArrayAdapter extends ArrayAdapter<DbObjectEntity> {

    private final Activity context;
    private final List<DbObjectEntity> items;
    private Set<Long> selectedIds;

    public OnlyNameArrayAdapter(Activity context, List<DbObjectEntity> items) {
        super(context, R.layout.row_only_name, items);
        this.context = context;
        this.items = items;
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {

        LayoutInflater inflater = context.getLayoutInflater();
        @SuppressLint({"ViewHolder", "InflateParams"})
        View rowView = inflater.inflate(R.layout.row_only_name, null, true);
        TextView name = rowView.findViewById(R.id.name);
        DbObjectEntity entity =  items.get(position);
        name.setText(entity.getName());
        if ( entity instanceof  TagEntity ) {
            if (selectedIds.contains(entity.getId())) {
              name.setText(entity.getName()+" ✓");
            }
        }
        return rowView;
    }

    public void setSelectedIds(Set<Long> selectedIds) {
        this.selectedIds = selectedIds;
    }
}