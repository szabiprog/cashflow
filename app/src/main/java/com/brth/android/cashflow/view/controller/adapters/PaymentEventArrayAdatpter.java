package com.brth.android.cashflow.view.controller.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.brth.android.cashflow.R;
import com.brth.android.cashflow.data.model.db_datas_entities.CurrencyEntity;
import com.brth.android.cashflow.data.model.db_datas_entities.DbObjectEntity;
import com.brth.android.cashflow.data.model.db_datas_entities.MoneyAccountEntity;
import com.brth.android.cashflow.data.model.db_datas_entities.PaymentEventEntity;
import com.brth.android.cashflow.data.model.db_datas_entities.ProjectEntity;
import com.brth.android.cashflow.persistance.db_table_services.CurrencyService;
import com.brth.android.cashflow.persistance.db_table_services.MoneyAccountService;
import com.brth.android.cashflow.persistance.db_table_services.ProjectService;
import com.brth.android.cashflow.persistance.db_table_services.TagService;

import java.text.NumberFormat;
import java.util.List;

public class PaymentEventArrayAdatpter extends ArrayAdapter<DbObjectEntity> {

    private final Activity context;
    private final List<DbObjectEntity> items;

    public PaymentEventArrayAdatpter(Activity context, List<DbObjectEntity> items) {
        super(context, R.layout.row_payment_event, items);
        this.context = context;
        this.items = items;

    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {

        LayoutInflater inflater = context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.row_payment_event, null, true);

        PaymentEventEntity paymentEventEntity = (PaymentEventEntity) items.get(position);
        ProjectEntity projectEntity =
                ProjectService.selectById (paymentEventEntity.getProjectId(),context);
        MoneyAccountEntity moneyAccountEntity =
                MoneyAccountService.selectById( paymentEventEntity.getMoneyAccountId(),context);
        CurrencyEntity currencyEntity =
                CurrencyService.selectById(moneyAccountEntity.getCurrencyId(),context);
        List<DbObjectEntity> tagEntities =
                TagService.selectByIdBinary(paymentEventEntity.getTagIdsBin(),context);

        TextView tvDate = rowView.findViewById(R.id.tv_currentDate);
        TextView tvDirection = rowView.findViewById(R.id.tv_direction);
        TextView tvAmount = rowView.findViewById(R.id.tv_amount);
        TextView tvCurrency = rowView.findViewById(R.id.tv_currency_name);
        TextView tvMoneyAcc = rowView.findViewById(R.id.tv_money_acc);
        TextView tvProject = rowView.findViewById(R.id.tv_project);
        TextView tvDescription = rowView.findViewById(R.id.tv_description);
        TextView tvTag = rowView.findViewById(R.id.tv_tag);


        TextView id = rowView.findViewById(R.id.tv_id);
        id.setVisibility(View.VISIBLE);
        id.setText(String.valueOf(items.size()-position)+".");

        if(paymentEventEntity.getDirectionType()>0){
            tvDirection.setText("+");
        }else tvDirection.setText("-");

        tvDate.setText(paymentEventEntity.getDate());

        tvAmount.setText(NumberFormat.getInstance().format(paymentEventEntity.getAmount()));
        tvCurrency.setText(currencyEntity.getName());
        tvMoneyAcc.setText(moneyAccountEntity.getName());

        tvDescription.setText(paymentEventEntity.getDescription());
        if(tagEntities.size()>0){
            StringBuilder text = new StringBuilder();
            for (DbObjectEntity entity: tagEntities) {
                text.append(entity.getName()+",");
            }
            text.replace(text.lastIndexOf(","),text.lastIndexOf(",")+1,"");
            tvTag.setText(text.toString());
        }else  tvTag.setText("-");

        if(projectEntity!=null){
            tvProject.setText(projectEntity.getName());
        }
        return rowView;
    }
}
