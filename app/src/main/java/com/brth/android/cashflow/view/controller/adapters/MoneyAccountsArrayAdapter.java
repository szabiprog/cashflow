package com.brth.android.cashflow.view.controller.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.brth.android.cashflow.R;
import com.brth.android.cashflow.data.model.db_datas_entities.CurrencyEntity;
import com.brth.android.cashflow.data.model.db_datas_entities.DbObjectEntity;
import com.brth.android.cashflow.data.model.db_datas_entities.MoneyAccountEntity;
import com.brth.android.cashflow.data.model.db_datas_entities.ProjectEntity;
import com.brth.android.cashflow.persistance.db_table_services.CurrencyService;
import com.brth.android.cashflow.persistance.db_table_services.ProjectService;

import java.text.NumberFormat;
import java.util.List;

public class MoneyAccountsArrayAdapter extends ArrayAdapter<DbObjectEntity> {

    private final Activity context;
    private final List<DbObjectEntity> items;

    public MoneyAccountsArrayAdapter(Activity context, List<DbObjectEntity> items) {
        super(context, R.layout.row_money_account, items);
        this.context = context;
        this.items = items;

    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.row_money_account, null, true);
        TextView name = rowView.findViewById(R.id.name);
        TextView ownerProject = rowView.findViewById(R.id.owner_project);
        ownerProject.setVisibility(View.GONE);
        TextView currency = rowView.findViewById(R.id.name_of_currency);
        TextView balance = rowView.findViewById(R.id.balance);

        MoneyAccountEntity moneyAccountEntity = (MoneyAccountEntity) items.get(position);

        name.setText(moneyAccountEntity.getName());
        //ownerProject.setText(getProjectName(moneyAccountEntity.getProjectId()));
        currency.setText(getCurrencyName(moneyAccountEntity.getCurrencyId()));
        balance.setText(NumberFormat.getInstance().format( moneyAccountEntity.getBalance()));
    /*    if (moneyAccountEntity.getDefaultOne() == Default.valueOfDefaultType(Default.DEFAULT))
            box.setChecked(true);
        else box.setChecked(false);
*/
        return rowView;
    }

    private String getCurrencyName(long currencyId) {
        String name = null;
        CurrencyEntity currency
                = CurrencyService.selectById( currencyId,context);
        return currency.getName();
    }

    private String getProjectName(long projectId) {
        String name = null;
        ProjectEntity owner =
                ProjectService.selectById( projectId ,context);
        return owner.getName();
    }
}