package com.brth.android.cashflow.persistance.db_table_services;

import android.content.Context;

import com.brth.android.cashflow.data.model.db_datas_entities.CurrencyEntity;
import com.brth.android.cashflow.data.model.db_datas_entities.DbObjectEntity;

import java.util.List;

public class CurrencyService extends DbService {
    private CurrencyService() {

    }


    public static List<DbObjectEntity> allCurrencies(Context context) {
        return selectAllDbObjectEntitiesByClass(CurrencyEntity.class, context);
    }

    public static CurrencyEntity selectById(long currencyId, Context context) {
        initializing(context);
        CurrencyEntity currencyEntity =
                (CurrencyEntity) dataBaseOperations.selectById(currenciesTable, currencyId);
        return currencyEntity;
    }

    public static long exchange(CurrencyEntity fromCurrency, CurrencyEntity toCurrency, long money, Context context) {
        initializing(context);
        CurrencyEntity defaultC = getDefaultCurrency(context);
        double amount = 0;
        if (fromCurrency.equals(toCurrency)) return money;

        double fromRatio = Double.valueOf(fromCurrency.getRatioString());
        double toRatio = Double.valueOf(toCurrency.getRatioString());
        if (fromCurrency.getRatioType() > toCurrency.getRatioType()) {
            amount = money * toRatio * fromRatio;
        } else if (fromCurrency.getRatioType() < toCurrency.getRatioType()) {
            amount = money / toRatio / fromRatio;
        } else if (toCurrency.getRatioType() > 0 && fromCurrency.getRatioType() == toCurrency.getRatioType()) {
            amount = money / toRatio * fromRatio;
        } else if (toCurrency.getRatioType() < 0 && fromCurrency.getRatioType() == toCurrency.getRatioType()) {
            amount = money * toRatio / fromRatio;
        }
        return Long.valueOf(Math.round(Math.floor(amount)));
    }

    public static CurrencyEntity getDefaultCurrency(Context context) {
        initializing(context);
        List<DbObjectEntity> currencies =
                dataBaseOperations.selectDbObjectEntities
                        (currenciesTable, currenciesTable.DEFAULT_COLUMN, String.valueOf(CurrencyEntity.DEFAULT));
        if (currencies.size() > 0) return (CurrencyEntity) currencies.get(0);
        return null;
    }

    public static long getIdByName(String nameOfCurrency, Context context) {
        initializing(context);
        return DbService.getIdByColumn(nameOfCurrency, currenciesTable,
                currenciesTable.NAME_COLUMN);
    }

    public static boolean update(CurrencyEntity currencyEntity, Context context) {
        initializing(context);
        return dataBaseOperations.update(currencyEntity, currenciesTable);
    }

    public static long insert(CurrencyEntity currencyEntity, Context context) {
        initializing(context);
        return dataBaseOperations.insert(currencyEntity, currenciesTable);
    }

    public static boolean unUsedName(CurrencyEntity currencyEntity, boolean update, String nameValue, Context context) {
        initializing(context);
        List<DbObjectEntity> currencies =
                dataBaseOperations.selectDbObjectEntities
                        (currenciesTable, currenciesTable.NAME_COLUMN, nameValue);
        if (update && !currencies.isEmpty()) {
            for (int i = 0; i < currencies.size(); i++) {
                CurrencyEntity c;
                c = (CurrencyEntity) currencies.get(i);
                if (c.getId() != currencyEntity.getId()) return false;
            }
            return true;
        }
        return currencies.isEmpty();
    }

    public static CurrencyEntity getByName(String nameOfCurrency, Context context) {
        initializing(context);
        List<DbObjectEntity> currencies =
                dataBaseOperations.selectDbObjectEntities
                        (currenciesTable, currenciesTable.NAME_COLUMN, nameOfCurrency);
        if (currencies.size() > 0) return (CurrencyEntity) currencies.get(0);
        return null;
    }


}

