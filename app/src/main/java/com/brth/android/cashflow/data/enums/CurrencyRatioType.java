package com.brth.android.cashflow.data.enums;

public enum CurrencyRatioType {
    BIGGER, LESS ;

    public static int valueOfCurrencyRatioType(CurrencyRatioType type) {
        if (type.equals(BIGGER)) return 1;
        if (type.equals(LESS)) return -1;
        return -1;
    }
    public static CurrencyRatioType valueOfDefaultType(long value) {
        if (value == 1) return BIGGER;
        if (value == -1) return LESS;
        return LESS;
    }
}
