package com.brth.android.cashflow.persistance;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.brth.android.cashflow.data.model.db_data_tables.CarServicesTable;
import com.brth.android.cashflow.data.model.db_data_tables.CurrenciesTable;
import com.brth.android.cashflow.data.model.db_data_tables.DbTable;
import com.brth.android.cashflow.data.model.db_data_tables.FuelsTable;
import com.brth.android.cashflow.data.model.db_data_tables.MoneyAccountsTable;
import com.brth.android.cashflow.data.model.db_data_tables.PaymentEventsTable;
import com.brth.android.cashflow.data.model.db_data_tables.ProjectsTable;
import com.brth.android.cashflow.data.model.db_data_tables.TagTable;
import com.brth.android.cashflow.data.model.db_data_tables.TransferBMATable;

import java.util.ArrayList;
import java.util.List;

class ApkSqlHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "cashFlow.db";
    private final static int DB_VERSION =6;
    private final static List<DbTable> dbTables = new ArrayList<>();


    public ApkSqlHelper(Context context) {
        super(context, DATABASE_NAME, null, DB_VERSION);
        dbTables.clear();
        dbTables.add(new ProjectsTable(context));
        dbTables.add(new CurrenciesTable(context));
        dbTables.add(new MoneyAccountsTable(context));
        dbTables.add(new PaymentEventsTable(context));
        dbTables.add(new TransferBMATable(context));
        dbTables.add(new FuelsTable(context));
        dbTables.add(new CarServicesTable(context));
        dbTables.add(new TagTable(context));

    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        for (int i = 0; i < dbTables.size(); i++) {
           db.execSQL(dbTables.get(i).createTable());
        }

    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        for (int i = 0; i < dbTables.size(); i++) {
     //       System.out.println(dbTables.get(i).creatTable());
            db.execSQL(dbTables.get(i).createTable());
        }
     //   db.execSQL("ALTER TABLE transfer_between_money_accounts ADD COLUMN from_payment_id integer default 0 ;");
     //  db.execSQL("ALTER TABLE transfer_between_money_accounts ADD COLUMN to_payment_id integer default 0 ;");
     //   db.execSQL("ALTER TABLE payment_events ADD COLUMN type integer default 0 ;");
      //  db.execSQL("ALTER TABLE payment_events ADD COLUMN odometer integer default 0 ;");
     //   db.execSQL("ALTER TABLE payment_events ADD COLUMN quantity  text   ;");
       // db.execSQL("ALTER TABLE payment_events ADD COLUMN fuel_id integer default 0 ;");

    }

    public  List<DbTable> getDbTables() {
        return dbTables;
    }
}
