package com.brth.android.cashflow.persistance.db_table_services;

import android.content.Context;

import com.brth.android.cashflow.data.model.db_datas_entities.CurrencyEntity;
import com.brth.android.cashflow.data.model.db_datas_entities.DbObjectEntity;
import com.brth.android.cashflow.data.model.db_datas_entities.MoneyAccountEntity;
import com.brth.android.cashflow.data.model.db_datas_entities.PaymentEventEntity;
import com.brth.android.cashflow.data.model.db_datas_entities.TransferBetweenMoneyAccountsEntity;

import java.util.List;

public class TransferService extends DbService {

    private static final int FROM = -1;
    private static final int TO = 1;

    private TransferService() {

    }


    public static List<DbObjectEntity> allTransfers(Context context) {
        return  selectAllDbObjectEntitiesByClass(TransferBetweenMoneyAccountsEntity.class, context);
    }

    public static boolean updateTransfer
            (TransferBetweenMoneyAccountsEntity transferEntity, Context context) {
        initializing(context);
        PaymentEventEntity fromPayment = PaymentEventService.selectById
                (transferEntity.getFromPaymentEventId(), context);
        PaymentEventEntity toPayment = PaymentEventService.selectById
                (transferEntity.getToPaymentEventId(), context);
        PaymentEventEntity oldFromPayment = PaymentEventService.clonePaymentEntityInverseDirection(fromPayment);
        PaymentEventEntity oldToPayment = PaymentEventService.clonePaymentEntityInverseDirection(toPayment);

        if (dataBaseOperations.update(transferEntity, transferBMATable)) {
            boolean backwards = updatePaymentToTransForUpdate(fromPayment, transferEntity, TransferService.FROM, context);
            backwards &= PaymentEventService.update(fromPayment, oldFromPayment, context);
            if (backwards) {
                backwards &= updatePaymentToTransForUpdate(toPayment, transferEntity, TransferService.TO, context);
                backwards &= PaymentEventService.update(toPayment, oldToPayment, context);
                if (backwards) {
                    return true;

                } else {
                    //visszaállítás
                    return false;
                }

            } else return false;
        } else {
            return false;
        }
    }

    public static boolean makeTransfer
            (TransferBetweenMoneyAccountsEntity transferEntity, Context context) {
        initializing(context);
        PaymentEventEntity fromPayment = fillUpNewPaymentToTransfer(transferEntity, TransferService.FROM, context);
        PaymentEventEntity toPayment = fillUpNewPaymentToTransfer(transferEntity, TransferService.TO, context);
        long idOfFromPayment = PaymentEventService.save(fromPayment, context);
        transferEntity.setFromPaymentEventId(idOfFromPayment);
        if (idOfFromPayment == 0) return false;

        long idOfToPayment = PaymentEventService.save(toPayment, context);
        transferEntity.setToPaymentEventId(idOfToPayment);
        if (idOfToPayment == 0) {
            PaymentEventEntity fromOldPayment = PaymentEventService.clonePaymentEntityInverseDirection(fromPayment);
            fromPayment.setAmount(0);
            PaymentEventService.update(fromPayment, fromOldPayment, context);
            return false;
        }
        TransferService.save(transferEntity);
        return true;
    }

    private static long save(TransferBetweenMoneyAccountsEntity transferEntity) {
        return dataBaseOperations.insert(transferEntity, transferBMATable);
    }

    private static boolean updatePaymentToTransForUpdate
            (PaymentEventEntity paymentEventEntity,
             TransferBetweenMoneyAccountsEntity trasfer, int fromOrTo, Context context) {
        String date;
        String description;
        long accId;
        long amount;
        long direction;
        date = trasfer.getDate();
        description = "transfer :" + MoneyAccountService.selectById(trasfer.getFromAccId(), context).getName() + " -> " +
                MoneyAccountService.selectById(trasfer.getToAccId(), context).getName();
        direction = fromOrTo;
        switch (fromOrTo) {
            case TransferService.FROM:
                accId = trasfer.getFromAccId();
                amount = trasfer.getFromAmount();
                break;
            case TransferService.TO:
                accId = trasfer.getToAccId();
                amount = trasfer.getToAmount();
                break;
            default:
                return false;
        }
        MoneyAccountEntity moneyAccountEntity = MoneyAccountService.selectById(accId, context);
        CurrencyEntity currencyEntity = CurrencyService.selectById(moneyAccountEntity.getCurrencyId(), context);

        paymentEventEntity.setDate(date);
        paymentEventEntity.setMoneyAccountId(accId);
        paymentEventEntity.setCurrencyId(currencyEntity.getId());
        paymentEventEntity.setDirectionType(direction);
        paymentEventEntity.setAmount(amount);
        paymentEventEntity.setValid(paymentEventEntity.TRANSFER);
        paymentEventEntity.setDescription(description);
        return true;
    }

    private static PaymentEventEntity fillUpNewPaymentToTransfer
            (TransferBetweenMoneyAccountsEntity transfer, int fromOrTo, Context context) {
        String date;
        String description;
        long accId;
        long amount;
        long direction;
        date = transfer.getDate();
        description = "transfer :" + MoneyAccountService.selectById(transfer.getFromAccId(), context).getName() + " -> " +
                MoneyAccountService.selectById(transfer.getToAccId(), context).getName();
        direction = fromOrTo;
        switch (fromOrTo) {
            case TransferService.FROM:
                accId = transfer.getFromAccId();
                amount = transfer.getFromAmount();
                break;
            case TransferService.TO:
                accId = transfer.getToAccId();
                amount = transfer.getToAmount();
                break;
            default:
                return null;
        }

        PaymentEventEntity paymentEventEntity = new PaymentEventEntity();
        MoneyAccountEntity moneyAccountEntity = MoneyAccountService.selectById(accId, context);
        CurrencyEntity currencyEntity = CurrencyService.selectById(moneyAccountEntity.getCurrencyId(), context);

        paymentEventEntity.setProjectId(0);
        paymentEventEntity.setDate(date);
        paymentEventEntity.setMoneyAccountId(accId);
        paymentEventEntity.setCurrencyId(currencyEntity.getId());
        paymentEventEntity.setDirectionType(direction);
        paymentEventEntity.setAmount(amount);
        paymentEventEntity.setValid(paymentEventEntity.TRANSFER);
        paymentEventEntity.setDescription(description);
        return paymentEventEntity;
    }

    public static TransferBetweenMoneyAccountsEntity selectById(long id, Context context) {
        initializing(context);
        TransferBetweenMoneyAccountsEntity entity =
                (TransferBetweenMoneyAccountsEntity) dataBaseOperations.selectById(transferBMATable, id);
        return entity;
    }

}
