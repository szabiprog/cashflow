package com.brth.android.cashflow.view.controller.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.brth.android.cashflow.R;
import com.brth.android.cashflow.persistance.db_table_services.ProjectService;
import com.brth.android.cashflow.view.controller.data.AverageFuelConsumption;

import java.text.NumberFormat;
import java.util.List;

public class FuelConsumptionsArrayAdapter extends ArrayAdapter<AverageFuelConsumption> {
    private final Activity context;
    private final List<AverageFuelConsumption> items;

    public FuelConsumptionsArrayAdapter(Activity context, List<AverageFuelConsumption> items) {
        super(context, R.layout.row_fuelling, items);
        this.context = context;
        this.items = items;
    }
    @Override
    public View getView(final int position, View view, ViewGroup parent) {

        LayoutInflater inflater = context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.row_fuelling, null, true);
        AverageFuelConsumption item =  items.get(position);

        TextView project = rowView.findViewById(R.id.tv_project_name);
        project.setText(ProjectService.selectById(item.getProjectId(),context).getName());

        TextView actual = rowView.findViewById(R.id.tv_actual);
        actual.setText(String.valueOf(item.getPartAverage()));

        TextView total = rowView.findViewById(R.id.tv_total);
        total.setText(String.valueOf(item.getNewAverage()));

        TextView c = rowView.findViewById(R.id.tv_covered_distance);
        c.setText(String.valueOf(item.getActualOdometer()-item.getLastOdometer()));

        TextView ac = rowView.findViewById(R.id.tv_all_covered_distance);
        ac.setText(String.valueOf(NumberFormat.getInstance().format(
                item.getActualOdometer()-item.getStartOdometer())));

        TextView tq = rowView.findViewById(R.id.tv_total_fuel_quantity);
        tq.setText(String.valueOf(item.getAllFuel()));

        TextView tm = rowView.findViewById(R.id.tv_total_money);
        tm.setText(String.valueOf(NumberFormat.getInstance().format(item.getAllMoney())));

        TextView num = rowView.findViewById(R.id.tv_the_number_of_filling_up);
        num.setText(String.valueOf(item.getFuellings().size()));
        return rowView;
    }

}
