package com.brth.android.cashflow.view.parking;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.Toast;

import com.brth.android.cashflow.R;
import com.brth.android.cashflow.data.model.db_datas_entities.DbObjectEntity;
import com.brth.android.cashflow.data.model.db_datas_entities.PaymentEventEntity;
import com.brth.android.cashflow.persistance.db_table_services.DbService;
import com.brth.android.cashflow.persistance.db_table_services.PaymentEventService;
import com.brth.android.cashflow.persistance.db_table_services.ProjectService;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static android.os.Environment.DIRECTORY_DOWNLOADS;

public class SaveActivity extends Activity {

    public static final int REQUEST_CODE_PICKFILE = 22;
    File dir = Environment.getExternalStoragePublicDirectory(DIRECTORY_DOWNLOADS);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_save);

    }

    public void onClick(View view) {
        // File dir=new File(getFilesDir(), "mydir");
        // File dir=new File(filepath, "mydir");
        //dir=new File(getExternalStorageDirectory(),"mydir");
        //dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS);
        if (!dir.exists()) {
            boolean result = dir.mkdir();
            Toast.makeText(this, String.valueOf(dir), Toast.LENGTH_SHORT).show();
        }

        switch (view.getId()) {

            case R.id.save_btn:
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss");
                String currentDateTime = sdf.format(new Date());
                File file = new File(dir, "cashflow_" + currentDateTime + ".txt");
                Toast.makeText(this, String.valueOf("save"), Toast.LENGTH_SHORT).show();

                String json = DbService.getAllEntitiesFromDatabase(this);

                try {
                    OutputStream outputStream = new FileOutputStream(file, false);
                    outputStream.write(json.getBytes());
                    outputStream.close();

                } catch (IOException e) {
                    Toast.makeText(this, String.valueOf(e), Toast.LENGTH_SHORT).show();
                    System.out.println(file.toString());
                    System.out.println(e.toString());
                }
                break;
            case R.id.open_btn:
                Intent chooseFile = new Intent(Intent.ACTION_GET_CONTENT);
                chooseFile.setType("text/*");
                chooseFile.addCategory(Intent.CATEGORY_OPENABLE);
                Intent intent = Intent.createChooser(chooseFile, "Choose a file");
                startActivityForResult(intent, REQUEST_CODE_PICKFILE);

                break;
            case R.id.query_btn:
                try {
                    File[] files = dir.listFiles();
                    String filesStr = "";
                    for (int i = 0; i < files.length; i++) {
                        filesStr += files[i].getName() + "\n";
                    }
                    Toast.makeText(this, String.valueOf(filesStr), Toast.LENGTH_SHORT).show();
                } catch (Exception e) {
                    Toast.makeText(this, String.valueOf(e), Toast.LENGTH_SHORT).show();

                }
                break;
            case R.id.create_btn:

                List<DbObjectEntity> paymentEventEntities = PaymentEventService.allValidPayments(this);
                ProjectService.resetAllProjectInnOut(this);
                for (int i = 0; i < paymentEventEntities.size(); i++) {
                    PaymentEventEntity paymentEventEntity = (PaymentEventEntity) paymentEventEntities.get(i);
                    ProjectService.paymentRegisterToProjectTree(this, paymentEventEntity);
                }
                break;
            default:
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        switch (requestCode) {

            case REQUEST_CODE_PICKFILE:

                if (resultCode == Activity.RESULT_OK) {
                    Uri selectedfile_uri = data.getData();

                    Uri fileUri = data.getData();
                    String filePath = fileUri.getPath();
                    File file = new File(filePath, filePath);
                    System.out.println(fileUri);
                    System.out.println(filePath);
                    String t = "";

                    try (InputStream inputStream = getContentResolver().openInputStream(selectedfile_uri)) {
                        InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                        //InputStreamReader inputStreamReader = new FileReader(file);
                        BufferedReader reader = new BufferedReader(inputStreamReader);

                        String row;
                        while ((row = reader.readLine()) != null) {
                            t += row;
                            //  System.out.println(row+'\n');
                        }
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    //  Toast.makeText(this, DbService.reBuildDatabaseFromGson(this,t), Toast.LENGTH_LONG).show();
                    if (t.length() > 10) DbService.reBuildDatabaseFromGson(this, t);


                    break;

                }

        }
    }
}
