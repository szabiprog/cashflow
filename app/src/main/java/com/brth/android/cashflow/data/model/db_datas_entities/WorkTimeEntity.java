package com.brth.android.cashflow.data.model.db_datas_entities;

import java.io.Serializable;

public class WorkTimeEntity implements Serializable {
    private long id;
    private String date;
    private String workTime;

    public WorkTimeEntity() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getWorkTime() {
        return workTime;
    }

    public void setWorkTime(String workTime) {
        this.workTime = workTime;
    }
}
