package com.brth.android.cashflow.data.enums;


public enum DirectionType {
    INCOME,
    OUTLAY;


    static int valueOfDirectionType(DirectionType directionType) {
        if (directionType.equals(OUTLAY)) return -1;
        if (directionType.equals(INCOME)) return 1;
        return 0;
    }

}
