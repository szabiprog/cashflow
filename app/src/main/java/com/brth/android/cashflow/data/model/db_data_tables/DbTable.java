package com.brth.android.cashflow.data.model.db_data_tables;

import android.content.Context;

import com.brth.android.cashflow.data.interfaces.DataBaseFunctions;
import com.brth.android.cashflow.data.interfaces.DbTables;

public abstract class DbTable implements DataBaseFunctions, DbTables {

    private Context context;
    public DbTable( Context context) {
        this.context = context;
    }
    public Context getContext() { return this.context;   }

}
