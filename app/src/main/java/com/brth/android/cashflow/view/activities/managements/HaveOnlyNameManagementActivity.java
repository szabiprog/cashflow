package com.brth.android.cashflow.view.activities.managements;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.brth.android.cashflow.R;
import com.brth.android.cashflow.data.model.db_datas_entities.DbObjectEntity;
import com.brth.android.cashflow.data.model.db_datas_entities.FuelEntity;
import com.brth.android.cashflow.data.model.db_datas_entities.TagEntity;
import com.brth.android.cashflow.persistance.db_table_services.FuelService;
import com.brth.android.cashflow.persistance.db_table_services.TagService;

public class HaveOnlyNameManagementActivity extends AppCompatActivity {
    private TextView header;
    private EditText nameTx;
    private DbObjectEntity entity = null;
    private boolean update = false;
    private String selected;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fuel_management);
        header = findViewById(R.id.row_head_line);
        nameTx = findViewById(R.id.name_value);
        Intent intent = getIntent();
        selected = intent.getStringExtra("selected");
        if(selected == null ) throw new NullPointerException("Non selected");
        String name = intent.getStringExtra("name");
        update = (name != null);

        switch(selected){
            case "fuels":
                entity = new FuelEntity();
                header.setText(R.string.new_fuel);
                break;
            case "tags":
                entity = new TagEntity();
                header.setText(R.string.new_tag);
                break;
        }
        if (update) {
            switch(selected){
                case "fuels":
                    entity = FuelService.getByName( this , name);
                    break;
                case "tags":
                    entity = TagService.getByName( this , name);
                    break;
            }
            this.nameTx.setText(name);
        }
    }

    synchronized public void onClick(View view) {
        switch (view.getId()) {
            case R.id.save_btn:
                findViewById(R.id.save_btn).setClickable(false);
                String nameValue;
                try {
                    nameValue =
                            this.nameTx.getText().toString();
                } catch (Exception e) {
                    nameValue = null;
                }
                if (nameValue != null && nameValue.length() > 0) {
                    if (saveEntity(nameValue)) {
                        setResult(Activity.RESULT_OK, null);
                        finish();
                    } else findViewById(R.id.save_btn).setClickable(true);
                } else {
                    sendToast(R.string.give_me_a_name);
                    findViewById(R.id.save_btn).setClickable(true);
                }
                break;
        }
    }

    private boolean saveEntity(String nameValue) {
        switch(selected){
            case "fuels":
                if (FuelService.unUsedName((FuelEntity) entity, update, nameValue, this)) {
                    entity.setName(nameValue);
                    if (update) FuelService.update((FuelEntity) entity, this);
                    else FuelService.insert((FuelEntity) entity, this);
                    return true;
                }
                break;
            case "tags":
                if (TagService.unUsedName((TagEntity) entity, update, nameValue, this)) {
                    entity.setName(nameValue);
                    if (update) TagService.update((TagEntity) entity, this);
                    else TagService.insert((TagEntity) entity, this);
                    return true;
                }
                break;
        }
        sendToast(R.string.used_name);
        return false;
    }

    private void sendToast(int str) {
        Toast toast = Toast.makeText(this, str, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 0, 0);
        toast.show();
    }

}
