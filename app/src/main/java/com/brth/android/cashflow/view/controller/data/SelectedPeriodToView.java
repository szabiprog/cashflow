package com.brth.android.cashflow.view.controller.data;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class SelectedPeriodToView implements Serializable {

    public static final int PERIOD_BY_DAY = 1 ;
    public static final int PERIOD_BY_WEEK = 2;
    public static final int PERIOD_BY_MONTH = 3;
    public static final int PERIOD_BY_YEAR = 4;
    static public final int LIST_BY_DEFAULT = 0;
    static public final int LIST_BY_PROJECT = 1;
    static public final int LIST_BY_MONEY_ACC = 2;
    static public final int LIST_BY_CURRENCY = 3;
    public static final int LIST_BY_TAG = 4;
    private static final int[] LIST_TYPES =
            {LIST_BY_DEFAULT, LIST_BY_PROJECT, LIST_BY_MONEY_ACC, LIST_BY_CURRENCY,LIST_BY_TAG} ;

    private Calendar myCalendar;
    private String selectedDay;
    private int period ;
    private long innAmount;
    private  long outAmount;
    private int listBy;
    private long listById;
    private int direction;

    public SelectedPeriodToView() {
        myCalendar = new GregorianCalendar();
        selectedDay="";
        period=0;
        setListBy(LIST_BY_DEFAULT);
    }


    public long getInnAmount() {
        return innAmount;
    }

    public void setInnAmount(long innAmount) {
        this.innAmount = innAmount;
    }

    public long getOutAmount() {
        return outAmount;
    }

    public void setOutAmount(long outAmount) {
        this.outAmount = outAmount;
    }


    public String getSelectedDay() {
        return selectedDay;
    }

    public void setSelectedDay(String selectedDay) {
        this.selectedDay = selectedDay;
    }

    public int getPeriod() {
        return period;
    }

    public void setPeriod(int period) {
        this.period = period;
    }

    public Calendar getMyCalendar() {
        return myCalendar;
    }

    public void setMyCalendar(Calendar myCalendar) {   this.myCalendar = myCalendar; }

    public void resetSelectedDay(){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd.");
        this.setSelectedDay(sdf.format(this.myCalendar.getTime()));
    }

    public int getListBy() {
        return listBy;
    }

    public void setListBy(int listBy) {
        this.listBy = listBy;
    }

    public long getListById() {
        return listById;
    }

    public void setListById(long listById) {
        this.listById = listById;
    }

    public void changeListType() {
        int i = this.listBy + 1;
        if( i >= this.LIST_TYPES.length ) this.listBy = LIST_BY_DEFAULT;
        else {
            this.listBy = LIST_TYPES[i];
        }
    }

    public int getDirection() {
        return direction;
    }

    public void setDirection(int direction) {
        this.direction = direction;
    }

}
