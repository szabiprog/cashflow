package com.brth.android.cashflow.view.controller.models;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.DatePicker;
import android.widget.TextView;

import com.brth.android.cashflow.R;
import com.brth.android.cashflow.persistance.IoService;
import com.brth.android.cashflow.persistance.db_table_services.PaymentEventService;
import com.brth.android.cashflow.view.activities.PaymentsByPeriodActivity;
import com.brth.android.cashflow.view.activities.managements.DataManagementActivity;
import com.brth.android.cashflow.view.controller.data.RequestCodes;
import com.brth.android.cashflow.view.controller.data.SelectedPeriodToView;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

public class MainViewModel extends Views {
    static private TextView tvDailyInn;
    static private TextView tvDailyOut;
    static private TextView tvWeeklyInn;
    static private TextView tvWeeklyOut;
    static private TextView tvMonthlyInn;
    static private TextView tvMonthlyOut;
    static private TextView tvYearlyInn;
    static private TextView tvYearlyOut;
    static private DatePickerDialog.OnDateSetListener date;
    static private SelectedPeriodToView selectedPeriodToView;
    static private MainViewModel instance;
    static private List<Views> history;
    static private Activity mainActivity;

    private MainViewModel() {
    }

    public static synchronized MainViewModel getInstance(List<Views> history, Bundle savedInstanceState) {
        MainViewModel.history = history;
        if (instance == null) {
            instance = new MainViewModel();
        }
        instance.setSavedInstanceState(savedInstanceState);
        history.add(instance);
        return instance;
    }

    @Override
    public void onResume(Context context) {

        mainActivity = (Activity) context;
        mainActivity.setContentView(R.layout.old);

        final Animation animation = new AlphaAnimation( 1,0.7f ); // Change alpha from fully visible to invisible
        animation.setDuration(1000); // duration - half a second
        animation.setInterpolator(new LinearInterpolator()); // do not alter animation rate
        animation.setRepeatCount(10); // Repeat animation infinitely
        animation.setRepeatMode(Animation.RESTART); // Reverse animation at the end so the button will fade back in
        mainActivity.findViewById(R.id.tvDaily).startAnimation(animation);
        mainActivity.findViewById(R.id.tvWeekly).startAnimation(animation);
        mainActivity.findViewById(R.id.tvMonthly).startAnimation(animation);
        mainActivity.findViewById(R.id.tvYearly).startAnimation(animation);

        tvDailyInn = mainActivity.findViewById(R.id.tvDailyInn);
        tvDailyOut = mainActivity.findViewById(R.id.tvDailyOut);
        tvWeeklyInn = mainActivity.findViewById(R.id.tvWeeklyInn);
        tvWeeklyOut = mainActivity.findViewById(R.id.tvWeeklyOut);
        tvMonthlyInn = mainActivity.findViewById(R.id.tvMonthlyInn);
        tvMonthlyOut = mainActivity.findViewById(R.id.tvMonthlyOut);
        tvYearlyInn = mainActivity.findViewById(R.id.tvYearlyInn);
        tvYearlyOut = mainActivity.findViewById(R.id.tvYearlyOut);
        selectedPeriodToView = new SelectedPeriodToView();
        datePickerListener(context);
        selectedPeriodToView.setMyCalendar(new GregorianCalendar());
        selectedPeriodToView.setListBy(SelectedPeriodToView.LIST_BY_DEFAULT);
        selectedPeriodToView.setDirection(1);
        tvDailyInn.setText(NumberFormat.getInstance().format(
                PaymentEventService.dailyMoving(context, selectedPeriodToView)));
        selectedPeriodToView.setDirection(-1);
        tvDailyOut.setText(NumberFormat.getInstance().format(
                PaymentEventService.dailyMoving(context, selectedPeriodToView)));
        selectedPeriodToView.setDirection(1);
        tvWeeklyInn.setText(NumberFormat.getInstance().format(
                PaymentEventService.weeklyMoving(context, new GregorianCalendar(), 1)));
        selectedPeriodToView.setDirection(-1);
        tvWeeklyOut.setText(NumberFormat.getInstance().format(
                PaymentEventService.weeklyMoving(context, new GregorianCalendar(), -1)));
        selectedPeriodToView.setDirection(1);
        tvMonthlyInn.setText(NumberFormat.getInstance().format(
                PaymentEventService.monthlyMoving(context, selectedPeriodToView)));
        selectedPeriodToView.setDirection(-1);
        tvMonthlyOut.setText(NumberFormat.getInstance().format(
                PaymentEventService.monthlyMoving(context, selectedPeriodToView)));
        selectedPeriodToView.setDirection(1);
        tvYearlyInn.setText(NumberFormat.getInstance().format(
                PaymentEventService.yearlyMoving(context, new GregorianCalendar(), 1)));
        selectedPeriodToView.setDirection(-1);
        tvYearlyOut.setText(NumberFormat.getInstance().format(
                PaymentEventService.yearlyMoving(context, new GregorianCalendar(), -1)));
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case RequestCodes.MENU_REQUEST_CODE_PICK_FILE:
                if (resultCode == Activity.RESULT_OK) {
                    IoService.loadData(mainActivity, data);
                }
        }
    }

    @Override
    public void putToSaveInstanceState(Bundle outState) {
    }

    @Override
    public void onClick(View view) {
        Intent intent;
        Context context = view.getContext();
        mainActivity = (Activity) context;
        switch (view.getId()) {
            case R.id.btOutlay:
                // intent = new Intent(context, PaymentEventManagementActivity.class);
                // context.startActivity(intent);
                mainActivity.findViewById(R.id.btOutlay).setClickable(false);
                PaymentEventManagementViewModel.getInstance(history, getSavedInstanceState());
                //  mainActivity.onBackPressed();
                mainActivity.onAttachedToWindow();
                break;
            case R.id.btStructure:
                intent = new Intent(context, DataManagementActivity.class);
                context.startActivity(intent);
                break;
            case R.id.tvDaily:
                selectedPeriodToView.setPeriod(SelectedPeriodToView.PERIOD_BY_DAY);
                showPicker(context);
                break;
            case R.id.tvWeekly:
                selectedPeriodToView.setPeriod(SelectedPeriodToView.PERIOD_BY_WEEK);
                showPicker(context);
                break;
            case R.id.tvMonthly:
                selectedPeriodToView.setPeriod(SelectedPeriodToView.PERIOD_BY_MONTH);
                showPicker(context);
                break;
            case R.id.tvYearly:
                selectedPeriodToView.setPeriod(SelectedPeriodToView.PERIOD_BY_YEAR);
                showPicker(context);
                break;
            case R.id.button_fuel_average:
                mainActivity.findViewById(R.id.button_fuel_average).setClickable(false);
                AveragesFuelConsumptionViewModel.getInstance(history, getSavedInstanceState());
                mainActivity.onAttachedToWindow();
                break;
        }
    }

    static private void showPicker(Context context) {
        DatePickerDialog dpd = pickerSetup(context);
        if (dpd != null) dpd.show();
    }

    private static DatePickerDialog pickerSetup(Context context) {
        selectedPeriodToView.setMyCalendar(new GregorianCalendar());
        Calendar myCalendar = selectedPeriodToView.getMyCalendar();
        DatePickerDialog dpd = null;
        switch (selectedPeriodToView.getPeriod()) {
            default:
                dpd = new DatePickerDialog(context, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH));
                break;
            case SelectedPeriodToView.PERIOD_BY_YEAR:
                dpd = new DatePickerDialog(context, android.R.style.Theme_Holo_Dialog, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH));
                dpd.getDatePicker().findViewById(context.getResources().
                        getIdentifier("day", "id", "android")).setVisibility(View.GONE);
                dpd.getDatePicker().findViewById(context.getResources().
                        getIdentifier("month", "id", "android")).setVisibility(View.GONE);
                break;
        }
        return dpd;
    }

    private static void datePickerListener(Context context) {
        date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                selectedPeriodToView.getMyCalendar().set(Calendar.YEAR, year);
                selectedPeriodToView.getMyCalendar().set(Calendar.MONTH, monthOfYear);
                selectedPeriodToView.getMyCalendar().set(Calendar.DAY_OF_MONTH, dayOfMonth);
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd.");
                selectedPeriodToView.setSelectedDay(sdf.format(selectedPeriodToView.getMyCalendar().getTime()));
                Intent intent = new Intent
                        (context, PaymentsByPeriodActivity.class);
                intent.putExtra("selected period", selectedPeriodToView);
                context.startActivity(intent);
                // finish();
            }
        };
    }


}
