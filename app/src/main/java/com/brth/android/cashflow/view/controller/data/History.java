package com.brth.android.cashflow.view.controller.data;

import android.os.Parcel;
import android.os.Parcelable;

import com.brth.android.cashflow.view.controller.models.Views;

import java.io.Serializable;
import java.util.List;

public class History implements Serializable, Parcelable {
    public List<Views> history;

    public History() {
    }

    public History(List<Views> history) {
        this.history = history;
    }

    protected History(Parcel in) {
    }

    public static final Creator<History> CREATOR = new Creator<History>() {
        @Override
        public History createFromParcel(Parcel in) {
            return new History(in);
        }

        @Override
        public History[] newArray(int size) {
            return new History[size];
        }
    };

    public List<Views> getHistory() {
        return history;
    }

    public void setHistory(List<Views> history) {
        this.history = history;
    }

    @Override
    public int describeContents() {return 0;}

    @Override
    public void writeToParcel(Parcel dest, int flags) {}

}
