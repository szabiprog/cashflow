package com.brth.android.cashflow.view.activities.managements;

import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.brth.android.cashflow.R;
import com.brth.android.cashflow.data.model.db_datas_entities.CurrencyEntity;
import com.brth.android.cashflow.data.model.db_datas_entities.MoneyAccountEntity;
import com.brth.android.cashflow.persistance.db_table_services.CurrencyService;
import com.brth.android.cashflow.persistance.db_table_services.MoneyAccountService;
import com.brth.android.cashflow.view.activities.CurrenciesActivity;
import com.brth.android.cashflow.view.activities.ProjectsActivity;

public class MoneyAccountManagementActivity extends AppCompatActivity {


    private final int REQUEST_CODE_PROJECT = 111;
    private final int REQUEST_CODE_CURRENCY = 222;
    private Button btCurrencySelect;
    private String nameOfCurrency = null;
    private EditText nameOfAccount;
    private EditText balance;
    private MoneyAccountEntity moneyAccountEntity;
    private CurrencyEntity currencyEntity;
    private boolean update = false;
    private EditText description;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_money_account_management);
        btCurrencySelect = findViewById(R.id.bt_select_currency);
        nameOfAccount = findViewById(R.id.name_value);
        balance = findViewById(R.id.balance_value);
        description = findViewById(R.id.description_value);

        Intent intent = getIntent();
        String nameOfMoneyAccount = intent.getStringExtra("name");

        update = (nameOfMoneyAccount != null);
        if (update) fillUppFields(nameOfMoneyAccount);
        else moneyAccountEntity = new MoneyAccountEntity();

    }

    private void fillUppFields(String nameOfMoneyAccount) {
        moneyAccountEntity = MoneyAccountService.selectByName(nameOfMoneyAccount, this);
        nameOfAccount.setText(nameOfMoneyAccount);
        currencyEntity = CurrencyService.selectById(moneyAccountEntity.getCurrencyId(), this);
        nameOfCurrency = currencyEntity.getName();
        btCurrencySelect.setText(nameOfCurrency);
        balance.setText(String.format("%d", moneyAccountEntity.getBalance()));
        description.setText(moneyAccountEntity.getDescription());
    }


    synchronized public void onClick(View view) {
        Intent intent;
        switch (view.getId()) {
            case R.id.bt_select_project:
                intent = new Intent(this, ProjectsActivity.class);
                intent.putExtra("select", "select");
                startActivityForResult(intent, REQUEST_CODE_PROJECT);
                break;
            case R.id.bt_select_currency:

                intent = new Intent(this, CurrenciesActivity.class);
                intent.putExtra("select", "select");
                startActivityForResult(intent, REQUEST_CODE_CURRENCY);
                break;
            case R.id.bt_save_account:
                findViewById(R.id.bt_save_account).setClickable(false);
                String nameValue;
                try {
                    nameValue = nameOfAccount.getText().toString();

                } catch (Exception e) {
                    nameValue = "";
                }
                if (nameOfCurrency != null && nameValue.length() > 0) {
                    if (saveEntity(nameValue)) this.onBackPressed();
                    else  findViewById(R.id.bt_save_account).setClickable(true);
                } else {
                    sendToast(R.string.select_currency_add_name);
                    findViewById(R.id.bt_save_account).setClickable(true);
                }
                break;
            case R.id.balance:
                Long b = MoneyAccountService.balanceOfOneMoneyAcc(moneyAccountEntity.getName(), this);
                if (b != null) balance.setText(b.toString());
                moneyAccountEntity.setBalance(b);
                break;
            default:

                break;

        }
    }

    private boolean saveEntity(String nameValue) {
        long idOfCurrency = CurrencyService.getIdByName(nameOfCurrency,this);
        moneyAccountEntity.setName(nameValue);
        if (MoneyAccountService.unUsedName(moneyAccountEntity,update,this) && idOfCurrency != 0) {
            moneyAccountEntity.setName(nameValue);
            moneyAccountEntity.setCurrencyId(idOfCurrency);
            long balanceLongValue = Long.parseLong(balance.getText().toString());
            moneyAccountEntity.setBalance(balanceLongValue);
            moneyAccountEntity.setDescription(description.getText().toString());
            if (update) MoneyAccountService.update(moneyAccountEntity,this);
            else MoneyAccountService.insert(moneyAccountEntity,this);
            return true;
        } else sendToast(R.string.used_name);
        return false;
    }


    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_CODE_PROJECT:
                if (resultCode == RESULT_OK) {
                    String name = data.getStringExtra("name");
                }
                break;
            case REQUEST_CODE_CURRENCY:
                if (resultCode == RESULT_OK) {
                    String name = data.getStringExtra("name");
                    btCurrencySelect.setText(name);
                    nameOfCurrency = name;
                }
                break;
        }

    }

    private void sendToast(int str) {
        Toast toast = Toast.makeText(this, str, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 0, 0);
        toast.show();
    }

}
