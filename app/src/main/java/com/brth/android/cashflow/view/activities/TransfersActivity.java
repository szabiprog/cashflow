package com.brth.android.cashflow.view.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.brth.android.cashflow.R;
import com.brth.android.cashflow.data.model.db_data_tables.TransferBMATable;
import com.brth.android.cashflow.data.model.db_datas_entities.DbObjectEntity;
import com.brth.android.cashflow.data.model.db_datas_entities.TransferBetweenMoneyAccountsEntity;
import com.brth.android.cashflow.persistance.db_table_services.TransferService;
import com.brth.android.cashflow.view.activities.managements.TransferManagementActivity;
import com.brth.android.cashflow.view.controller.adapters.TransferArrayAdapter;

import java.util.List;

public class TransfersActivity extends AppCompatActivity {
    private ListView listViewDbTables;
    private ArrayAdapter<DbObjectEntity> arrayAdapter;
    private TextView header;
    private Button button;
    private String toSelect;
    private TransferBMATable table;
    private List<DbObjectEntity> dbObjectEntityList;
    private TransferBetweenMoneyAccountsEntity entity;
    private TextView parentHead;
    private TextView tvOrder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        header = findViewById(R.id.tv_header);
        button = findViewById(R.id.bt_new_item);
        parentHead = findViewById(R.id.parent_head);
        tvOrder = findViewById(R.id.tv_order);

        Intent intent = getIntent();
        findViewById(R.id.iv_back_empty_list).setVisibility(View.GONE);
        toSelect = intent.getStringExtra("select");
        if (toSelect == null) {
            header.setText(R.string.transfers);
            button.setText(R.string.new_transfer);
        } else {
            header.setText(R.string.select_transfer);
            button.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        setUpAdapter();
    }

    private void setUpAdapter() {


        dbObjectEntityList = TransferService.allTransfers(this);
        arrayAdapter = new TransferArrayAdapter(this, dbObjectEntityList);

        listViewDbTables = findViewById(R.id.lv_items);
        listViewDbTables.setAdapter(arrayAdapter);

        listViewDbTables.setLongClickable(true);
        listViewDbTables.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
                                           int pos, long id) {
                System.out.println(pos + " " + id);
                TransferBetweenMoneyAccountsEntity entity = ((TransferBetweenMoneyAccountsEntity) listViewDbTables.getAdapter().getItem(pos));
                Intent intent = new Intent();
                if (toSelect != null) {
                    intent.putExtra("id", String.valueOf(entity.getId()));
                    setResult(Activity.RESULT_OK, intent);
                    finish();
                } else {
                    intent = new Intent
                            (TransfersActivity.this, TransferManagementActivity.class);
                    intent.putExtra("id", String.valueOf(entity.getId()));
                    TransfersActivity.this.startActivity(intent);
                }
                return true;
            }
        });
    }

    public void onClick(View view) {
        Intent intent;
        switch (view.getId()) {
            case R.id.bt_new_item:
                intent = new Intent
                        (TransfersActivity.this, TransferManagementActivity.class);
                TransfersActivity.this.startActivity(intent);
                break;
        }
    }
}