package com.brth.android.cashflow.data.model.db_datas_entities;

import java.io.Serializable;

public class DbObjectEntity implements Serializable {
    private long id;
    private String name;

    public DbObjectEntity() {
    }

    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

}
