package com.brth.android.cashflow.view.controller.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.brth.android.cashflow.R;
import com.brth.android.cashflow.data.model.db_data_tables.MoneyAccountsTable;
import com.brth.android.cashflow.data.model.db_datas_entities.DbObjectEntity;
import com.brth.android.cashflow.data.model.db_datas_entities.MoneyAccountEntity;
import com.brth.android.cashflow.data.model.db_datas_entities.TransferBetweenMoneyAccountsEntity;
import com.brth.android.cashflow.persistance.DataBaseOperations;

import java.util.List;

;

public class TransferArrayAdapter extends ArrayAdapter<DbObjectEntity> {


    private DataBaseOperations dataBaseOperations;
    private final Activity context;
    private final List<DbObjectEntity> items;

    public TransferArrayAdapter(Activity context, List<DbObjectEntity> items) {
        super(context, R.layout.row_transfer, items);
        this.context = context;
        this.items = items;
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {

        dataBaseOperations = DataBaseOperations.getInstance(context);
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.row_transfer, null, true);

        TextView id = rowView.findViewById(R.id.tv_id);
        TextView tvFromName = rowView.findViewById(R.id.tv_from_id);
        TextView tvFromAmount = rowView.findViewById(R.id.tv_from_amount);
        TextView tvToName = rowView.findViewById(R.id.tv_to_id);
        TextView tvToAmount = rowView.findViewById(R.id.tv_to_amount);
        TextView tvDate = rowView.findViewById(R.id.tv_currentDate);
        TextView tvDescription = rowView.findViewById(R.id.tv_description);

        TransferBetweenMoneyAccountsEntity entity =
                (TransferBetweenMoneyAccountsEntity) items.get(position);
        MoneyAccountEntity fromAcc = (MoneyAccountEntity) dataBaseOperations.selectById
                (new MoneyAccountsTable(context),entity.getFromAccId());
        MoneyAccountEntity toAcc = (MoneyAccountEntity) dataBaseOperations.selectById
                (new MoneyAccountsTable(context),entity.getToAccId());

        tvDate.setText(entity.getDate());
        tvFromName.setText(fromAcc.getName());
        tvFromAmount.setText(entity.getFromAmount()+"");
        tvToName.setText(toAcc.getName());
        tvToAmount.setText(entity.getToAmount()+"");
        tvDescription.setText(entity.getDescription());

        return rowView;
    }
}