package com.brth.android.cashflow.view.controller.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.brth.android.cashflow.R;
import com.brth.android.cashflow.view.controller.data.ElementFromEntity;

import java.util.List;

public class PaymentBySomethingArrayAdapter extends ArrayAdapter<ElementFromEntity> {
    private final Activity context;
    private final List<ElementFromEntity> items;


    public PaymentBySomethingArrayAdapter(Activity context, List<ElementFromEntity> items) {
        super(context, R.layout.row_project_inn_out, items);
        this.context = context;
        this.items = items;
    }


    @Override
    public View getView(final int position, View view, ViewGroup parent) {

        LayoutInflater inflater = context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.row_project_inn_out, null, true);



        TextView nameTV = rowView.findViewById(R.id.tv_project_name);
        TextView piecesTV = rowView.findViewById(R.id.tv_pieces);
        TextView innTV = rowView.findViewById(R.id.tv_inn);
        TextView outTV = rowView.findViewById(R.id.tv_out);
        TextView balanceTV = rowView.findViewById(R.id.tv_balance);
        ElementFromEntity item =items.get(position);

        // the list separate by tag ?
        piecesTV.setText(String.valueOf(item.getPaymentEventEntities().size()));
        nameTV.setText(item.getName()+" : ");
        innTV.setText(String.valueOf(item.getInn()));
        outTV.setText(String.valueOf(item.getOut()));
        balanceTV.setText(String.valueOf(item.getBalance()));
        return rowView;
    }
}


