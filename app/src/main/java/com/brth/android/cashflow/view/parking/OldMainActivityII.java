package com.brth.android.cashflow.view.parking;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.brth.android.cashflow.R;
import com.brth.android.cashflow.persistance.IoService;
import com.brth.android.cashflow.persistance.db_table_services.PaymentEventService;
import com.brth.android.cashflow.view.activities.PaymentsByPeriodActivity;
import com.brth.android.cashflow.view.activities.managements.DataManagementActivity;
import com.brth.android.cashflow.view.activities.managements.PaymentEventManagementActivity;
import com.brth.android.cashflow.view.controller.data.SelectedPeriodToView;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class OldMainActivityII extends AppCompatActivity {
    private TextView tvDailyInn;
    private TextView tvDailyOut;
    private TextView tvWeeklyInn;
    private TextView tvWeeklyOut;
    private TextView tvMonthlyInn;
    private TextView tvMonthlyOut;
    private TextView tvYearlyInn;
    private TextView tvYearlyOut;
    private DatePickerDialog.OnDateSetListener date;
    private SelectedPeriodToView selectedPeriodToView;
    private boolean doubleBackToExitPressedOnce = false;
    public static final int REQUEST_CODE_PICKFILE = 22;
    private boolean saveFlag ;
    public static final String SHOULD_FINISH = "should_finish";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.old);
        tvDailyInn = findViewById(R.id.tvDailyInn);
        tvDailyOut = findViewById(R.id.tvDailyOut);
        tvWeeklyInn = findViewById(R.id.tvWeeklyInn);
        tvWeeklyOut = findViewById(R.id.tvWeeklyOut);
        tvMonthlyInn = findViewById(R.id.tvMonthlyInn);
        tvMonthlyOut = findViewById(R.id.tvMonthlyOut);
        tvYearlyInn = findViewById(R.id.tvYearlyInn);
        tvYearlyOut = findViewById(R.id.tvYearlyOut);
        selectedPeriodToView = new SelectedPeriodToView();
        datePickerListener();
        //    drive();
    }

    @Override
    public boolean onCreatePanelMenu(int featureId, @NonNull Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu,menu);
        return true;
    }

    private void drive() {
        //ProjectService.rebuild(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        //  PaymentEventService.revalidateTransferEvents(this);
        selectedPeriodToView.setMyCalendar( new GregorianCalendar());
        selectedPeriodToView.setListBy( SelectedPeriodToView.LIST_BY_DEFAULT);
        selectedPeriodToView.setDirection(1);
        tvDailyInn.setText(NumberFormat.getInstance().format(
                PaymentEventService.dailyMoving(this,selectedPeriodToView)));
        selectedPeriodToView.setDirection(-1);
        tvDailyOut.setText(NumberFormat.getInstance().format(
                PaymentEventService.dailyMoving(this, selectedPeriodToView)));
        selectedPeriodToView.setDirection(1);
        tvWeeklyInn.setText(NumberFormat.getInstance().format(
                PaymentEventService.weeklyMoving(this, new GregorianCalendar(), 1)));
        selectedPeriodToView.setDirection(-1);
        tvWeeklyOut.setText(NumberFormat.getInstance().format(
                PaymentEventService.weeklyMoving(this, new GregorianCalendar(), -1)));
        selectedPeriodToView.setDirection(1);
        tvMonthlyInn.setText(NumberFormat.getInstance().format(
                PaymentEventService.monthlyMoving(this,selectedPeriodToView)));
        selectedPeriodToView.setDirection(-1);
        tvMonthlyOut.setText(NumberFormat.getInstance().format(
                PaymentEventService.monthlyMoving(this, selectedPeriodToView)));
        selectedPeriodToView.setDirection(1);
        tvYearlyInn.setText(NumberFormat.getInstance().format(
                PaymentEventService.yearlyMoving(this, new GregorianCalendar(), 1)));
        selectedPeriodToView.setDirection(-1);
        tvYearlyOut.setText(NumberFormat.getInstance().format(
                PaymentEventService.yearlyMoving(this, new GregorianCalendar(), -1)));
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    public void onClick(View view) {
        Intent intent;
        switch (view.getId()) {
            case R.id.btOutlay:
                intent = new Intent(this, PaymentEventManagementActivity.class);
                startActivity(intent);
                this.finish();
                break;
            case R.id.btStructure:
                intent = new Intent(this, DataManagementActivity.class);
                startActivity(intent);
                this.finish();
                break;
            case R.id.tvDaily:
                selectedPeriodToView.setPeriod(SelectedPeriodToView.PERIOD_BY_DAY);
                showPicker();
                break;
            case R.id.tvWeekly:
                selectedPeriodToView.setPeriod(SelectedPeriodToView.PERIOD_BY_WEEK);
                showPicker();
                break;
            case R.id.tvMonthly:
                selectedPeriodToView.setPeriod(SelectedPeriodToView.PERIOD_BY_MONTH);
                showPicker();
                break;
            case R.id.tvYearly:
                selectedPeriodToView.setPeriod(SelectedPeriodToView.PERIOD_BY_YEAR);
                showPicker();
                break;

        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        saveFlag = false;
        switch (item.getItemId()){
            case R.id.menu_save:
                saveFlag = true;
                ActivityCompat.requestPermissions(OldMainActivityII.this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        1);
                return true;
            case R.id.menu_load:
                Intent chooseFile = new Intent(Intent.ACTION_GET_CONTENT);
                chooseFile.setType("text/*");
                chooseFile.addCategory(Intent.CATEGORY_OPENABLE);
                Intent intent = Intent.createChooser(chooseFile, "Choose a file");
                startActivityForResult(intent, REQUEST_CODE_PICKFILE);
                return true;
            case R.id.menu_open_drive:
                Intent launchIntent = getPackageManager().getLaunchIntentForPackage("com.google.android.apps.docs");
                if (launchIntent != null) {
                    startActivity(launchIntent);//null pointer check in case package name was not found
                }
                break;
            case R.id.menu_close:
                finishAndRemoveTask();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    private void showPicker() {
        DatePickerDialog dpd=  pickerSetup();
        if(dpd!=null)  dpd.show();
    }

    private DatePickerDialog pickerSetup() {
        selectedPeriodToView.setMyCalendar(new GregorianCalendar());
        Calendar myCalendar = selectedPeriodToView.getMyCalendar();
        DatePickerDialog  dpd=null;
        switch (selectedPeriodToView.getPeriod()) {
            default:
                dpd = new DatePickerDialog(OldMainActivityII.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH));
                break;
            case SelectedPeriodToView.PERIOD_BY_YEAR:
                dpd = new DatePickerDialog(OldMainActivityII.this, android.R.style.Theme_Holo_Dialog, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH));
                dpd.getDatePicker().findViewById(getResources().
                        getIdentifier("day", "id", "android")).setVisibility(View.GONE);
                dpd.getDatePicker().findViewById(getResources().
                        getIdentifier("month", "id", "android")).setVisibility(View.GONE);
                break;
        }
        return dpd;
    }

    private void datePickerListener() {
        date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                selectedPeriodToView.getMyCalendar().set(Calendar.YEAR, year);
                selectedPeriodToView.getMyCalendar().set(Calendar.MONTH, monthOfYear);
                selectedPeriodToView.getMyCalendar().set(Calendar.DAY_OF_MONTH, dayOfMonth);
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd.");
                selectedPeriodToView.setSelectedDay(sdf.format(selectedPeriodToView.getMyCalendar().getTime()));
                Intent intent = new Intent
                        (OldMainActivityII.this, PaymentsByPeriodActivity.class);
                intent.putExtra("selected period", selectedPeriodToView);
                OldMainActivityII.this.startActivity(intent);
                finish();
            }
        };
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        switch (requestCode) {

            case REQUEST_CODE_PICKFILE:
                if (resultCode == Activity.RESULT_OK) {
                    IoService.loadData(this, data);
                }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            finishAndRemoveTask();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, R.string.back_again, Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
    }
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        System.out.println(requestCode);
        switch (requestCode) {
            case 1: {

                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if(saveFlag){
                        IoService.saveAllData(this);
                    }
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(OldMainActivityII.this, "Permission denied to read your External storage", Toast.LENGTH_SHORT).show();
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

}
