package com.brth.android.cashflow.view.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.brth.android.cashflow.R;
import com.brth.android.cashflow.data.model.db_datas_entities.DbObjectEntity;
import com.brth.android.cashflow.data.model.db_datas_entities.PaymentEventEntity;
import com.brth.android.cashflow.persistance.db_table_services.FuelService;
import com.brth.android.cashflow.persistance.db_table_services.TagService;
import com.brth.android.cashflow.view.activities.managements.HaveOnlyNameManagementActivity;
import com.brth.android.cashflow.view.controller.adapters.OnlyNameArrayAdapter;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class HaveOnlyNameActivity extends Activity {

    final int REQUEST_CODE_NEW = 555;

    private ListView listViewDbTables;
    private ArrayAdapter<DbObjectEntity> arrayAdapter;
    private TextView header;
    private Button button;
    private String toSelect;
    private String selected;
    private List<DbObjectEntity> dataToListView;
    private Intent intentToManagement;
    private Set<Long> selectedIds ;
    private Long tagIdsBin;

    private PaymentEventEntity oldPaymentEventEntity;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        selectedIds = new HashSet<>();
        header = findViewById(R.id.tv_header);
        button = findViewById(R.id.bt_new_item);

        Intent intent = getIntent();
        toSelect = intent.getStringExtra("select");
        selected = intent.getStringExtra("selected");
        tagIdsBin = intent.getLongExtra("tagIdsBin",0);
        selectedIds.addAll( TagService.listOfIdsFromBinary(tagIdsBin));

        try {
            oldPaymentEventEntity = (PaymentEventEntity) intent.getExtras().get("old");
        } catch (Exception e) {
            oldPaymentEventEntity = null;
        }
        if(selected == null ) throw new NullPointerException("Non selected");
        findViewById(R.id.iv_back_empty_list).setVisibility(View.GONE);
        intentToManagement = new Intent
                (this, HaveOnlyNameManagementActivity.class);
        intentToManagement.putExtra("selected",selected);
        if (toSelect == null) {
            switch(selected){
                case "fuels":
                    header.setText(R.string.fuels);
                    button.setText(R.string.new_fuel);
                    break;
                case "tags":
                    header.setText(R.string.tags);
                    button.setText(R.string.new_tag);
                    break;
            }
        } else {
            button.setVisibility(View.GONE);
            switch(selected){
                case "fuels":
                   header.setText(R.string.select_fuel);
                    break;
                case "tags":
                    header.setText(R.string.select_tag);
                    button.setText(R.string.ready);
                    button.setVisibility(View.VISIBLE);
                    break;
            }

        }
    }

    private void fillUpData() {
        switch(selected){
            case "fuels":
                dataToListView = FuelService.getAllFuelEntities(this);
                break;
            case "tags":
                dataToListView = TagService.getAllTagEntities(this);
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        fillUpData();
        setUpAdapter();
    }

    private void setUpAdapter() {
        arrayAdapter = new OnlyNameArrayAdapter(this, dataToListView);
        ((OnlyNameArrayAdapter)arrayAdapter).setSelectedIds(selectedIds);

        listViewDbTables = findViewById(R.id.lv_items);
        listViewDbTables.setAdapter(arrayAdapter);
        listViewDbTables.setLongClickable(true);
        if (toSelect != null)
            listViewDbTables.setOnItemClickListener(new HaveOnlyNameActivity.MyItemClickListener());
        else
            listViewDbTables.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                @Override
                public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
                                               int pos, long id) {
                    String name = ((TextView) arg1.findViewById(R.id.name)).getText().toString();
                    intentToManagement.putExtra("name", name);
                    startActivity(intentToManagement);
                    return true;
                }
            });
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.bt_new_item:
                if(toSelect==null)  startActivityForResult(intentToManagement, REQUEST_CODE_NEW);
                else{
                    finish();
                }
                break;
        }
    }

    private class MyItemClickListener implements AdapterView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            DbObjectEntity entity =  dataToListView.get(position);
            TextView name = view.findViewById(R.id.name);
            if(toSelect!=null && selected.equals("tags") ) {
                if (selectedIds.contains(entity.getId())){
                    selectedIds.remove(entity.getId());
                    name.setText(entity.getName());
                }
                else {
                    selectedIds.add(entity.getId());
                    name.setText(entity.getName()+" ✓");
                }
            }
            Intent data = new Intent();
            data.putExtra("name",
                    ((TextView) view.findViewById(R.id.name)).getText().toString());
            data.putExtra("old",oldPaymentEventEntity);
            List<Long> tags = new ArrayList<>();
            tags.addAll(selectedIds);
            data.putExtra("tagIdsBin",TagService.generateBinaryFromList(tags));
            setResult(Activity.RESULT_OK, data);
             if (!selected.equals("tags"))finish();
        }
    }
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_CODE_NEW:
                onResume();
                break;
        }
    }

}