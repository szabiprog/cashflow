package com.brth.android.cashflow.data.model.db_data_tables;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import androidx.annotation.NonNull;

import com.brth.android.cashflow.R;
import com.brth.android.cashflow.data.model.db_datas_entities.DbObjectEntity;
import com.brth.android.cashflow.data.model.db_datas_entities.MoneyAccountEntity;

import java.util.ArrayList;
import java.util.List;

public class MoneyAccountsTable extends DbTable {


    public final static String NAME_TABLE = "money_accounts";
    public final static String ID_COLUMN = "id";
    public final static String NAME_COLUMN = "name";
    public final static String OWNER_ID_COLUMN = "project_id";
    public final static String DESCRIPTION_COLUMN = "description";
    public final static String BALANCE_COLUMN = "balance";
    public final static String CURRENCY_ID_COLUMN = "currency_id";
    public final static String[] ALL_COLUMN = {ID_COLUMN, NAME_COLUMN, OWNER_ID_COLUMN, DESCRIPTION_COLUMN,BALANCE_COLUMN,CURRENCY_ID_COLUMN};

    public MoneyAccountsTable(Context context) {
        super(context);
    }


    @Override
    public long insert(DbObjectEntity dbObjectEntity, SQLiteDatabase database) {
        MoneyAccountEntity p = (MoneyAccountEntity) dbObjectEntity;

        long index = database.insert(NAME_TABLE, null, dbObjectEntityToContentValues(p));
        p.setId(index);

        return index;

    }

    @Override
    public boolean update(DbObjectEntity dbObjectEntity, SQLiteDatabase database) {
        long r = database.update(NAME_TABLE, dbObjectEntityToContentValues(dbObjectEntity),
                ID_COLUMN + "=" + dbObjectEntity.getId(), null);
        return r == 1;
    }

    @Override
    public int delete(DbObjectEntity dbObjectEntity, SQLiteDatabase database) {

        return database.delete(NAME_TABLE,ID_COLUMN + "=" + dbObjectEntity.getId(), null);
    }

    @Override
    public void deleteAll( SQLiteDatabase database)
    {
        database.execSQL("delete from "+ NAME_TABLE);
        database.execSQL(String.format("DELETE FROM SQLITE_SEQUENCE WHERE NAME = '%s'", NAME_TABLE));
    }
    @Override
    public List<DbObjectEntity> selectAllDbObjectEntities( SQLiteDatabase database) {

        Cursor cursor = database.query(NAME_TABLE, ALL_COLUMN, null,
                null, null, null, ID_COLUMN + " asc");
        List<DbObjectEntity> dbObjectEntityList = new ArrayList<>();
        while (cursor.moveToNext()) {
            DbObjectEntity dbObjectEntity = cursorToDbObjectEntity(cursor);

                dbObjectEntityList.add(dbObjectEntity);

        }
        return dbObjectEntityList;
    }

    @Override
    public DbObjectEntity cursorToDbObjectEntity(Cursor cursor) {
        MoneyAccountEntity moneyAccountEntity = new MoneyAccountEntity();
        moneyAccountEntity.setId(cursor.getLong(0));
        moneyAccountEntity.setName(cursor.getString(1));
        moneyAccountEntity.setProjectId(cursor.getLong(2));
        moneyAccountEntity.setDescription(cursor.getString(3));
        moneyAccountEntity.setBalance(cursor.getLong(4));
        moneyAccountEntity.setCurrencyId(cursor.getLong(5));

        return moneyAccountEntity;
    }

    @Override
    public ContentValues dbObjectEntityToContentValues(DbObjectEntity dbObjectEntity) {
        MoneyAccountEntity p = (MoneyAccountEntity) dbObjectEntity;

        ContentValues values = new ContentValues();
        values.put(NAME_COLUMN, p.getName());
        values.put(OWNER_ID_COLUMN, p.getProjectId());
        values.put(DESCRIPTION_COLUMN, p.getDescription());
        values.put(BALANCE_COLUMN, p.getBalance());
        values.put(CURRENCY_ID_COLUMN,p.getCurrencyId());
        return values;
    }

    @Override
    public List<DbObjectEntity> selectDbObjectEntities( SQLiteDatabase database, String inColums, String values) {
        Cursor cursor;

        cursor = database.query(NAME_TABLE, ALL_COLUMN, inColums + "=?", new String[]{values}
                , null, null, ID_COLUMN + " asc");
        List<DbObjectEntity> dbObjectEntityList = new ArrayList<>();
        while (cursor.moveToNext()) {
            DbObjectEntity dbObjectEntity = cursorToDbObjectEntity(cursor);

                dbObjectEntityList.add(dbObjectEntity);

        }
        return dbObjectEntityList;
    }

    @Override
    public DbObjectEntity selectById(Long id, SQLiteDatabase database) {

        List<DbObjectEntity> dbObjectEntityList = new ArrayList<>();

        if(database.isOpen()){
            Cursor cursor;
            cursor = database.query(NAME_TABLE, ALL_COLUMN, ID_COLUMN + "=?", new String[]{id + ""}
                    , null, null, ID_COLUMN + " asc");
            while (cursor.moveToNext()) {
                DbObjectEntity dbObjectEntity = cursorToDbObjectEntity(cursor);
                dbObjectEntityList.add(dbObjectEntity);
            }
            cursor.close();
        }

        if(!dbObjectEntityList.isEmpty()) return dbObjectEntityList.get(0);
        else return null;
    }

    @Override
    public String createTable() {
        return "CREATE TABLE IF NOT EXISTS " + NAME_TABLE + " (" +
                ID_COLUMN + " integer primary key autoincrement, " + NAME_COLUMN + " text not null, "
                + OWNER_ID_COLUMN + " integer default 0, " + DESCRIPTION_COLUMN + " text not null,"
                + BALANCE_COLUMN +  " integer default 0, "+ CURRENCY_ID_COLUMN + " integer default 0 );";
    }

    @Override
    public String getTableName() {
        return NAME_TABLE;
    }

    @NonNull
    @Override
    public String toString() {
        return getContext().getString(R.string.money_accounts);
    }
}
