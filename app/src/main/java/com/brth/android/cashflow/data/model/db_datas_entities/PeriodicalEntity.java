package com.brth.android.cashflow.data.model.db_datas_entities;

import com.brth.android.cashflow.data.enums.DirectionType;
import com.brth.android.cashflow.data.enums.Periodic;

public class PeriodicalEntity extends DbObjectEntity {

    private long projectId;
    private long currencyId;
    private Periodic periodic;
    private long amount;
    private String description;
    private DirectionType directionType;
    private String startDate;
    private long alive;

    public PeriodicalEntity() {
    }

    public long getProjectId() {
        return projectId;
    }

    public void setProjectId(long projectId) {
        this.projectId = projectId;
    }

    public Periodic getPeriodic() {
        return periodic;
    }

    public void setPeriodic(Periodic periodic) {
        this.periodic = periodic;
    }

    public long getAmount() {
        return amount;
    }

    public void setAmount(long amount) {
        this.amount = amount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public DirectionType getDirectionType() {
        return directionType;
    }

    public void setDirectionType(DirectionType directionType) {
        this.directionType = directionType;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        startDate = startDate;
    }

    public long getAlive() {
        return alive;
    }

    public void setAlive(long alive) {
        this.alive = alive;
    }

    public long getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(long currencyId) {
        this.currencyId = currencyId;
    }
}
