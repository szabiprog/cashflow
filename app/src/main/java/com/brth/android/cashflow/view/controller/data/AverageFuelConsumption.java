package com.brth.android.cashflow.view.controller.data;

import com.brth.android.cashflow.data.model.db_datas_entities.PaymentEventEntity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class AverageFuelConsumption implements Serializable {
    private long projectId  ;
    private long startOdometer;
    private long lastOdometer;
    private long actualOdometer;
    private double lastAverage;
    private double partAverage;
    private double newAverage;
    private double allFuel;
    private long allMoney;
    private List<PaymentEventEntity> fuellings ;


    public AverageFuelConsumption(PaymentEventEntity paymentEventEntity) {
        fuellings = new ArrayList<>();
        this.projectId = paymentEventEntity.getProjectId();
        this.startOdometer = paymentEventEntity.getOdometer();
        this.lastOdometer =  paymentEventEntity.getOdometer();
        this.actualOdometer = paymentEventEntity.getOdometer();
        this.lastAverage = 0 ;
        this.partAverage  = 0 ;
        this.allFuel = Double.parseDouble(paymentEventEntity.getQuantity());
        this.allMoney = paymentEventEntity.getAmount();
        addFuelling(paymentEventEntity);
    }
    public boolean  addFuelling(PaymentEventEntity paymentEventEntity){
        if (paymentEventEntity.getProjectId() == projectId) {
            fuellings.add(paymentEventEntity);
            this.lastOdometer = this.actualOdometer;
            this.actualOdometer =  paymentEventEntity.getOdometer();
            this.allFuel +=((int) Double.parseDouble(paymentEventEntity.getQuantity())* 100)/100. ;
            this.allMoney += paymentEventEntity.getAmount();
            this.lastAverage = this.newAverage;
            this.newAverage = ((int) (this.allFuel  / (this.actualOdometer - this.startOdometer) * 10000))/100. ;
            this.partAverage =((int)(Double.parseDouble(paymentEventEntity.getQuantity())* 10000 /
                    (this.actualOdometer-this.lastOdometer))) /100.;
            return true;
        }else return false;
    }

    public double getAllFuel() {
        return allFuel;
    }

    public void setAllFuel(double allFuel) {
        this.allFuel = allFuel;
    }

    public long getAllMoney() {
        return allMoney;
    }

    public void setAllMoney(long allMoney) {
        this.allMoney = allMoney;
    }

    public List<PaymentEventEntity> getFuellings() {
        return fuellings;
    }

    public void setFuellings(List<PaymentEventEntity> fuellings) {
        this.fuellings = fuellings;
    }



    public long getProjectId() {
        return projectId;
    }

    public void setProjectId(long projectId) {
        this.projectId = projectId;
    }

    public long getStartOdometer() {
        return startOdometer;
    }

    public void setStartOdometer(long startOdometer) {
        this.startOdometer = startOdometer;
    }

    public long getLastOdometer() {
        return lastOdometer;
    }

    public void setLastOdometer(long lastOdometer) {
        this.lastOdometer = lastOdometer;
    }

    public long getActualOdometer() {
        return actualOdometer;
    }

    public void setActualOdometer(long actualOdometer) {
        this.actualOdometer = actualOdometer;
    }

    public double getLastAverage() {
        return lastAverage;
    }

    public void setLastAverage(double lastAverage) {
        this.lastAverage = lastAverage;
    }

    public double getPartAverage() {
        return partAverage;
    }

    public void setPartAverage(double partAverage) {
        this.partAverage = partAverage;
    }

    public double getNewAverage() {
        return newAverage;
    }

    public void setNewAverage(double newAverage) {
        this.newAverage = newAverage;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AverageFuelConsumption that = (AverageFuelConsumption) o;
        return projectId == that.projectId;
    }

    @Override
    public int hashCode() {
        return 0;
    }

    @Override
    public String toString() {
        return "AverageFuelConsumption{" +
                "projectId=" + projectId +
                ", startOdometer=" + startOdometer +
                ", lastOdometer=" + lastOdometer +
                ", actualOdometer=" + actualOdometer +
                ", lastAverage=" + lastAverage +
                ", partAverage=" + partAverage +
                ", newAverage=" + newAverage +
                ", allFuel=" + allFuel +
                ", allMoney=" + allMoney +

                '}';
    }
}
