package com.brth.android.cashflow.view.controller.models;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.content.ContextCompat;

import com.brth.android.cashflow.R;
import com.brth.android.cashflow.data.model.db_datas_entities.CarServiceEntity;
import com.brth.android.cashflow.data.model.db_datas_entities.CurrencyEntity;
import com.brth.android.cashflow.data.model.db_datas_entities.MoneyAccountEntity;
import com.brth.android.cashflow.data.model.db_datas_entities.PaymentEventEntity;
import com.brth.android.cashflow.data.model.db_datas_entities.ProjectEntity;
import com.brth.android.cashflow.persistance.db_table_services.CarServiceService;
import com.brth.android.cashflow.persistance.db_table_services.CurrencyService;
import com.brth.android.cashflow.persistance.db_table_services.MoneyAccountService;
import com.brth.android.cashflow.persistance.db_table_services.PaymentEventService;
import com.brth.android.cashflow.persistance.db_table_services.ProjectService;
import com.brth.android.cashflow.view.activities.HaveOnlyNameActivity;
import com.brth.android.cashflow.view.activities.MoneyAccountsActivity;
import com.brth.android.cashflow.view.activities.ProjectsActivity;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class PaymentEventManagementViewModel extends Views {


    private static PaymentEventManagementViewModel instance;
    final int REQUEST_CODE_PROJECT = 111;
    final int REQUEST_CODE_MONEY_ACC = 333;
    final int REQUEST_CODE_TAG = 444;

    private static List<Views> history;
    private Activity mainActivity;
    private Intent incomingIntent;
    private Button btProjectSelect;
    private Button btTagSelect;
    private Button btSwitchType;
    private Button buttonSave;
    private Button btMoneyAccountSelect;
    private Switch btSwMoneyMoveDirection;
    private EditText textDescription;
    private EditText textMoney;
    private TextView tvCurrencyName;
    private TextView tvDate;
    private TextView tvDateInterval;
    private EditText textOdometer;
    private EditText textQuantity;
    private EditText textOdometerInterval;
    private LinearLayout lnOdometer;
    private LinearLayout lnDateInterval;
    private LinearLayout lnOdometerInterval;
    private LinearLayout lnQuantity;

    static private String getMoney;
    static private String spendMoney;
    static private String currentDateTime = null;
    static private String dateOfNextService;

    static private boolean update;
    static private PaymentEventEntity paymentEventEntity;
    static private CarServiceEntity carServiceEntity;
    static private PaymentEventEntity oldPaymentEventEntity;
    static private Calendar myCalendar;
    static private DatePickerDialog.OnDateSetListener date;

    private PaymentEventManagementViewModel() {
    }

    public static synchronized PaymentEventManagementViewModel
    getInstance(List<Views> history, Bundle savedInstanceState) {
        PaymentEventManagementViewModel.history = history;
        if (instance == null) {
            instance = new PaymentEventManagementViewModel();
        }
        instance.setSavedInstanceState(savedInstanceState);
        history.add(instance);
        return instance;
    }


    void onMyCreate(Activity a) {
        paymentEventEntity = null;
        a.setContentView(R.layout.activity_outlay);
        buttonSave = a.findViewById(R.id.bt_save);
        btSwitchType = a.findViewById(R.id.switch_service_fuel_normal);
        btProjectSelect = a.findViewById(R.id.bt_select_project);
        btTagSelect = a.findViewById(R.id.bt_select_tag);
        btMoneyAccountSelect = a.findViewById(R.id.bt_select_account);
        textDescription = a.findViewById(R.id.text_description);
        tvCurrencyName = a.findViewById(R.id.tv_currency_name);
        btSwMoneyMoveDirection = a.findViewById(R.id.switch_money_move_direction);
        textOdometer = a.findViewById(R.id.text_odometer);
        textQuantity = a.findViewById(R.id.text_quantity);
        lnOdometer = a.findViewById(R.id.ln_odometer);
        lnQuantity = a.findViewById(R.id.ln_quantity);
        lnDateInterval = a.findViewById(R.id.ln_date_interval);
        lnOdometerInterval = a.findViewById(R.id.ln_odometer_interval);
        textMoney = a.findViewById(R.id.text_money);
        tvDate = a.findViewById(R.id.tv_currentDate);
        tvDateInterval = a.findViewById(R.id.tv_date_interval);
        textOdometerInterval = a.findViewById(R.id.text_odometer_interval);
        getMoney = a.getString(R.string.get_money);
        spendMoney = a.getString(R.string.spend_money);
        btSwMoneyMoveDirection.setBackground(ContextCompat.getDrawable
                (a, R.drawable.button_green));

        datePicker();
        tvDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(a, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd. HH:mm:ss");
        currentDateTime = sdf.format(new Date());

        incomingIntent = a.getIntent();
        buttonSave.setText(a.getResources().getString(R.string.save));

        String dateOfEntity = incomingIntent.getStringExtra("date");
        update = false;
        update = (dateOfEntity != null);
        if (update) {
            paymentEventEntity = (PaymentEventEntity)
                    PaymentEventService.allPaymentsByADate(dateOfEntity, a).get(0);
            if (oldPaymentEventEntity == null) {
                oldPaymentEventEntity =
                        PaymentEventService.clonePaymentEntityInverseDirection(paymentEventEntity);
            }
            setUpByEntity(false);
        } else {
            if (getSavedInstanceState() != null) {
                paymentEventEntity =
                        (PaymentEventEntity) getSavedInstanceState().getSerializable("payment");
            }
            if (paymentEventEntity == null) {
                paymentEventEntity = PaymentEventService.createPaymentEntity(currentDateTime);
                btSwMoneyMoveDirection.setChecked(true);
                btSwMoneyMoveDirection.setText(getMoney);
            }

            carServiceEntity = new CarServiceEntity();
            carServiceEntity.setOdometerInterval(0);
            dateOfNextService = "";
        }
        selectViewOfBtType();
        tvDate.setText(paymentEventEntity.getDate());
        btSwMoneyMoveDirection.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView,
                                         boolean isChecked) {
                long moneyMoveDirection = paymentEventEntity.getDirectionType() * -1;
                paymentEventEntity.setDirectionType(moneyMoveDirection);
                setUpMoneyMoveDirection();
            }
        });
    }

    void selectViewOfBtType() {
        switch ((int) paymentEventEntity.getType()) {
            case (int) PaymentEventEntity.NORMAL_TYPE:
                lnOdometer.setVisibility(View.GONE);
                lnQuantity.setVisibility(View.GONE);
                lnOdometerInterval.setVisibility(View.GONE);
                lnDateInterval.setVisibility(View.GONE);
                btSwitchType.setText(R.string.normal);
                break;
            case (int) PaymentEventEntity.FUEL_TYPE:
                lnQuantity.setVisibility(View.VISIBLE);
                lnOdometer.setVisibility(View.VISIBLE);
                lnOdometerInterval.setVisibility(View.GONE);
                lnDateInterval.setVisibility(View.GONE);
                btSwitchType.setText(R.string.Fuel);
                break;
            case (int) PaymentEventEntity.SERVICE_TYPE:
                lnQuantity.setVisibility(View.GONE);
                lnOdometer.setVisibility(View.VISIBLE);
                lnOdometerInterval.setVisibility(View.VISIBLE);
                lnDateInterval.setVisibility(View.VISIBLE);
                btSwitchType.setText(R.string.Service);
                break;
        }
    }

    void updateLabel() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd. HH:mm:ss");
        tvDate.setText(sdf.format(myCalendar.getTime()));
        currentDateTime = (String) tvDate.getText();
        paymentEventEntity.setDate(currentDateTime);
    }

    void setupFromAccByName(String nameOfMoneyAccount) {
        MoneyAccountEntity mAcc = MoneyAccountService.selectByName(nameOfMoneyAccount, mainActivity);
        if (mAcc != null) {
            long moneyAccountId = mAcc.getId();
            paymentEventEntity.setMoneyAccountId(moneyAccountId);
            long currencyId = mAcc.getCurrencyId();
            paymentEventEntity.setCurrencyId(currencyId);
            CurrencyEntity currencyEntity = CurrencyService.selectById(currencyId, mainActivity);
            String nameOfCurrency = currencyEntity.getName();
            tvCurrencyName.setText(nameOfCurrency);
            paymentEventEntity.setCurrencyId(currencyId);
        } else {
            //hibakezelés
            System.out.println("visszatért2");
        }
    }

    void sendToast(String str) {
        Toast toast = Toast.makeText(mainActivity, str, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 0, 0);
        toast.show();
    }

    void sendToast(int str) {
        Toast toast = Toast.makeText(mainActivity, str, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 0, 0);
        toast.show();
    }

    void datePicker() {
        myCalendar = Calendar.getInstance();
        date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }
        };
    }

    long saveEntity() {
        long projectAccId = paymentEventEntity.getProjectId();
        long moneyAccountId = paymentEventEntity.getMoneyAccountId();
        getDatasFromForm();
        if (projectAccId > 0 && moneyAccountId > 0 && PaymentEventService.goodForFuelType(paymentEventEntity) ) {
            if (update) {
                boolean backwards =
                        PaymentEventService.update(paymentEventEntity, oldPaymentEventEntity, mainActivity);
                if (!backwards) {
                    sendToast(R.string.backwards_error);
                }
                return paymentEventEntity.getId();
            } else {
                paymentEventEntity.setDate(currentDateTime);
                return PaymentEventService.save(paymentEventEntity, mainActivity);
            }
        } else sendToast(R.string.invalid_datas);
        return 0;
    }


    private void alertDialog() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(mainActivity);
        dialog.setMessage(R.string.alert_update);
        dialog.setTitle(R.string.update);
        dialog.setPositiveButton(R.string.yes,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,
                                        int which) {
                        String moneyValue = textMoney.getText().toString();
                        if (paymentEventEntity.getMoneyAccountId() > 0 &&
                                paymentEventEntity.getProjectId() > 0 &&
                                moneyValue.length() > 0 && Integer.valueOf(moneyValue) > 0) {
                            if (saveEntity() != 0) {
                                //   mainActivity.onBackPressed();// manual kell

                                mainActivity.onBackPressed();
                            }
                        } else {
                            sendToast(R.string.select_project_and_acc_and_price);
                            buttonSave.setClickable(true);
                        }
                    }
                });
        dialog.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                buttonSave.setClickable(true);
            }
        });
        AlertDialog alertDialog = dialog.create();
        alertDialog.show();
    }

    private void setUpByEntity(boolean update) {

        selectViewOfBtType();
        String quantity = paymentEventEntity.getQuantity();
        textQuantity.setText(quantity);
        textOdometer.setText(String.valueOf(paymentEventEntity.getOdometer()));
        CarServiceEntity carServiceEntity =
                CarServiceService.selectById(paymentEventEntity.getServiceId(), mainActivity);
        if (carServiceEntity != null) {
            textOdometerInterval.setText(String.valueOf(carServiceEntity.getOdometerInterval()));
            dateOfNextService = carServiceEntity.getDateOfNextService();
            tvDateInterval.setText(dateOfNextService);
        } else {
            textOdometerInterval.setText(String.valueOf(0));
            dateOfNextService = "";
            tvDateInterval.setText("");
        }
        currentDateTime = paymentEventEntity.getDate();
        tvDate.setText(currentDateTime);
        if (paymentEventEntity.getProjectId() > 0) {
            ProjectEntity projectEntity =
                    ProjectService.selectById(paymentEventEntity.getProjectId(), mainActivity);
            String nameOfProject = projectEntity.getName();
            btProjectSelect.setText(nameOfProject);
        }
        long moneyAccountId = paymentEventEntity.getMoneyAccountId();
        if (moneyAccountId > 0) {
            MoneyAccountEntity moneyAccountEntity =
                    MoneyAccountService.selectById(moneyAccountId, mainActivity);
            String nameOfMoneyAccount = moneyAccountEntity.getName();
            btMoneyAccountSelect.setText(nameOfMoneyAccount);
            long currencyId = moneyAccountEntity.getCurrencyId();
            String nameOfCurrency = CurrencyService.selectById(currencyId, mainActivity).getName();
            tvCurrencyName.setText(nameOfCurrency);
        }

        textDescription.setText(paymentEventEntity.getDescription());
        textMoney.setText(String.format("%d", paymentEventEntity.getAmount()));
        setUpMoneyMoveDirection();
    }

    private long setUpMoneyMoveDirection() {
        if (paymentEventEntity.getDirectionType() > 0) {
            btSwMoneyMoveDirection.setText(getMoney);
            btSwMoneyMoveDirection.setBackground(ContextCompat.getDrawable
                    (mainActivity, R.drawable.button_green));
            return 1;
        } else {
            btSwMoneyMoveDirection.setText(spendMoney);
            btSwMoneyMoveDirection.setBackground(ContextCompat.getDrawable
                    (mainActivity, R.drawable.button_red));
            return -1;
        }
    }

    private void setupScreen() {
        if (paymentEventEntity.getId() > 0) setUpByEntity(true);
        else setUpByEntity(false);
    }

    public void getDatasFromForm() {
        String moneyValue = textMoney.getText().toString();
        long amount = Long.parseLong(moneyValue);
        paymentEventEntity.setAmount(amount);
        paymentEventEntity.setDescription(textDescription.getText().toString());
        long odometer = 0;
        try {
            odometer = Long.valueOf(textOdometer.getText().toString());
        } catch (NumberFormatException e) {
            sendToast(R.string.odometer_value_problem);
        }
        paymentEventEntity.setOdometer(odometer);
        paymentEventEntity.setQuantity(textQuantity.getText().toString());
    }

    @Override
    public void onResume(Context context) {
        mainActivity = (Activity) context;
        onMyCreate(mainActivity);
        setupScreen();
    }

    @Override
    synchronized public void onClick(View view) {
        Intent intent;
        switch (view.getId()) {
            case R.id.switch_service_fuel_normal:
                long paymentType = PaymentEventService.switchType(paymentEventEntity.getType());
                paymentEventEntity.setType(paymentType);
                selectViewOfBtType();
                break;
            case R.id.bt_select_project:
                intent = new Intent(mainActivity, ProjectsActivity.class);
                intent.putExtra("select", "select");
                if (update) intent.putExtra("old", oldPaymentEventEntity);
                mainActivity.startActivityForResult(intent, REQUEST_CODE_PROJECT);
                break;
            case R.id.bt_select_tag:
                intent = new Intent(mainActivity, HaveOnlyNameActivity.class);
                intent.putExtra("select", "select");
                intent.putExtra("selected", "tags");
                if (update) intent.putExtra("old", oldPaymentEventEntity);
                intent.putExtra("tagIdsBin", paymentEventEntity.getTagIdsBin());
                mainActivity.startActivityForResult(intent, REQUEST_CODE_TAG);
                break;
            case R.id.bt_select_account:
                intent = new Intent(mainActivity, MoneyAccountsActivity.class);
                intent.putExtra("select", "select");
                if (update) intent.putExtra("old", oldPaymentEventEntity);
                mainActivity.startActivityForResult(intent, REQUEST_CODE_MONEY_ACC);
                break;
            case R.id.bt_save:
                buttonSave.setClickable(false);
                if (update) alertDialog();
                else {
                    String moneyValue;
                    try {
                        moneyValue = textMoney.getText().toString();
                    } catch (Exception e) {
                        moneyValue = null;
                    }
                    if (paymentEventEntity.getMoneyAccountId() > 0 &&
                            paymentEventEntity.getProjectId() > 0 &&
                            moneyValue != null && moneyValue.length() > 0 && Integer.valueOf(moneyValue) > 0) {
                        if (saveEntity() != 0) mainActivity.onBackPressed();
                    } else {
                        sendToast(R.string.select_project_and_acc_and_price);
                        buttonSave.setClickable(true);
                    }
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void putToSaveInstanceState(Bundle outState) {
        getDatasFromForm();
        outState.putSerializable("payment", paymentEventEntity);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_CODE_PROJECT:
                if (resultCode == mainActivity.RESULT_OK) {
                    String nameOfProject = data.getStringExtra("name");
                    btProjectSelect.setText(nameOfProject);
                    long projectAccId = ProjectService.getIdByName(mainActivity, nameOfProject);
                    paymentEventEntity.setProjectId(projectAccId);
                }
                break;
            case REQUEST_CODE_MONEY_ACC:
                if (resultCode == mainActivity.RESULT_OK) {
                    String nameOfMoneyAccount = data.getStringExtra("name");
                    oldPaymentEventEntity = (PaymentEventEntity) data.getExtras().get("old");
                    btMoneyAccountSelect.setText(nameOfMoneyAccount);
                    setupFromAccByName(nameOfMoneyAccount);
                }
                break;
            case REQUEST_CODE_TAG:
                if (resultCode == mainActivity.RESULT_OK) {
                    String name = data.getStringExtra("name");
                    oldPaymentEventEntity = (PaymentEventEntity) data.getExtras().get("old");
                    //btTagSelect.setText(name);
                    Long tagIdsBin = data.getLongExtra("tagIdsBin", 0);

                    paymentEventEntity.setTagIdsBin(tagIdsBin);
                }
                break;
        }
    }
}

