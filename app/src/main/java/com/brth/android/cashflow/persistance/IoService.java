package com.brth.android.cashflow.persistance;

import static android.os.Environment.DIRECTORY_DOWNLOADS;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.widget.Toast;

import com.brth.android.cashflow.persistance.db_table_services.DbService;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

public class IoService {
    private static File dir = Environment.getExternalStoragePublicDirectory(DIRECTORY_DOWNLOADS);

    private IoService() {
    }

    public static boolean saveAllData(Context context){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss");
        String currentDateTime = sdf.format(new Date());
        File file = new File(dir, "cashflow_" + currentDateTime + ".txt");
        Toast.makeText(context, String.valueOf("save"), Toast.LENGTH_SHORT).show();
        String json = DbService.getAllEntitiesFromDatabase(context);
       try {
            System.out.println(file);
           // OutputStream outputStream = new FileOutputStream(file, false);
           ParcelFileDescriptor a = context.getContentResolver().openFileDescriptor(Uri.fromFile(file), "w", null);
           OutputStream  outputStream = new FileOutputStream(a.getFileDescriptor());
           outputStream.write(json.getBytes());
           outputStream.close();
           return true;

        } catch (IOException e) {
            Toast.makeText(context, String.valueOf(e), Toast.LENGTH_SHORT).show();
            System.out.println(e.toString());
        }
        return false;
    }

    public static boolean loadData(Context context, Intent data){

        Uri selectedfile_uri = data.getData();

        Uri fileUri = data.getData();
        String filePath = fileUri.getPath();
        File file = new File(filePath, filePath);
        //System.out.println(fileUri);
        //System.out.println(filePath);
        String t = "";

        try (InputStream inputStream = context.getContentResolver().openInputStream(selectedfile_uri)) {
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
            //InputStreamReader inputStreamReader = new FileReader(file);
            BufferedReader reader = new BufferedReader(inputStreamReader);

            String row;
            while ((row = reader.readLine()) != null) {
                t += row;
                //  System.out.println(row+'\n');
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        //  Toast.makeText(this, DbService.reBuildDatabaseFromGson(this,t), Toast.LENGTH_LONG).show();
        if (t.length() > 10) DbService.reBuildDatabaseFromGson(context, t);

        return true ;
    }
}
