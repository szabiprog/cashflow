package com.brth.android.cashflow.view.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.brth.android.cashflow.R;
import com.brth.android.cashflow.data.model.db_data_tables.ProjectsTable;
import com.brth.android.cashflow.data.model.db_datas_entities.DbObjectEntity;
import com.brth.android.cashflow.data.model.db_datas_entities.ProjectEntity;
import com.brth.android.cashflow.persistance.db_table_services.ProjectService;
import com.brth.android.cashflow.view.activities.managements.ProjectManagementActivity;
import com.brth.android.cashflow.view.controller.adapters.ProjectArrayAdapter;
import com.brth.android.cashflow.view.controller.adapters.ProjectArrayAdapterToSelect;

import java.text.NumberFormat;
import java.util.List;

public class ProjectsActivity extends AppCompatActivity {

    private ListView listViewDbTables;
    private ArrayAdapter<DbObjectEntity> arrayAdapter;
    private TextView header;
    private Button button;
    private String ownerId;
    private String grandParentId;
    private String toSelect;
    private ProjectsTable projectsTable;
    private List<DbObjectEntity> projects;
    private ProjectEntity owner;
    private TextView parentHead;
    private TextView tvOrder;
    private LinearLayout summaLl;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        projectsTable = new ProjectsTable(this);
        header = findViewById(R.id.tv_header);
        button = findViewById(R.id.bt_new_item);
        parentHead = findViewById(R.id.parent_head);
        tvOrder = findViewById(R.id.tv_order);

        Intent intent = getIntent();
        ownerId = intent.getStringExtra("ownerId");
        if (ownerId == null) {
            ownerId = "0";
            owner = null;
        }
        summaLl = findViewById(R.id.ll_period);
        View viewToLoad = LayoutInflater.from(
                getApplicationContext()).inflate(
                R.layout.row_inn_out_stat, null);

        View row = viewToLoad.findViewById(R.id.row_in_out_stat);
        summaLl.addView(row);

        toSelect = intent.getStringExtra("select");

        setUpAdapter();
        setupHeader();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setUpAdapter();
        setupHeader();
    }

    private void setupHeader() {
        if(owner!=null && toSelect == null){
            String headetText = "";
            TextView tvDdate = ((View) summaLl).findViewById(R.id.tv_currentDate);
            tvDdate.setPaddingRelative(0, 0, 100, 0);
            TextView tvInn = summaLl.findViewById(R.id.tv_inn);
            TextView tvOut = summaLl.findViewById(R.id.tv_out);
            summaLl.setVisibility(View.VISIBLE);
            String lineHead = owner.getName();
           // List<DbObjectEntity> list = PaymentEventService.paymentEventsByProjectTree( owner.getId(),this);
           // long inn = PaymentEventService.moving(this, list, 1);
            //long out = PaymentEventService.moving(this, list, -1);

            long inn = owner.getInn();
            long out =owner.getOut();

            tvInn.setText(NumberFormat.getInstance().format(inn));
            tvOut.setText(NumberFormat.getInstance().format(out));

            tvDdate.setText(lineHead);
        }else summaLl.setVisibility(View.GONE);

    }

    private void setUpAdapter() {
        if (Long.valueOf(ownerId) > 0) {
            owner = ProjectService.selectById(Long.valueOf(ownerId),this);
            parentHead.setVisibility(View.VISIBLE);
            parentHead.setText(owner.getName());
        } else {
            owner = null;
            parentHead.setVisibility(View.GONE);
            parentHead.setText("...");
        }

        projects = ProjectService.selectAllByOwnerId(ownerId,this);

        if (ownerId.equals("0")) {
            grandParentId = "0";
        } else {
            grandParentId = ((ProjectEntity)
                    ProjectService.selectById(Long.parseLong(ownerId),this)).getOwnerId()+"";
        }
        if (toSelect == null) {
            header.setText(R.string.projects);
            button.setText(R.string.new_project);
            arrayAdapter = new ProjectArrayAdapter(this, projects);
        } else {
            tvOrder.setVisibility(View.VISIBLE);
            tvOrder.setText(R.string.long_press_select);
            header.setText(R.string.select_project);
            button.setVisibility(View.GONE);
            arrayAdapter = new ProjectArrayAdapterToSelect(this, projects);
        }
        listViewDbTables = findViewById(R.id.lv_items);
        listViewDbTables.setAdapter(arrayAdapter);
        listViewDbTables.setLongClickable(true);
        listViewDbTables.setOnItemClickListener(new MyItemClickListener());
        listViewDbTables.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
                                           int pos, long id) {
                String name = ((TextView) arg1.findViewById(R.id.name)).getText().toString();
                Intent intent = new Intent();
                if (toSelect != null) {
                    intent.putExtra("name", name);
                    setResult(Activity.RESULT_OK, intent);
                    finish();
                } else {
                    intent = new Intent
                            (ProjectsActivity.this, ProjectManagementActivity.class);
                    intent.putExtra("name", name);
                    ProjectsActivity.this.startActivity(intent);
                }

                return true;
            }
        });

    }

    public void onClick(View view) {
        Intent intent;
        switch (view.getId()) {
            case R.id.bt_new_item:
                intent = new Intent
                        (ProjectsActivity.this, ProjectManagementActivity.class);
                intent.putExtra("ownerId", ownerId);
                ProjectsActivity.this.startActivity(intent);
                break;
            case R.id.iv_back_empty_list:

                if(ownerId.equals(grandParentId))finish();
                ownerId = grandParentId;
                onResume();
                break;
            case R.id.ll_for_list:
                intent = new Intent
                        (ProjectsActivity.this, PaymentEventsActivity.class);
                intent.putExtra("projectId", view.getTag().toString());
                ProjectsActivity.this.startActivity(intent);

                break;
        }
    }


    private class MyItemClickListener implements AdapterView.OnItemClickListener {


        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            ownerId = ((TextView) view.findViewById(R.id.tv_id)).getText().toString();
            onResume();
        }
    }



}
