package com.brth.android.cashflow.view.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.brth.android.cashflow.R;
import com.brth.android.cashflow.data.model.db_datas_entities.PaymentEventEntity;
import com.brth.android.cashflow.persistance.db_table_services.PaymentEventService;
import com.brth.android.cashflow.view.activities.managements.PaymentEventManagementActivity;
import com.brth.android.cashflow.view.controller.adapters.PaymentBySomethingArrayAdapter;
import com.brth.android.cashflow.view.controller.adapters.PaymentEventArrayAdatpter;
import com.brth.android.cashflow.view.controller.adapters.PeriodListArrayAdapter;
import com.brth.android.cashflow.view.controller.data.ElementFromEntity;
import com.brth.android.cashflow.view.controller.data.SelectedPeriodToView;

import java.text.NumberFormat;
import java.util.Calendar;
import java.util.List;

public class PaymentsByPeriodActivity extends AppCompatActivity{
    public static final int SWIPE_THRESHOLD = 100;
    public static final int SWIPE_VELOCITY_THRESHOLD = 100;
    private ListView listViewDbTables;
    private TextView header;
    private Intent arrivedIntent;
    private LinearLayout summaryLl;
    private GestureDetector gestureDetector;
    SelectedPeriodToView selectedPeriodToView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        findViewById(R.id.bt_new_item).setVisibility(View.GONE);
        findViewById(R.id.iv_back_empty_list).setVisibility(View.INVISIBLE);
        arrivedIntent = getIntent();
        selectedPeriodToView = (SelectedPeriodToView) arrivedIntent.getSerializableExtra("selected period");
        listViewDbTables = findViewById(R.id.lv_items);

        header = findViewById(R.id.tv_header);

        summaryLl = findViewById(R.id.ll_period);
        View viewToLoad = LayoutInflater.from(
                getApplicationContext()).inflate(
                R.layout.row_inn_out_stat, null);

        View row = viewToLoad.findViewById(R.id.row_in_out_stat);
        (row.findViewById(R.id.tv_currentDate)).setOnClickListener(new MyItemClickListener());
        summaryLl.addView(row);
        GestureDetector.OnGestureListener listener= new GestureDetector.OnGestureListener() {

            @Override
            public boolean onDown(MotionEvent motionEvent) {
                return false;
            }

            @Override
            public void onShowPress(MotionEvent motionEvent) {      }

            @Override
            public boolean onSingleTapUp(MotionEvent motionEvent) {
                return false;
            }

            @Override
            public boolean onScroll(MotionEvent motionEvent, MotionEvent motionEvent1, float v, float v1) {
                return false;
            }

            @Override
            public void onLongPress(MotionEvent motionEvent) {  }

            @Override
            public boolean onFling(MotionEvent downEvent, MotionEvent moveEvent, float velocityX, float velocityY) {

                float movedX = moveEvent.getX() - downEvent.getX();
                float movedY = moveEvent.getY() - downEvent.getY();

                if (Math.abs(movedX) > SWIPE_THRESHOLD && Math.abs(velocityX) > SWIPE_VELOCITY_THRESHOLD) {
                    if (movedX > 0) onSwipeRight();
                    else onSwipeLeft();
                    selectedPeriodToView.resetSelectedDay();
                    onResume();
                    return true;
                }
                return false;
            }
        };
        gestureDetector = new GestureDetector(this, listener );
    }

    private void setupHeader() {
        String headerText = "";
        TextView tvDate = ((View) summaryLl).findViewById(R.id.tv_currentDate);
        tvDate.setPaddingRelative(0, 0, 5, 0);
        TextView tvInn = summaryLl.findViewById(R.id.tv_inn);
        TextView tvOut = summaryLl.findViewById(R.id.tv_out);
        summaryLl.setVisibility(View.VISIBLE);

        String lineHead = "";


        switch (selectedPeriodToView.getPeriod()) {
            case SelectedPeriodToView.PERIOD_BY_DAY:
                headerText = getString(R.string.Daily_flow_of_money);
                lineHead = selectedPeriodToView.getSelectedDay();
                selectedPeriodToView.setDirection(1);
                selectedPeriodToView.setInnAmount(PaymentEventService.dailyMoving(this,
                        selectedPeriodToView));
                selectedPeriodToView.setDirection(-1);
                selectedPeriodToView.setOutAmount(PaymentEventService.dailyMoving(this,
                        selectedPeriodToView));

                break;
            case SelectedPeriodToView.PERIOD_BY_WEEK:
                headerText = getString(R.string.weekly_flow_of_money);
                lineHead = selectedPeriodToView.getSelectedDay().substring(0, 5) + " / "
                        + selectedPeriodToView.getMyCalendar().get(Calendar.WEEK_OF_YEAR) + ".";
                selectedPeriodToView.setInnAmount(PaymentEventService.weeklyMoving(this,
                        selectedPeriodToView.getMyCalendar(), 1));
                selectedPeriodToView.setOutAmount(PaymentEventService.weeklyMoving(this,
                        selectedPeriodToView.getMyCalendar(), -1));
                break;
            case SelectedPeriodToView.PERIOD_BY_MONTH:
                lineHead = selectedPeriodToView.getSelectedDay().substring(0, 8);
                headerText = getString(R.string.monthly_flow_of_money);

                selectedPeriodToView.setDirection(1);
                selectedPeriodToView.setInnAmount(PaymentEventService.monthlyMoving(this,
                     selectedPeriodToView));
                selectedPeriodToView.setDirection(-1);
                selectedPeriodToView.setOutAmount(PaymentEventService.monthlyMoving(this,
                        selectedPeriodToView));
                break;
            case SelectedPeriodToView.PERIOD_BY_YEAR:
                headerText = getString(R.string.yearly_flow_of_money);
                lineHead = selectedPeriodToView.getSelectedDay().substring(0, 5);
                selectedPeriodToView.setInnAmount(PaymentEventService.yearlyMoving(this,
                        selectedPeriodToView.getMyCalendar(), 1));
                selectedPeriodToView.setOutAmount(PaymentEventService.yearlyMoving(this,
                        selectedPeriodToView.getMyCalendar(), -1));
                break;
        }
        String inn = NumberFormat.getInstance().format(selectedPeriodToView.getInnAmount());
        String out = NumberFormat.getInstance().format(selectedPeriodToView.getOutAmount());
        tvInn.setText(inn);
        tvOut.setText(out);

        tvDate.setText(lineHead);
        tvDate.setTag(lineHead);
        tvDate.setOnClickListener(new MyItemClickListener());
        header.setText(headerText);
    }

    @Override
    protected void onResume() {
        setupHeader();
        setUpAdapter();
        super.onResume();

    }

    private void setUpAdapter() {
        String headerBase = (String) header.getText();
        List<ElementFromEntity> elementFromEntities ;
        switch(selectedPeriodToView.getListBy()){
            case SelectedPeriodToView.LIST_BY_DEFAULT:
                switch (selectedPeriodToView.getPeriod()) {
                    case SelectedPeriodToView.PERIOD_BY_DAY:
                        listViewDbTables.setAdapter(new PaymentEventArrayAdatpter
                                (this,  PaymentEventService.allPaymentsByADate
                                        (selectedPeriodToView.getSelectedDay(), this)));
                        setAdapterByDefault(SelectedPeriodToView.PERIOD_BY_DAY);
                        setAdapterByDefaultToDay();
                        break;
                    case SelectedPeriodToView.PERIOD_BY_WEEK:
                        listViewDbTables.setAdapter(new PeriodListArrayAdapter
                                (this, PaymentEventService.makeWeekOfSelectedDay
                                        (this,selectedPeriodToView)));
                        setAdapterByDefault(SelectedPeriodToView.PERIOD_BY_DAY);

                        break;
                    case SelectedPeriodToView.PERIOD_BY_MONTH:
                        listViewDbTables.setAdapter(new PeriodListArrayAdapter
                                (this, PaymentEventService.makeListOfDayBySelectedMonth
                                        (this,selectedPeriodToView)));
                        setAdapterByDefault(SelectedPeriodToView.PERIOD_BY_DAY);

                        break;
                    case SelectedPeriodToView.PERIOD_BY_YEAR:
                        listViewDbTables.setAdapter(new PeriodListArrayAdapter
                                (this, PaymentEventService.makeListOfMonthsOfSelectedYear
                                        (this, selectedPeriodToView)));
                        setAdapterByDefault(SelectedPeriodToView.PERIOD_BY_MONTH);
                        break;
                }
                break;
            default:
                switch (selectedPeriodToView.getListBy()) {
                    case SelectedPeriodToView.LIST_BY_PROJECT:
                        headerBase = headerBase + " " + getString(R.string.by_project);
                        break;
                    case SelectedPeriodToView.LIST_BY_CURRENCY:
                        headerBase = headerBase + " "+ getString(R.string.by_currency);
                        break;
                    case SelectedPeriodToView.LIST_BY_MONEY_ACC:
                        headerBase = headerBase + " " + getString(R.string.by_money_acc);
                        break;
                    case SelectedPeriodToView.LIST_BY_TAG:
                        headerBase = headerBase + " "+ getString(R.string.by_tag);
                        break;
                    default:
                        break;
                }
                 elementFromEntities =
                         PaymentEventService.makeListOfElementFromEntity(this,selectedPeriodToView);
                listViewDbTables.setAdapter(new PaymentBySomethingArrayAdapter
                        (this, elementFromEntities));
                listViewDbTables.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                        ElementFromEntity p = (ElementFromEntity) listViewDbTables.getAdapter().getItem(i);
                        Intent intent = new Intent
                                (PaymentsByPeriodActivity.this, PaymentEventsActivity.class);
                        intent.putExtra("payments", p);
                        PaymentsByPeriodActivity.this.startActivity(intent);
                    }
                });
                break;
        }
        header.setText(headerBase);
    }

    private void setAdapterByDefaultToDay() {
        listViewDbTables.setLongClickable(true);
        listViewDbTables.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
                                           int pos, long id) {
                PaymentEventEntity p = (PaymentEventEntity) listViewDbTables.getAdapter().getItem(pos);
                if(p.getValid()!=PaymentEventEntity.INVALID & p.getValid()!=PaymentEventEntity.TRANSFER){
                    String date = ((TextView) arg1.findViewById(R.id.tv_currentDate)).getText().toString();
                    Intent intent = new Intent
                            (PaymentsByPeriodActivity.this, PaymentEventManagementActivity.class);
                    intent.putExtra("date", date);
                    PaymentsByPeriodActivity.this.startActivity(intent);
                }
                return true;
            }
        });
    }

    private void setAdapterByDefault(int period) {
        listViewDbTables.setLongClickable(true);
        listViewDbTables.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
                                           int pos, long id) {
                SelectedPeriodToView p = (SelectedPeriodToView) listViewDbTables.getAdapter().getItem(pos);
                p.setPeriod(period);
                Intent intent = new Intent
                        (PaymentsByPeriodActivity.this, PaymentsByPeriodActivity.class);
                intent.putExtra("selected period", p);
                PaymentsByPeriodActivity.this.startActivity(intent);
                return true;
            }
        });
    }


    private class MyItemClickListener implements View.OnClickListener {


        @Override
        public void onClick(View view) {
            Intent intent;
            switch (view.getId()) {
                case R.id.tv_currentDate:
                    selectedPeriodToView.changeListType();
                    String period = view.getTag().toString();
                    intent = new Intent(PaymentsByPeriodActivity.this, PaymentsByPeriodActivity.class);
                    intent.putExtra("selected period",
                            selectedPeriodToView);
                    PaymentsByPeriodActivity.this.startActivity(intent);
                    finish();
                    break;


            }
        }
    }

    private void onSwipeLeft() {

        switch (selectedPeriodToView.getPeriod()) {
            case SelectedPeriodToView.PERIOD_BY_DAY:
                selectedPeriodToView.getMyCalendar().add(Calendar.DAY_OF_WEEK, 1);
                break;
            case SelectedPeriodToView.PERIOD_BY_WEEK:
                selectedPeriodToView.getMyCalendar().add(Calendar.DAY_OF_WEEK, 7);
                break;
            case SelectedPeriodToView.PERIOD_BY_MONTH:
                selectedPeriodToView.getMyCalendar().add(Calendar.MONTH, 1);
                break;
            case SelectedPeriodToView.PERIOD_BY_YEAR:
                selectedPeriodToView.getMyCalendar().add(Calendar.MONTH, 12);
                break;
        }
    }

    private void onSwipeRight() {

        switch (selectedPeriodToView.getPeriod()) {
            case SelectedPeriodToView.PERIOD_BY_DAY:
                selectedPeriodToView.getMyCalendar().add(Calendar.DAY_OF_WEEK, -1);
                break;
            case SelectedPeriodToView.PERIOD_BY_WEEK:
                selectedPeriodToView.getMyCalendar().add(Calendar.DAY_OF_WEEK, -7);
                break;
            case SelectedPeriodToView.PERIOD_BY_MONTH:
                selectedPeriodToView.getMyCalendar().add(Calendar.MONTH, -1);
                break;
            case SelectedPeriodToView.PERIOD_BY_YEAR:
                selectedPeriodToView.getMyCalendar().add(Calendar.MONTH, -12);
                break;
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        gestureDetector.onTouchEvent(event);
        return super.onTouchEvent(event);
    }

     @Override
    public void onBackPressed() {
       Intent intent = new Intent
                (this, MainActivity.class);
         intent.putExtra("selected period", selectedPeriodToView);
       this.startActivity(intent);
       this.finish();
    }


}
