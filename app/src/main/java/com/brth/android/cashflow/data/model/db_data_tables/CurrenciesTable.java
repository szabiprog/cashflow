package com.brth.android.cashflow.data.model.db_data_tables;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.brth.android.cashflow.R;
import com.brth.android.cashflow.data.model.db_datas_entities.CurrencyEntity;
import com.brth.android.cashflow.data.model.db_datas_entities.DbObjectEntity;

import java.util.ArrayList;
import java.util.List;

public class CurrenciesTable extends DbTable {


    public final static String NAME_TABLE = "currencies";
    public final static String ID_COLUMN = "id";
    public final static String NAME_COLUMN = "name";
    public final static String DEFAULT_COLUMN = "defaulte";
    public final static String RATIO_STRING_COLUMN = "ratio_string";
    public final static String RATIO_TYPE_COLUMN = "ratio_type";
    public final static String[] ALL_COLUMN = {ID_COLUMN, NAME_COLUMN, DEFAULT_COLUMN, RATIO_STRING_COLUMN,RATIO_TYPE_COLUMN};

    public CurrenciesTable(Context context) {
        super(context);
    }


    @Override
    public long insert(DbObjectEntity dbObjectEntity, SQLiteDatabase database) {
        long index = -1;
        CurrencyEntity p = (CurrencyEntity) dbObjectEntity;
        if(database.isOpen()){
            index = database.insert(NAME_TABLE, null, dbObjectEntityToContentValues(p));
        }
        p.setId(index);
        return index;
    }

    @Override
    public boolean update(DbObjectEntity dbObjectEntity, SQLiteDatabase database) {
        long r = database.update(NAME_TABLE, dbObjectEntityToContentValues(dbObjectEntity),
                ID_COLUMN + "=" + dbObjectEntity.getId(), null);
        return r == 1;
    }

    @Override
    public int delete(DbObjectEntity dbObjectEntity, SQLiteDatabase database) {

        return database.delete(NAME_TABLE,ID_COLUMN + "=" + dbObjectEntity.getId(), null);
    }
    @Override
    public List<DbObjectEntity> selectAllDbObjectEntities(SQLiteDatabase database) {
        List<DbObjectEntity> dbObjectEntityList = new ArrayList<>();
        if(database.isOpen()){
            Cursor cursor = database.query(NAME_TABLE, ALL_COLUMN, null,
                    null, null, null, ID_COLUMN + " asc");
            while (cursor.moveToNext()) {
                DbObjectEntity dbObjectEntity = cursorToDbObjectEntity(cursor);
                dbObjectEntityList.add(dbObjectEntity);
            }
            cursor.close();
        }
        return dbObjectEntityList;
    }

    @Override
    public DbObjectEntity cursorToDbObjectEntity(Cursor cursor) {
        CurrencyEntity currencyEntity = new CurrencyEntity();
        currencyEntity.setId(cursor.getLong(0));
        currencyEntity.setName(cursor.getString(1));
        currencyEntity.setDefaultOne(cursor.getLong(2));
        currencyEntity.setRatioString(cursor.getString(3));
        currencyEntity.setRatioType(cursor.getLong(4));


        return currencyEntity;
    }

    @Override
    public ContentValues dbObjectEntityToContentValues(DbObjectEntity dbObjectEntity) {
        ContentValues values = new ContentValues();
        values.put(NAME_COLUMN, dbObjectEntity.getName());
        values.put(DEFAULT_COLUMN, ((CurrencyEntity) dbObjectEntity).getDefaultOne());
        values.put(RATIO_STRING_COLUMN, ((CurrencyEntity) dbObjectEntity).getRatioString());
        values.put(RATIO_TYPE_COLUMN, ((CurrencyEntity) dbObjectEntity).getRatioType());
        return values;
    }

    @Override
    public List<DbObjectEntity> selectDbObjectEntities(
            SQLiteDatabase database, String inColums, String values) {
        List<DbObjectEntity> dbObjectEntityList = new ArrayList<>();
        if(database.isOpen()){
            Cursor cursor;
            cursor = database.query(NAME_TABLE, ALL_COLUMN, inColums + "=?", new String[]{values}
                    , null, null, ID_COLUMN + " asc");

            while (cursor.moveToNext()) {
                DbObjectEntity dbObjectEntity = cursorToDbObjectEntity(cursor);
                dbObjectEntityList.add(dbObjectEntity);
            }
            cursor.close();
        }
        return dbObjectEntityList;
    }

    @Override
    public DbObjectEntity selectById(Long id, SQLiteDatabase database) {

        List<DbObjectEntity> dbObjectEntityList = new ArrayList<>();
        if(database.isOpen()){
            Cursor cursor;
            cursor = database.query(NAME_TABLE, ALL_COLUMN, ID_COLUMN + "=?", new String[]{id + ""}
                    , null, null, ID_COLUMN + " asc");
            while (cursor.moveToNext()) {
                DbObjectEntity dbObjectEntity = cursorToDbObjectEntity(cursor);
                dbObjectEntityList.add(dbObjectEntity);
            }
            cursor.close();
        }
        if(!dbObjectEntityList.isEmpty()) return  dbObjectEntityList.get(0);
        else return null;
    }

   /* @Override
    public boolean overWriteId(Long id, DbObjectEntity dbObjectEntity, SQLiteDatabase database) {
                  long r = database.update(NAME_TABLE, dbObjectEntityToContentValues(dbObjectEntity),
                    ID_COLUMN + "=" + dbObjectEntity.getId(), null);
            return r == 1;
        }*/

    @Override
    public String createTable() {
        return "CREATE TABLE IF NOT EXISTS " + NAME_TABLE + " (" +
                ID_COLUMN + " integer primary key autoincrement, " + NAME_COLUMN + " text not null, " +
                DEFAULT_COLUMN + " integer default 0, " + RATIO_STRING_COLUMN + " text not null , " +
                RATIO_TYPE_COLUMN + " integer default -1);";
    }

    @Override
    public String getTableName() {
        return NAME_TABLE;
    }
    @Override
    public void deleteAll( SQLiteDatabase database)
    {
        database.execSQL("delete from "+ NAME_TABLE);
        database.execSQL(String.format("DELETE FROM SQLITE_SEQUENCE WHERE NAME = '%s'", NAME_TABLE));
    }
    @Override
    public String toString() {
        return getContext().getString(R.string.currencies);
    }


}
