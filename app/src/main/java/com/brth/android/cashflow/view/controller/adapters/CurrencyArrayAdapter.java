package com.brth.android.cashflow.view.controller.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.brth.android.cashflow.R;
import com.brth.android.cashflow.data.enums.Default;
import com.brth.android.cashflow.data.model.db_datas_entities.CurrencyEntity;
import com.brth.android.cashflow.data.model.db_datas_entities.DbObjectEntity;

import java.util.List;

public class CurrencyArrayAdapter extends ArrayAdapter<DbObjectEntity> {

    private final Activity context;
    private final List<DbObjectEntity> items;

    public CurrencyArrayAdapter(Activity context, List<DbObjectEntity> items) {
        super(context, R.layout.row_currency, items);
        this.context = context;
        this.items = items;
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {

        LayoutInflater inflater = context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.row_currency, null, true);

        TextView name = rowView.findViewById(R.id.name);
        TextView ratio = rowView.findViewById(R.id.ratio);

        TextView box = rowView.findViewById(R.id.tv_default);

        CurrencyEntity currencyEntity = (CurrencyEntity) items.get(position);

        name.setText(currencyEntity.getName());
        ratio.setText(currencyEntity.getRatioString());
        if (currencyEntity.getDefaultOne() == Default.valueOfDefaultType(Default.DEFAULT)) {
            box.setText(R.string.default_string);
        } else box.setText("");

        return rowView;
    }
}