package com.brth.android.cashflow.view.controller;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import com.brth.android.cashflow.view.controller.data.RequestCodes;
import com.brth.android.cashflow.persistance.IoService;
import com.brth.android.cashflow.view.controller.models.Views;

import java.util.List;

public class ActivityResults {
    private ActivityResults(){}
    public static void switcher
            (List<Views> history, int requestCode, int resultCode, Intent data, Context context) {
            switch (requestCode) {
                case RequestCodes.MENU_REQUEST_CODE_PICK_FILE:
                    if (resultCode == Activity.RESULT_OK) {
                        IoService.loadData(context, data);
                    }
            }
    }
}
