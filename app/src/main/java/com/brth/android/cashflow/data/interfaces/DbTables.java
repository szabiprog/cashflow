package com.brth.android.cashflow.data.interfaces;


public interface DbTables {

  String createTable();
  String getTableName();
}
