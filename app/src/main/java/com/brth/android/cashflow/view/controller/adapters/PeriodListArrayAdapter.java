package com.brth.android.cashflow.view.controller.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.brth.android.cashflow.R;
import com.brth.android.cashflow.view.controller.data.SelectedPeriodToView;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.List;

public class PeriodListArrayAdapter extends ArrayAdapter<SelectedPeriodToView> {
    private final Activity context;
    private final List<SelectedPeriodToView> items;

    public PeriodListArrayAdapter(Activity context, List<SelectedPeriodToView> items) {
        super(context, R.layout.row_inn_out_stat, items);
        this.context = context;
        this.items = items;
    }


    @Override
    public View getView(final int position, View view, ViewGroup parent) {

        LayoutInflater inflater = context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.row_inn_out_stat, null, true);

        TextView dateTV = rowView.findViewById(R.id.tv_currentDate);
        TextView innTV = rowView.findViewById(R.id.tv_inn);
        TextView outTV = rowView.findViewById(R.id.tv_out);
        SelectedPeriodToView item =items.get(position);
        String lineHead;
        SimpleDateFormat sdf;
        switch (item.getPeriod()){
            case  SelectedPeriodToView.PERIOD_BY_YEAR:
                sdf= new SimpleDateFormat("yyyy.MM.");
                break;
            default:
                sdf = new SimpleDateFormat("yyyy.MM.dd.");
        }

        innTV.setText(" " + NumberFormat.getInstance().format(item.getInnAmount()) + " ");
        outTV.setText(" " + NumberFormat.getInstance().format(item.getOutAmount()) + " ");
        lineHead = (sdf.format(item.getMyCalendar().getTime()));
        dateTV.setText(lineHead);
        return rowView;
    }
}
