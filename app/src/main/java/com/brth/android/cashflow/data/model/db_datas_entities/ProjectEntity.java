package com.brth.android.cashflow.data.model.db_datas_entities;

public class ProjectEntity extends DbObjectEntity {

    private long ownerId;
    private String description;
    private long inn;
    private long out;
    private long status;
    public ProjectEntity() {

    }

    public long getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(long ownerId) {
        this.ownerId = ownerId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getInn() {
        return inn;
    }

    public void setInn(long inn) {
        this.inn = inn;
    }

    public long getOut() {
        return out;
    }

    public void setOut(long out) {
        this.out = out;
    }

    public long getStatus() {
        return status;
    }

    public void setStatus(long status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "ProjectEntity{" +
                "name=" + getName() +
                "ownerId=" + ownerId +
                ", description='" + description + '\'' +'}';
    }


}
