package com.brth.android.cashflow.data.model.db_data_tables;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.brth.android.cashflow.R;
import com.brth.android.cashflow.data.model.db_datas_entities.DbObjectEntity;
import com.brth.android.cashflow.data.model.db_datas_entities.PaymentEventEntity;

import java.util.ArrayList;
import java.util.List;

public class PaymentEventsTable extends DbTable {


    public final static String NAME_TABLE = "payment_events";
    public final static String ID_COLUMN = "id";
    public final static String VALID_COLUMN = "valid";
    public final static String DIRECTION_COLUMN = "direction";
    public final static String DATE_COLUMN = "date";
    public final static String OWNER_ID_COLUMN = "project_id";
    public final static String DESCRIPTION_COLUMN = "description";
    public final static String AMOUNT_COLUMN = "amount";
    public final static String ACCOUNT_ID_COLUMN = "account_id";
    public final static String CURRENCY_ID_COLUMN = "currency_id";
    public final static String TYPE_COLUMN = "type";
    public final static String ODOMETER_COLUMN = "odometer";
    public final static String FUEL_QUANTITY_COLUMN = "quantity";
    public final static String FUEL_ID_COLUMN = "fuel_id";
    private static final String SERVICE_ID_COLUMN = "service_id";
    private static final String TAG_ID_COLUMN = "tag_id";


    public final static String[] ALL_COLUMN = {ID_COLUMN, VALID_COLUMN, DIRECTION_COLUMN, DATE_COLUMN,
            OWNER_ID_COLUMN, DESCRIPTION_COLUMN, AMOUNT_COLUMN, ACCOUNT_ID_COLUMN,
            CURRENCY_ID_COLUMN, TYPE_COLUMN, ODOMETER_COLUMN, FUEL_QUANTITY_COLUMN, FUEL_ID_COLUMN,
            SERVICE_ID_COLUMN, TAG_ID_COLUMN};


    public PaymentEventsTable(Context context) {
        super(context);
    }


    @Override
    public long insert(DbObjectEntity dbObjectEntity, SQLiteDatabase database) {
        PaymentEventEntity p = (PaymentEventEntity) dbObjectEntity;

        long index = database.insert(NAME_TABLE, null, dbObjectEntityToContentValues(p));
        p.setId(index);

        return index;

    }

    @Override
    public boolean update(DbObjectEntity dbObjectEntity, SQLiteDatabase database) {
        PaymentEventEntity p = (PaymentEventEntity) dbObjectEntity;
        long r = database.update(NAME_TABLE, dbObjectEntityToContentValues(p),
                ID_COLUMN + "=" + dbObjectEntity.getId(), null);
        return r == 1;
    }

    @Override
    public int delete(DbObjectEntity dbObjectEntity, SQLiteDatabase database) {
        return 0;
    }

    @Override
    public List<DbObjectEntity> selectAllDbObjectEntities(SQLiteDatabase database) {

        Cursor cursor = database.query(NAME_TABLE, ALL_COLUMN, null,
                null, null, null, DATE_COLUMN + " desc");
        List<DbObjectEntity> dbObjectEntityList = new ArrayList<>();
        while (cursor.moveToNext()) {
            DbObjectEntity dbObjectEntity = cursorToDbObjectEntity(cursor);

            dbObjectEntityList.add(dbObjectEntity);
        }
        cursor.close();
        return dbObjectEntityList;
    }

    @Override
    public DbObjectEntity cursorToDbObjectEntity(Cursor cursor) {
        PaymentEventEntity entity = new PaymentEventEntity();
        entity.setId(cursor.getLong(0));
        entity.setValid(cursor.getLong(1));
        entity.setDirectionType(cursor.getLong(2));
        entity.setDate(cursor.getString(3));
        entity.setProjectId(cursor.getLong(4));
        entity.setDescription(cursor.getString(5));
        entity.setAmount(cursor.getLong(6));
        entity.setMoneyAccountId(cursor.getLong(7));
        entity.setCurrencyId(cursor.getLong(8));
        entity.setType(cursor.getLong(9));
        entity.setOdometer(cursor.getLong(10));
        entity.setQuantity(cursor.getString(11));
        entity.setFuelId(cursor.getLong(12));
        entity.setServiceId(cursor.getLong(13));
        entity.setTagIdsBin(cursor.getLong(14));
        return entity;
    }

    @Override
    public ContentValues dbObjectEntityToContentValues(DbObjectEntity dbObjectEntity) {
        PaymentEventEntity p = (PaymentEventEntity) dbObjectEntity;

        ContentValues values = new ContentValues();
        values.put(VALID_COLUMN, p.getValid());
        values.put(DIRECTION_COLUMN, p.getDirectionType());
        values.put(DATE_COLUMN, p.getDate());
        values.put(OWNER_ID_COLUMN, p.getProjectId());
        values.put(DESCRIPTION_COLUMN, p.getDescription());
        values.put(AMOUNT_COLUMN, p.getAmount());
        values.put(ACCOUNT_ID_COLUMN, p.getMoneyAccountId());
        values.put(CURRENCY_ID_COLUMN, p.getCurrencyId());
        values.put(TYPE_COLUMN, p.getType());
        values.put(ODOMETER_COLUMN, p.getOdometer());
        values.put(FUEL_QUANTITY_COLUMN, p.getQuantity());
        values.put(SERVICE_ID_COLUMN, p.getServiceId());
        values.put(FUEL_ID_COLUMN, p.getFuelId());
        values.put(SERVICE_ID_COLUMN, p.getServiceId());
        values.put(TAG_ID_COLUMN, p.getTagIdsBin());
        return values;
    }

    @Override
    public List<DbObjectEntity> selectDbObjectEntities(SQLiteDatabase database, String inColums, String values) {
        Cursor cursor;

        cursor = database.query(true, NAME_TABLE, ALL_COLUMN, inColums + " LIKE ?",
                new String[]{values}, null, null, DATE_COLUMN + " desc",
                null);


        //  cursor = database.query(NAME_TABLE, ALL_COLUMN, inColums + "=?", new String[]{values}
        //       , null, null, ID_COLUMN + " asc");
        List<DbObjectEntity> dbObjectEntityList = new ArrayList<>();
        while (cursor.moveToNext()) {
            DbObjectEntity dbObjectEntity = cursorToDbObjectEntity(cursor);

            dbObjectEntityList.add(dbObjectEntity);

        }
        cursor.close();
        return dbObjectEntityList;
    }

    @Override
    public DbObjectEntity selectById(Long id, SQLiteDatabase database) {
        Cursor cursor;
        cursor = database.query(NAME_TABLE, ALL_COLUMN, ID_COLUMN + "=?", new String[]{id + ""}
                , null, null, ID_COLUMN + " asc");
        List<DbObjectEntity> dbObjectEntityList = new ArrayList<>();
        while (cursor.moveToNext()) {
            DbObjectEntity dbObjectEntity = cursorToDbObjectEntity(cursor);
            dbObjectEntityList.add(dbObjectEntity);
        }
        cursor.close();
        if (!dbObjectEntityList.isEmpty()) return dbObjectEntityList.get(0);
        else return null;
    }


    @Override
    public String createTable() {
        return "CREATE TABLE IF NOT EXISTS " + NAME_TABLE + " (" +
                ID_COLUMN + " integer primary key autoincrement, " + VALID_COLUMN + " integer default 0, "
                + DIRECTION_COLUMN + " integer default 0, " + DATE_COLUMN + " text not null, "
                + OWNER_ID_COLUMN + " integer default 0, " + DESCRIPTION_COLUMN + " text not null,"
                + AMOUNT_COLUMN + " integer default 0, " + ACCOUNT_ID_COLUMN + " integer default 0, "
                + CURRENCY_ID_COLUMN + " integer default 0, " + TYPE_COLUMN + " integer default 0, "
                + ODOMETER_COLUMN + " integer default 0, " + FUEL_QUANTITY_COLUMN + "  text not null, "
                + FUEL_ID_COLUMN + " integer default 0 , " + SERVICE_ID_COLUMN  + " integer default 0 , "
                + TAG_ID_COLUMN + " integer default 0 );";
    }

    @Override
    public String getTableName() {
        return NAME_TABLE;
    }

    @Override
    public String toString() {
        return getContext().getString(R.string.payment_events);
    }

    @Override
    public void deleteAll(SQLiteDatabase database) {
        database.execSQL("delete from " + NAME_TABLE);
        database.execSQL(String.format("DELETE FROM SQLITE_SEQUENCE WHERE NAME = '%s'", NAME_TABLE));
    }
}