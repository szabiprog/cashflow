package com.brth.android.cashflow.view.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.brth.android.cashflow.R;
import com.brth.android.cashflow.persistance.IoService;
import com.brth.android.cashflow.view.controller.data.History;
import com.brth.android.cashflow.view.controller.data.RequestCodes;
import com.brth.android.cashflow.view.controller.data.SelectedPeriodToView;
import com.brth.android.cashflow.view.controller.models.MainViewModel;
import com.brth.android.cashflow.view.controller.models.Views;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private SelectedPeriodToView selectedPeriodToView;
    private boolean doubleBackToExitPressedOnce = false;
    private boolean saveFlag;
    private List<Views> history;
    private Views actualView;
    private Bundle savedInstanceState;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.savedInstanceState = savedInstanceState;
        if (savedInstanceState != null) {
            History historyObject = (History) savedInstanceState.getSerializable("History");
            if (historyObject != null) {
                history = historyObject.history;
            }
        }
        if (history == null) {
            history = new ArrayList<>();
            MainViewModel.getInstance(history, savedInstanceState);
        }
    }

    @Override
    public boolean onCreatePanelMenu(int featureId, @NonNull Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu, menu);
        return true;
    }

    public void onClick(View view) {
        actualView.onClick(view);
    }

    @Override
    protected void onResume() {
        super.onResume();
        actualView = Views.selectViewModel(history);
        actualView.onResume(this);
    }

    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (history.size() > 1) {
            onResume();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        actualView.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        saveFlag = false;
        switch (item.getItemId()) {
            case R.id.menu_save:
                saveFlag = true;
                ActivityCompat.requestPermissions(MainActivity.this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        1);
                return true;
            case R.id.menu_load:
                Intent chooseFile = new Intent(Intent.ACTION_GET_CONTENT);
                chooseFile.setType("text/*");
                chooseFile.addCategory(Intent.CATEGORY_OPENABLE);
                Intent intent = Intent.createChooser(chooseFile, "Choose a file");
                startActivityForResult(intent, RequestCodes.MENU_REQUEST_CODE_PICK_FILE);
                return true;
            case R.id.menu_open_drive:
                Intent launchIntent = getPackageManager().getLaunchIntentForPackage("com.google.android.apps.docs");
                if (launchIntent != null) {
                    startActivity(launchIntent);//null pointer check in case package name was not found
                }
                break;
            case R.id.menu_close:
                finishAndRemoveTask();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        if (history.size() > 1) {
            history.remove(history.size() - 1);
            onResume();
        } else {
            if (doubleBackToExitPressedOnce) {
                finishAndRemoveTask();
                return;
            }
            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, R.string.back_again, Toast.LENGTH_SHORT).show();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 2000);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (saveFlag) {
                        IoService.saveAllData(this);
                    }
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(MainActivity.this, "Permission denied to read your External storage", Toast.LENGTH_SHORT).show();
                }
                return;
            }
            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        actualView.putToSaveInstanceState(outState);
        outState.putSerializable("History", new History(history));
        actualView.setSavedInstanceState(outState);
    }

}
