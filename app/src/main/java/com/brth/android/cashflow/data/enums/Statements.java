package com.brth.android.cashflow.data.enums;

public enum Statements {
    INACTIVATED,
    HIDDEN ;

    public static int valueOfStatementsType(Statements statement) {
        if (statement.equals(INACTIVATED)) return -1;
        if (statement.equals(HIDDEN)) return -10;
        return 0;
    }
    public static Statements valueOfStatementsType(long value) {
        if (value == valueOfStatementsType(INACTIVATED) ) return INACTIVATED;
        if (value == valueOfStatementsType(HIDDEN) ) return HIDDEN;
        return HIDDEN;
    }
}
