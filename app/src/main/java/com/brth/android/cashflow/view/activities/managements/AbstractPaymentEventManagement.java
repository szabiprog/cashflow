package com.brth.android.cashflow.view.activities.managements;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.brth.android.cashflow.R;
import com.brth.android.cashflow.data.model.db_datas_entities.CarServiceEntity;
import com.brth.android.cashflow.data.model.db_datas_entities.CurrencyEntity;
import com.brth.android.cashflow.data.model.db_datas_entities.MoneyAccountEntity;
import com.brth.android.cashflow.data.model.db_datas_entities.PaymentEventEntity;
import com.brth.android.cashflow.persistance.db_table_services.CurrencyService;
import com.brth.android.cashflow.persistance.db_table_services.MoneyAccountService;
import com.brth.android.cashflow.persistance.db_table_services.PaymentEventService;

import java.text.SimpleDateFormat;
import java.util.Calendar;

abstract class AbstractPaymentEventManagement extends Activity {

    final int REQUEST_CODE_PROJECT = 111;
    final int REQUEST_CODE_MONEY_ACC = 333;
    final int REQUEST_CODE_TAG = 444;

    Intent incomingIntent;
    Button btProjectSelect;
    Button btTagSelect;
    Button btSwitchType;
    Button buttonSave;
    Button btMoneyAccountSelect;
    Switch swMoneyMoveDirection;
    EditText textDescription;
    EditText textMoney;
    TextView tvCurrencyName;
    TextView tvDate;
    TextView tvDateInterval;
    EditText textOdometer;
    EditText textQuantity;
    EditText textOdometerInterval;
    LinearLayout lnOdometer;
    LinearLayout lnDateInterval;
    LinearLayout lnOdometerInterval;
    LinearLayout lnQuantity;

    String getMoney;
    String spendMoney;
    String currentDateTime = null;
    String dateOfNextService;


    boolean update;
    PaymentEventEntity paymentEventEntity;
    CarServiceEntity carServiceEntity;
    PaymentEventEntity oldPaymentEventEntity;
    Calendar myCalendar;
    DatePickerDialog.OnDateSetListener date;


    void onMyCreate(){
        setContentView(R.layout.activity_outlay);
        buttonSave = this.findViewById(R.id.bt_save);
        btSwitchType = this.findViewById(R.id.switch_service_fuel_normal);
        btProjectSelect = findViewById(R.id.bt_select_project);
        btTagSelect = findViewById(R.id.bt_select_tag);
        btMoneyAccountSelect = findViewById(R.id.bt_select_account);
        textDescription = findViewById(R.id.text_description);
        tvCurrencyName = findViewById(R.id.tv_currency_name);
        swMoneyMoveDirection = findViewById(R.id.switch_money_move_direction);
        textOdometer = findViewById(R.id.text_odometer);
        textQuantity = findViewById(R.id.text_quantity);
        lnOdometer = findViewById(R.id.ln_odometer);
        lnQuantity = findViewById(R.id.ln_quantity);
        lnDateInterval = findViewById(R.id.ln_date_interval);
        lnOdometerInterval = findViewById(R.id.ln_odometer_interval);
        textMoney = findViewById(R.id.text_money);
        tvDate = findViewById(R.id.tv_currentDate);
        tvDateInterval = findViewById(R.id.tv_date_interval);
        textOdometerInterval = findViewById(R.id.text_odometer_interval);
        getMoney = getString(R.string.get_money);
        spendMoney = getString(R.string.spend_money);
    }

    void selectViewOfBtType() {
        switch ((int) paymentEventEntity.getType()) {
            case (int) PaymentEventEntity.NORMAL_TYPE:
                lnOdometer.setVisibility(View.GONE);
                lnQuantity.setVisibility(View.GONE);
                lnOdometerInterval.setVisibility(View.GONE);
                lnDateInterval.setVisibility(View.GONE);
                btSwitchType.setText(R.string.normal);
                break;
            case (int) PaymentEventEntity.FUEL_TYPE:
                lnQuantity.setVisibility(View.VISIBLE);
                lnOdometer.setVisibility(View.VISIBLE);
                lnOdometerInterval.setVisibility(View.GONE);
                lnDateInterval.setVisibility(View.GONE);
                btSwitchType.setText(R.string.Fuel);
                break;
            case (int) PaymentEventEntity.SERVICE_TYPE:
                lnQuantity.setVisibility(View.GONE);
                lnOdometer.setVisibility(View.VISIBLE);
                lnOdometerInterval.setVisibility(View.VISIBLE);
                lnDateInterval.setVisibility(View.VISIBLE);
                btSwitchType.setText(R.string.Service);
                break;
        }
    }

    void updateLabel() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd. HH:mm:ss");
        tvDate.setText(sdf.format(myCalendar.getTime()));
        currentDateTime = (String) tvDate.getText();
        paymentEventEntity.setDate(currentDateTime);
    }

    void setupFromAccByName(String nameOfMoneyAccount) {
        MoneyAccountEntity mAcc = MoneyAccountService.selectByName(nameOfMoneyAccount, this);
        if (mAcc != null) {
            long moneyAccountId = mAcc.getId();
            paymentEventEntity.setMoneyAccountId(moneyAccountId);
            long currencyId = mAcc.getCurrencyId();
            paymentEventEntity.setCurrencyId(currencyId);
            CurrencyEntity currencyEntity = CurrencyService.selectById(currencyId, this);
            String nameOfCurrency = currencyEntity.getName();
            tvCurrencyName.setText(nameOfCurrency);
            paymentEventEntity.setCurrencyId(currencyId);
        } else {
            //hibakezelés
            System.out.println("visszatért2");
        }
    }

    void sendToast(String str) {
        Toast toast = Toast.makeText(this, str, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 0, 0);
        toast.show();
    }

    void sendToast(int str) {
        Toast toast = Toast.makeText(this, str, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 0, 0);
        toast.show();
    }

    void datePicker() {
        myCalendar = Calendar.getInstance();
        date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }
        };
    }

    long saveEntity() {
        long projectAccId = paymentEventEntity.getProjectId();
        long moneyAccountId = paymentEventEntity.getMoneyAccountId();
        if (projectAccId > 0 && moneyAccountId > 0) {
            getDatasFromForm();
            if (update) {
                boolean backwards = PaymentEventService.update(paymentEventEntity, oldPaymentEventEntity, this);
                if (!backwards) {
                    sendToast(R.string.backwards_error);
                }
                return paymentEventEntity.getId();
            } else {
                paymentEventEntity.setDate(currentDateTime);
                return PaymentEventService.save(paymentEventEntity, this);
            }
        } else sendToast(R.string.invalid_datas);
        return 0;
    }

    protected void getDatasFromForm(){
        String moneyValue = textMoney.getText().toString();
        long amount = Long.parseLong(moneyValue);
        paymentEventEntity.setAmount(amount);
        paymentEventEntity.setDescription(textDescription.getText().toString());
        long odometer = 0;
        try {
            odometer = Long.valueOf(textOdometer.getText().toString());
        } catch (NumberFormatException e) {
            sendToast(R.string.odometer_value_problem);
        }
        paymentEventEntity.setOdometer(odometer);
        paymentEventEntity.setQuantity(textQuantity.getText().toString());

    }

}
