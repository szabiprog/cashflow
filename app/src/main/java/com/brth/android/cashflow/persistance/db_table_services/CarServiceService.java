package com.brth.android.cashflow.persistance.db_table_services;

import android.content.Context;

import com.brth.android.cashflow.data.model.db_datas_entities.CarServiceEntity;

public class CarServiceService extends DbService {
    private CarServiceService() {
    }
    public static CarServiceEntity selectById(long id, Context context) {
        initializing(context);
        CarServiceEntity entity =
                (CarServiceEntity) dataBaseOperations.selectById(carServicesTable, id);
        return entity;
    }
}
