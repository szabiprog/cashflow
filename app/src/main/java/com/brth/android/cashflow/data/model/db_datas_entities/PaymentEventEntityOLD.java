package com.brth.android.cashflow.data.model.db_datas_entities;

public class PaymentEventEntityOLD extends DbObjectEntity {

    public static final long INVALID = 0;
    public static final long VALID = 1;
    public static final long REGISTERED = 2;
    public static final long TRANSFER = -1;


    private long projectId;
    private long moneyAccountId;
    private long currencyId;
    private long amount;
    private String description;
    private long directionType;
    private String date;
    private long valid;

    public long getProjectId() {
        return projectId;
    }

    public void setProjectId(long projectId) {
        this.projectId = projectId;
    }

    public long getAmount() {
        return amount;
    }

    public void setAmount(long amount) {
        this.amount = amount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getDirectionType() {
        return directionType;
    }

    public void setDirectionType(long directionType) {
        this.directionType = directionType;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }


    public long getValid() {
        return valid;
    }

    public void setValid(long valid) {
        this.valid = valid;
    }

    public long getMoneyAccountId() {
        return moneyAccountId;
    }

    public void setMoneyAccountId(long moneyAccountId) {
        this.moneyAccountId = moneyAccountId;
    }

    public long getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(long currencyId) {
        this.currencyId = currencyId;
    }
}
