package com.brth.android.cashflow.view.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.brth.android.cashflow.R;
import com.brth.android.cashflow.data.model.db_datas_entities.DbObjectEntity;
import com.brth.android.cashflow.persistance.db_table_services.CurrencyService;
import com.brth.android.cashflow.view.activities.managements.CurrencyManagementActivity;
import com.brth.android.cashflow.view.controller.adapters.CurrencyArrayAdapter;

import java.util.List;

public class CurrenciesActivity extends AppCompatActivity {
    private ListView listViewDbTables;
    private ArrayAdapter<DbObjectEntity> arrayAdapter;
    private TextView header;
    private Button button;
    private String toSelect;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        header = findViewById(R.id.tv_header);
        button = findViewById(R.id.bt_new_item);

        Intent intent = getIntent();
        toSelect = intent.getStringExtra("select");
        findViewById(R.id.iv_back_empty_list).setVisibility(View.GONE);
        if (toSelect == null) {
            header.setText(R.string.currencies);
            button.setText(R.string.new_currency);
        } else {
            header.setText(R.string.select_currency);
            button.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        setUpAdapter();
    }

    private void setUpAdapter() {
        List<DbObjectEntity> currencies = CurrencyService.allCurrencies(this);
        arrayAdapter = new CurrencyArrayAdapter(this, currencies);
        listViewDbTables = findViewById(R.id.lv_items);
        listViewDbTables.setAdapter(arrayAdapter);
        listViewDbTables.setLongClickable(true);
        if (toSelect != null) listViewDbTables.setOnItemClickListener(new MyItemClickListener());
        else
            listViewDbTables.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                @Override
                public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
                                               int pos, long id) {
                    String name = ((TextView) arg1.findViewById(R.id.name)).getText().toString();
                    Intent intent = new Intent
                            (CurrenciesActivity.this, CurrencyManagementActivity.class);
                    intent.putExtra("name", name);
                    CurrenciesActivity.this.startActivity(intent);
                    return true;
                }
            });
    }

    public void onClick(View view) {
        Intent intent;
        switch (view.getId()) {
            case R.id.bt_new_item:
                intent = new Intent(this, CurrencyManagementActivity.class);
                startActivity(intent);
                break;
        }
    }

    private class MyItemClickListener implements AdapterView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Intent data = new Intent();
            data.putExtra("name",
                    ((TextView) view.findViewById(R.id.name)).getText().toString());
            setResult(Activity.RESULT_OK, data);
            finish();
        }
    }

}
