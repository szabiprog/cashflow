package com.brth.android.cashflow.persistance.db_table_services;

import android.content.Context;

import com.brth.android.cashflow.data.model.db_data_tables.TagTable;
import com.brth.android.cashflow.data.model.db_datas_entities.DbObjectEntity;
import com.brth.android.cashflow.data.model.db_datas_entities.TagEntity;

import java.util.ArrayList;
import java.util.List;

public class TagService  extends DbService {
    private TagService() {
    }

    public static TagEntity selectById(long id, Context context) {
        initializing(context);
        return (TagEntity) dataBaseOperations.selectById(tagTable, id);
    }

    public static boolean unUsedName(TagEntity entity, boolean update, String nameValue, Context context) {
        initializing(context);
        List<DbObjectEntity> dbObjectEntities =
                dataBaseOperations.selectDbObjectEntities
                        (tagTable, TagTable.NAME_COLUMN, nameValue);
        if (update && !dbObjectEntities.isEmpty()) {
            for (int i = 0; i < dbObjectEntities.size(); i++) {
                TagEntity c;
                c = (TagEntity) dbObjectEntities.get(i);
                if (c.getId() != entity.getId()) return false;
            }
            return true;
        }
        return dbObjectEntities.isEmpty();
    }

    public static boolean update(TagEntity entity,  Context context) {
        initializing(context);
        return dataBaseOperations.update(entity, tagTable);
    }

    public static long insert(TagEntity entity,  Context context) {
        initializing(context);
        return dataBaseOperations.insert(entity, tagTable);
    }
    public static TagEntity getByName( Context context ,String name) {
        initializing(context);
        List<DbObjectEntity> dbObjectEntities =
                dataBaseOperations.selectDbObjectEntities
                        (tagTable, TagTable.NAME_COLUMN, name);
        if (dbObjectEntities.size() > 0) return (TagEntity) dbObjectEntities.get(0);
        return null;
    }

    public static Long generateBinaryFromList(List<Long> tagIds) {
        double binary = 0;
        if (tagIds == null || tagIds.size()==0 ) return (long) binary;
        for (int i = 0; i < tagIds.size() ; i++) {
            long id = tagIds.get(i);
            binary += Math.pow(2,id);
        }
        return  (long) binary ;
    }

    public static List<Long> listOfIdsFromBinary(Long tagId) {
        List<Long> ids = new ArrayList<>();
            for (int i = 1; i < 63 ; i++) {
                long mask = (long) Math.pow(2,i);
                if ( (tagId & mask) > 0) ids.add((long) i);
            }
        return ids;
    }

    public static List<DbObjectEntity> getAllTagEntities(Context context) {
       return DbService.selectAllDbObjectEntitiesByClass(TagEntity.class,context);
    }

    public static List<DbObjectEntity> selectByIdBinary(long tagIdsBin, Context context) {
        initializing(context);
        List<DbObjectEntity> tags = new ArrayList<>();
        List<Long> ids = listOfIdsFromBinary(tagIdsBin);
        for ( Long id: ids) {
            tags.add(selectById(id,context));
        }
        return tags;
    }
}