package com.brth.android.cashflow.view.controller.models;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.brth.android.cashflow.R;
import com.brth.android.cashflow.persistance.db_table_services.PaymentEventService;
import com.brth.android.cashflow.view.controller.adapters.FuelConsumptionsArrayAdapter;
import com.brth.android.cashflow.view.controller.data.AverageFuelConsumption;

import java.util.List;

public class AveragesFuelConsumptionViewModel extends Views {

    private ListView listViewDbTables;
    private ArrayAdapter<AverageFuelConsumption> arrayAdapter;
    private TextView header;
    private Button button;


    static private AveragesFuelConsumptionViewModel instance;
    static private List<Views> history;
    static private Activity mainActivity;

    private AveragesFuelConsumptionViewModel() {
    }

    public static synchronized AveragesFuelConsumptionViewModel getInstance(List<Views> history, Bundle savedInstanceState) {
        AveragesFuelConsumptionViewModel.history = history;
        if (instance == null) {
            instance = new AveragesFuelConsumptionViewModel();
        }
        instance.setSavedInstanceState(savedInstanceState);
        history.add(instance);
        return instance;
    }
    private void setUpAdapter(Activity mainActivity) {
        List<AverageFuelConsumption> averageFuelConsumptions= PaymentEventService.listOfAverageFuelConsumption(mainActivity);
        arrayAdapter = new FuelConsumptionsArrayAdapter(mainActivity, averageFuelConsumptions);
        listViewDbTables = mainActivity.findViewById(R.id.lv_items);
        listViewDbTables.setAdapter(arrayAdapter);
    }


    @Override
    public void onResume(Context context) {
            mainActivity = (Activity) context;
            mainActivity.setContentView(R.layout.activity_list);
            header = mainActivity.findViewById(R.id.tv_header);
            button = mainActivity.findViewById(R.id.bt_new_item);
            mainActivity.findViewById(R.id.iv_back_empty_list).setVisibility(View.GONE);
            header.setText(R.string.average_fuel_consumptions);
            button.setVisibility(View.GONE);
            setUpAdapter(mainActivity);

        }

    @Override
    public void onClick(View view) {

    }

    @Override
    public void putToSaveInstanceState(Bundle outState) {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

    }
}
