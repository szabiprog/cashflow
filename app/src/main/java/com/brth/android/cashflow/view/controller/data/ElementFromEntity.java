package com.brth.android.cashflow.view.controller.data;

import com.brth.android.cashflow.data.model.db_datas_entities.DbObjectEntity;

import java.io.Serializable;
import java.util.List;

public class ElementFromEntity implements Serializable {
    private long entityId;
    private String name ;
    private long inn;
    private long out;
    private long balance;
    private List<DbObjectEntity> paymentEventEntities;


    private ElementFromEntity() {
    }

    public ElementFromEntity(long id, String name) {
        this.entityId = id;
        this.name = name;
    }

    public long getEntityId() {
        return entityId;
    }

    public String getName() {
        return name;
    }


    public long getInn() {
        return inn;
    }

    public void setInn(long inn) {
        this.inn = inn;
    }

    public long getOut() {
        return out;
    }

    public void setOut(long out) {
        this.out = out;
    }

    public long getBalance() {
        return balance;
    }

    public void setBalance(long balance) {
        this.balance = balance;
    }

    public List<DbObjectEntity> getPaymentEventEntities() {
        return paymentEventEntities;
    }

    public void setPaymentEventEntities(List<DbObjectEntity> paymentEventEntities) {
        this.paymentEventEntities = paymentEventEntities;
    }
}
