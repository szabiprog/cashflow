package com.brth.android.cashflow.view.controller.models;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public interface ViewModels {

    void onResume(Context context);

    void onClick(View view);

    void onActivityResult(int requestCode, int resultCode, Intent data);

    void putToSaveInstanceState(Bundle outState);


}
