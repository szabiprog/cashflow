package com.brth.android.cashflow.data.enums;

public enum Default {
    DEFAULT, NOT_DEFAULT ;

    public static int valueOfDefaultType(Default aDefault) {
        if (aDefault.equals(DEFAULT)) return 1;
        if (aDefault.equals(NOT_DEFAULT)) return 0;
        return 0;
    }
    public static Default valueOfDefaultType(long value) {
        if (value==1) return DEFAULT;
        if (value==0) return NOT_DEFAULT;
        return NOT_DEFAULT;
    }
}
